import webpackTestConfig from './webpack-test.config';
import { ConfigOptions } from 'karma';

export default (config) => {
  config.set({
    // Base path that will be used to resolve all patterns (eg. files, exclude).
    basePath: './',

    // Frameworks to use.
    // Available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],

    // List of files to load in the browser.
    files: [
      'karma-test-entry.ts'
    ],

    // Preprocess matching files before serving them to the browser.
    // Available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      'karma-test-entry.ts': ['webpack', 'sourcemap']
    },

    webpack: webpackTestConfig,

    // Webpack please don't spam the console when running in karma!
    webpackMiddleware: {
      noInfo: true,
      // Use stats to turn off verbose output.
      stats: {
        chunks: false
      }
    },

    mime: {
      'text/x-typescript': [ 'ts' ]
    },

    coverageIstanbulReporter: {
      reports: ['text-summary', 'html', 'lcovonly'],
      fixWebpackSourcePaths: true
    },

    // Test results reporter to use.
    // Possible values: 'dots', 'progress'.
    // Available reporters: https://npmjs.org/browse/keyword/karma-reporter
    
    reporters: ['mocha', 'coverage-istanbul'],
    // reporters: ['progress', 'html'],

    // // the default configuration
    // htmlReporter: {
    //   outputDir: 'karma_html', // where to put the reports 
    //   templatePath: null, // set if you moved jasmine_template.html
    //   focusOnFailures: true, // reports show failures on start
    //   namedFiles: false, // name files instead of creating sub-directories
    //   pageTitle: null, // page title for reports; browser info by default
    //   urlFriendlyName: false, // simply replaces spaces with _ for files/dirs
    //   reportName: 'report-summary-filename', // report summary filename; browser info by default
      
      
    //   // experimental
    //   preserveDescribeNesting: false, // folded suites stay folded 
    //   foldAll: false, // reports start folded (only with preserveDescribeNesting)
    // },
    

    // Level of logging
    // Possible values:
    // - config.LOG_DISABLE
    // - config.LOG_ERROR
    // - config.LOG_WARN
    // - config.LOG_INFO
    // - config.LOG_DEBUG
    logLevel: config.LOG_DISABLE,

    // Start these browsers.
    // Available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],

    browserConsoleLogOptions: {
      terminal: false,
      level: 'log'
    },

    singleRun: false,
    colors: true
  } as ConfigOptions);
};
