module.exports = function(){

    function findParent(element,className){
        if (element){
            if (element.classList && element.classList.contains(className)){
                return element;
            } else {
                return findParent(element.parentNode,className);
            }
        } else {
            return false;
        }
    }
    function resizeBar(target,tabs){
        tabs.style.backgroundPosition = 'bottom 0 left '+target.offsetLeft+'px';
        console.log(target.offsetWidth);
        tabs.style.backgroundSize = target.offsetWidth+'px 3px';
    };

    window.changeTab = function(e){
        e.preventDefault();
        
        let link = e.target;
        let tab = link.parentNode;
        let tabs = tab.parentNode;
        let linkText = link.innerText;

        let linkTarget = link.getAttribute('href');

        let activeTab = tabs.querySelector('.active');
        
        if (activeTab!==tab){
            tabs.dataset.selectedText = linkText;
            activeTab.classList.remove('active');
            tab.classList.add('active');

            let target = document.querySelector(linkTarget);

            let tabsParent = findParent(target,'cmx-tabs__content-wrap');
            [...tabsParent.childNodes].map((tabContent)=>{
                if ((typeof(tabContent.classList)!=='undefined') && (tabContent.classList.contains('active'))){
                    tabContent.classList.remove('active');
                }
            });
            target.classList.add('active');
            resizeBar(tab,tabs);
        }
    };
    window.openTabs = function(e){
        let target = e.target;

        let tabs = findParent(target,'cmx-tabs');
        if (tabs.classList.contains('open')){
            tabs.classList.remove('open');
        } else {
            let check = window.getComputedStyle(tabs, ':before').getPropertyValue('content');
            if (check!=="\"tabs_on_desktop\""){
                tabs.classList.add('open');
            }
        }
    };

    function initTabs(){
        let activeTabs = document.querySelectorAll('.cmx-tabs__tab.active');
        [...activeTabs].map((tab)=>{
            resizeBar(tab,tab.parentNode);
        });
    }

    // on resize
    window.addEventListener('resize',initTabs);

    // on load
    initTabs();
};