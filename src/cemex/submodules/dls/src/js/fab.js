module.exports = ()=>{
    function isWithin(element,className){
        if (element){
            if (element.classList && element.classList.contains(className)){
                return true;
            } else {
                return isWithin(element.parentNode,className);
            }
        } else {
            return false;
        }
    }
    // a little function that clones jQuerys parents()
    function findParent(element,className){
        if (element){
            if (element.classList && element.classList.contains(className)){
                return element;
            } else {
                return findParent(element.parentNode,className);
            }
        } else {
            return false;
        }
    };
    function closeFab(e){
        let target = e.target;
        let isWithinMenu = isWithin(target,'cmx-fab__menu') || isWithin (target,'cmx-fab__toggle');

        if(!isWithinMenu){
            document.querySelector('.cmx-fab').classList.remove('active');
            document.removeEventListener('click',closeFab);
        }
    }
    window.toggleFab = (e) =>{ 
        let button = e.target;
        let menu = findParent(button,'cmx-fab');

        let isOpen = menu.classList.contains('active');

        if (!isOpen) {
            menu.classList.add('active');

            document.addEventListener('click',closeFab);
        } else {
            menu.classList.remove('active');
        }
    }
};