module.exports = function() {
    // the toggling funciton
    function stepperEl(e){
        console.log('woot')
        // function to verify if is within some element with specific class
        function isWithin(element,className){
            if (element){
                if (element.classList && element.classList.contains(className)){
                    return true;
                } else {
                    return isWithin(element.parentNode,className);
                }
            } else {
                return false;
            }
        }

        
        let target = e.target;
        // is it the stepper?
        let iw = isWithin(target,'cmx-stepper');
        // is it the button?
        let ib = isWithin(target,'cmx-stepper__heading');

        //if it isn't the stepper, nor the button....
        if (!iw && !ib){
            window.toggleStepper();
            document.removeEventListener('click',stepperEl);
        }
    };
    window.toggleStepper = function(){
        // toggle stepper
        let body = document.querySelector('body');

        if (body.classList.contains('cmx-stepper--open')){
            body.classList.remove('cmx-stepper--open');
            body.classList.add('cmx-stepper--closed');
        } else {
            body.classList.add('cmx-stepper--open');
            body.classList.remove('cmx-stepper--closed');

            document.addEventListener('click',stepperEl);
        }
    };

    // on load, add closed class to the body if not present.
    //is there any stepper?
    let gotStepper = document.querySelector('.cmx-stepper');
    if(gotStepper){
        document.querySelector('body').classList.add('cmx-stepper--closed');
    }
};