CYAN='\033[0;36m'
NC='\033[0m' # No Color

# Default env vars
export APP_CODE=DCMWEBPORTAL
export API_ORG=''
export API_ENV=''
export API_HOST=https://cemexqas.azure-api.net/
export ENV=nonprod
export TRANSLATE_URL=https://uscldcnxwaadmq01.azurewebsites.net/
export COUNTLY_KEY=56cc30d0da706ccb84d642e3275da73543c71768
export COUNTLY_URL=https://cemex.count.ly
export SITE_BUSY=no
export HOST_EUROPE=localhost:57928
export HOST_AMERICA=localhost:57928
export COUNTLY_URL=https://cemex.count.ly
export SITE_BUSY=no
export SITE_DOMAIN=''
export CMX_LOYALTY_PROGRAM_URL='https://www.mycemex.cemex.com/'
export LANGUAGE='en_US'

if [ "$1" == "dev-ame" ]; then
    export API_HOST=https://uscldcnxapmd01.azure-api.net/
    export API_ORG=''
    export API_ENV=''
    export COUNTLY_KEY=56cc30d0da706ccb84d642e3275da73543c71768
    export TRANSLATE_URL=https://uscldcnxwaadmq01.azurewebsites.net/
    export ENV=nonprod
    export LANGUAGE='en_US'
fi

if [ "$1" == "dev2-ame" ]; then
    export API_HOST=https://uscldcnxapmsa01.azure-api.net/
    export API_ORG=''
    export API_ENV=''
    export COUNTLY_KEY=56cc30d0da706ccb84d642e3275da73543c71768
    export TRANSLATE_URL=https://uscldcnxwaadmq01.azurewebsites.net/
    export ENV=nonprod
    export LANGUAGE='en_US'
    export ROLLOUT_APP_KEY='5bb24a54bae7137e6c9de19e'
fi

if [ "$1" == "dev2-eu" ]; then
    export API_HOST=https://uscldcnxapmsa01.azure-api.net/
    export API_ORG=''
    export API_ENV=''
    export COUNTLY_KEY=56cc30d0da706ccb84d642e3275da73543c71768
    export TRANSLATE_URL=https://uscldcnxwaadmq01.azurewebsites.net/
    export ENV=nonprod
    export LANGUAGE='en_UK'
    export ROLLOUT_APP_KEY='5bb24a54bae7137e6c9de19e'
fi


if [ "$1" == "dev-eu" ]; then
    export API_HOST=https://uscldcnxapmd01.azure-api.net/
    export COUNTLY_KEY=56cc30d0da706ccb84d642e3275da73543c71768
    export TRANSLATE_URL=https://ukcldcnxwaadmq01.azurewebsites.net/
    export ENV=nonprod
    export LANGUAGE='en_GB'
fi

if [ "$1" == "qa-ame" ]; then
    export API_HOST=https://cemexqas.azure-api.net/
    export COUNTLY_KEY=b287fda9ffdf4d330741597ae2eff4cc47206260
    export TRANSLATE_URL=https://uscldcnxwaadmq01.azurewebsites.net/
    export ENV=nonprod
    export LANGUAGE='en_US'
    export ROLLOUT_APP_KEY='5bb24a5abae7137e6c9de1a1'
fi

if [ "$1" == "qa-eu" ]; then
    export API_HOST=https://cemexqas.azure-api.net/
    export COUNTLY_KEY=b287fda9ffdf4d330741597ae2eff4cc47206260
    export TRANSLATE_URL=https://ukcldcnxwaadmq01.azurewebsites.net/
    export ENV=nonprod
    export LANGUAGE='en_GB'
    export ROLLOUT_APP_KEY='5bb24a5abae7137e6c9de1a1'
fi

if [ "$1" == "prep" ]; then
    export API_HOST=https://uscldcnxapmpp01.azure-api.net/
    export COUNTLY_KEY=b287fda9ffdf4d330741597ae2eff4cc47206260
    export TRANSLATE_URL=http://configuration-console.cemexgo.com/
    export LANGUAGE='en_US'
fi

if [ "$1" == "prod" ]; then
    export API_HOST=https://www.cemexgo.com/api/
    export COUNTLY_KEY=a382584c57ca22a6040c9861b65a82af9539d561
    export TRANSLATE_URL=http://configuration-console.cemexgo.com/
fi

cd ClientApp/
gulp css
gulp minify

cd ..

printf ".NET Environment: ${CYAN}Development${NC}\n"
export ASPNETCORE_ENVIRONMENT=Development

printf "Restoring .NET ${CYAN}project.json${NC} dependencies\n"
dotnet restore

printf "Running .NET ${CYAN}server${NC}\n"
dotnet run