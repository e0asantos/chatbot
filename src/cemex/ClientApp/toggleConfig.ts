import * as Rox from 'rox-browser';

export const toggleConfig =  {
    useChatbot: new Rox.Flag(),
    useToggleFeatureButton: new Rox.Flag()
};
