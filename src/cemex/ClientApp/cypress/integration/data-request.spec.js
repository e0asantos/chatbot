describe('CEMEX Go APP', function () {
    it('starts the application', function () {
        cy.visit('http://localhost:57928/login');
    });
    it('Authenticate', function () {
        cy.get('#cmx-login-form-username-field').clear();
        cy.get('#cmx-login-form-username-field').type('da.us.qa@yopmail.com');
        cy.get('#cmx-login-form-password-field').clear();
        cy.get('#cmx-login-form-password-field').type('Amazon123!');
        cy.get('#cmx-login-form-submit-btn').click({ force: true });
        cy.wait(5000);
    });
    it('It is on Dashboard', function () {
        cy.url().should('eq', 'http://localhost:57928/dashboard');
    });
    it('Go to Profile', function () {
        cy.get('#cmx-header-profile-menu').click({ force: true });
        cy.get('#cmx-header-profile-link').click({ force: true });
        cy.wait(2000);
    });
    it('Click on My Information tab', function () {
        cy.get('#1').contains('My Information');
        cy.get('#1').should('have.class', 'nav-link');
        cy.get('#1').click({ force: true });
    });
    it('Go to Data Access Request', function () {
        cy.get('#cmx-btn-go-data-request-MYPROFILE').click({ force: true });
        cy.wait(1000);
        cy.url().should('eq', 'http://localhost:57928/profile/data-request');
    });
    it('Display jobsite list', function () {
        cy.get('#cmx-table-jobsites').click({ force: true });
    });
});