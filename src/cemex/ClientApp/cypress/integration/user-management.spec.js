describe('Dashboard', function () {
    it('Starts the application', function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
        });
        cy.visit('http://localhost:57928/login/');
    });
    it('Authenticate', function () {
        cy.get('#cmx-login-form-username-field').clear();
        cy.get('#cmx-login-form-username-field').type('da.us.qa@yopmail.com');
        cy.get('#cmx-login-form-password-field').clear();
        cy.get('#cmx-login-form-password-field').type('Amazon123!');
        cy.get('#cmx-login-form-submit-btn').click({ force: true });
        cy.wait(5000);
    });
    it('It is on Dashboard', function () {
        cy.url().should('eq', 'http://localhost:57928/dashboard');
    });
    it('Go to User Management', function () {
        cy.get('#cmx-card-app-UserProvisioning_App-DASHBOARD').click({ force: true });
        cy.url().should('eq', 'http://localhost:57928/user-management/inbox');
    });
    it('Select approved requests', function () {
        cy.get('#cmx-usermanagement-request-dpdown').click({ force: true });
        cy.get('#cmx-dropdown-item-status-a-USER-MANAGEMENT').click({ force: true });
        cy.get('#cmx-table-requests').click({ force: true });
    });
    it('Open request detail, should have status as approved', function () {
        cy.get('#cmx-btn-request-detail-0-USER-MANAGEMENT').click({ force: true });
        cy.get('#cmx-flag-status-request-detail').should('have.class', 'cmx-flag-status--success');
    });
    it('Close request detail modal', function () {
        cy.get('body > app-root > app-pages-auth > div > div > app-user-management > cmx-dialog:nth-child(3) > div:nth-child(2) > button > span:nth-child(1) ').should('have.class', 'cmx-icon-close');
        cy.get('body > app-root > app-pages-auth > div > div > app-user-management > cmx-dialog:nth-child(3) > div:nth-child(2) > button > span:nth-child(1) ').click({ force: true });
    });
    it('Click on User management tab', function () {
        cy.get('#1').contains('User Management');
        cy.get('#1').should('have.class', 'nav-link');
        cy.get('#1').click({ force: true });
    });
});