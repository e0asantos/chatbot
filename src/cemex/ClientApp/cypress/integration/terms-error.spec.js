describe('Login', function () {

  it('starts the application', function () {
    cy.clearCookies();
    cy.clearLocalStorage();
    cy.window().then((win) => {
      win.sessionStorage.clear();
    });
    cy.visit('http://localhost:57928/login/');
  });

  it('Authentication to app', function () {

    // Make an intentional 500 error for terms
    cy.server();
    cy.route({
      method: 'GET',
      url: 'v5/legal/agreements?regionId=&countryCode=US&category=',
      status: 500,
      response: { "agreements": [{ "AcceptanceAgreementId": 21, "AgreementVersionId": 98, "AgreementCategory": "LGL", "CountryCode": "US ", "RegionId": 0, "AgreementSign": false, "AgreementStatus": true, "AgreementVersion": "v1.0", "DocumentHash": "8305d849b", "DocumentPath": "usa/secm/", "DateCreate": "2018-09-11T20:07:51", "DateUpdate": "2018-09-11T20:07:51", "UserCreateCode": "cnxadm", "UserUpdateCode": "cnxadm", "IsSigned": false }, { "AcceptanceAgreementId": 28, "AgreementVersionId": 109, "AgreementCategory": "PRP", "CountryCode": "US ", "RegionId": 0, "AgreementSign": false, "AgreementStatus": true, "AgreementVersion": "v1.0", "DocumentHash": "8305d849c", "DocumentPath": "usa/secm/", "DateCreate": "2018-09-11T20:07:51", "DateUpdate": "2018-09-11T20:07:51", "UserCreateCode": "cnxadm", "UserUpdateCode": "cnxadm", "IsSigned": false }, { "AcceptanceAgreementId": 7, "AgreementVersionId": 87, "AgreementCategory": "TOU", "CountryCode": "US ", "RegionId": 0, "AgreementSign": true, "AgreementStatus": true, "AgreementVersion": "v1.0", "DocumentHash": "8305d849", "DocumentPath": "usa/secm/", "DateCreate": "2018-09-11T20:07:51", "DateUpdate": "2018-09-11T20:07:51", "UserCreateCode": "cnxadm", "UserUpdateCode": "cnxadm", "IsSigned": false, "DateSigned": "2018-09-11T22:56:49" }] }
    }).as('getLegals');

    cy.get('#cmx-login-form-username-field').type('da.us.qa@yopmail.com');
    cy.get('#cmx-login-form-password-field').type('Amazon123!');
    cy.get('#cmx-login-form-submit-btn').click({ force: true });
    cy.wait(3000);
  });

  it('should redirect to login and show issue in alert', function () {
    cy.url().should('contain', '/login');
    cy.get('#tou-issue-alert').should('exist');
  });

});
