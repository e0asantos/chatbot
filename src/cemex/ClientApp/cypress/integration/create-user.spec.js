describe('Dashboard', function () {
    it('Starts the application', function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
        });
        cy.visit('http://localhost:57928/login/');
    });
    it('Authenticate', function () {
        cy.get('#cmx-login-form-username-field').clear();
        cy.get('#cmx-login-form-username-field').type('da.us.qa@yopmail.com');
        cy.get('#cmx-login-form-password-field').clear();
        cy.get('#cmx-login-form-password-field').type('Amazon123!');
        cy.get('#cmx-login-form-submit-btn').click({ force: true });
        cy.wait(5000);
    });
    it('It is on Dashboard', function () {
        cy.url().should('eq', 'http://localhost:57928/dashboard');
    });
    it('Go to User Management', function () {
        cy.get('#cmx-card-app-UserProvisioning_App-DASHBOARD').click({ force: true });
        cy.url().should('eq', 'http://localhost:57928/user-management/inbox');
    });
    it('Go to Create User', function () {
        cy.get('#1').contains('User Management');
        cy.get('#1').should('have.class', 'nav-link');
        cy.get('#1').click({ force: true });
        cy.get('#cmx-btn-go-create-user-USER-MANAGEMENT').click({ force: true });
        cy.url().should('eq', 'http://localhost:57928/user-management/create-user');
    });
    it('Select customer', function () {
        cy.get('#cmx-createuser-dropdown-list').click({ force: true });
        cy.get('#cmx-dropdown-item-customer--CREATE-USER').click({ force: true });
    });
    it('Add name', function () {
        cy.get('#cmx-input-first-name-CREATE-USER').clear();
        cy.get('#cmx-input-first-name-CREATE-USER').type('John');
    });
    it('Add lastname', function () {
        cy.get('#cmx-input-last-name-CREATE-USER').clear();
        cy.get('#cmx-input-last-name-CREATE-USER').type('Doe');
    });
    it('Add position', function () {
        cy.get('#cmx-input-position-CREATE-USER').clear();
        cy.get('#cmx-input-position-CREATE-USER').type('Position');
    });
    it('Add email', function () {
        cy.get('#cmx-input-email-CREATE-USER').clear();
        cy.get('#cmx-input-email-CREATE-USER').type('mail@test.com');
    });
    it('Add phone', function () {
        cy.get('#cmx-input-phone-CREATE-USER').clear();
        cy.get('#cmx-input-phone-CREATE-USER').type('1554765235');
    });
    it('Add password', function () {
        cy.get('#cmx-input-temp-password-CREATE-USER').clear();
        cy.get('#cmx-input-temp-password-CREATE-USER').type('Gjrot4_d5-');
    });
    it('Click on Next if is enabled', function () {
        cy.get('#cmx-btn-next-CREATE-USER').click({ force: true });
    });
    it('Add new permission', function () {
        cy.get('#cmx-btn-add-permission-CREATE-USER').click({ force: true });
    });
    it('Select User Management Application', function () {
        cy.get('#cmx-dropdown-permission-application-dropdown').click({ force: true });
        cy.get('#cmx-item-app-UserProvisioning_App-CREATE-USER').contains('User Management');
        cy.get('#cmx-item-app-UserProvisioning_App-CREATE-USER').click({ force: true });
    });
    it('Select Creates Users role', function () {
        cy.get('#cmx-dropdown-permission-role-dropdown').click({ force: true });
        cy.get('#cmx-checkbox-role-SECMORGUSR-CREATE-USER').click({ force: true });
    });
    it('Click on Next if is enabled', function () {
        cy.get('#cmx-btn-next-CREATE-USER').click({ force: true });
    });
    it('Add customer', function () {
        cy.get('#cmx-btn-add-jobsites-CREATE-USER').click({ force: true });
    });
    it('Assign first customer', function () {
        cy.get('body > app-root > app-pages-auth > div > div > app-create-user > legal-entities-modal > cmx-dialog > div:nth-child(2) > div:nth-child(1) > cmx-dialog-body > div:nth-child(7) > customers-table > cmx-table > div:nth-child(2) > div:nth-child(2) > cmx-cell:nth-child(1) > cmx-checkbox > input ').check();
        cy.get('#cmx-btn-assign-legal-entities-modal').click({ force: true });
    });
    it('Interaction step 4', function () {
        cy.get('#1').contains('Locations');
        cy.get('#1').should('have.class', 'nav-link');
        cy.get('#1').click({ force: true });
    });
    it('Interaction step 5', function () {
        cy.get('#cmx-btn-add-jobsites-CREATE-USER').contains('Add Locations');
        cy.get('#cmx-btn-add-jobsites-CREATE-USER').should('have.class', 'cmx-button');
        cy.get('#cmx-btn-add-jobsites-CREATE-USER').should('have.class', 'cmx-ripple');
        cy.get('#cmx-btn-add-jobsites-CREATE-USER').click({ force: true });
    });
    it('Interaction step 6', function () {
    });
    it('Interaction step 6', function () {
        cy.get('body > app-root > app-pages-auth > div > div > app-create-user > jobsites-modal > cmx-dialog:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dialog-body > div:nth-child(6) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(1) > div > div > cmx-checkbox > input ').check();
    });
    it('Interaction step 7', function () {
        cy.get('body > app-root > app-pages-auth > div > div > app-create-user > jobsites-modal > cmx-dialog:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dialog-body > div:nth-child(6) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(1) > div > div > cmx-checkbox > input ').check();
    });
    it('Interaction step 8', function () {
        cy.get('body > app-root > app-pages-auth > div > div > app-create-user > jobsites-modal > cmx-dialog:nth-child(1) > div:nth-child(2) > div:nth-child(2) > cmx-dialog-footer > div > button:nth-child(2) > span ').contains('Assign');
        cy.get('body > app-root > app-pages-auth > div > div > app-create-user > jobsites-modal > cmx-dialog:nth-child(1) > div:nth-child(2) > div:nth-child(2) > cmx-dialog-footer > div > button:nth-child(2) > span ').click({ force: true });
    });
    it('Interaction step 9', function () {
        cy.get('#cmx-btn-next-CREATE-USER').contains('Next');
        cy.get('#cmx-btn-next-CREATE-USER').should('have.class', 'cmx-button');
        cy.get('#cmx-btn-next-CREATE-USER').should('have.class', 'cmx-ripple');
        cy.get('#cmx-btn-next-CREATE-USER').click({ force: true });
    });
});