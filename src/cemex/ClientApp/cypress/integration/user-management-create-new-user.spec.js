describe('CEMEX Go APP', function () {
    it('starts the application', function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
        });
        cy.visit('http://localhost:57928/dashboard');
    });
    it('Authentication - Fill username', function () {
        cy.get('#cmx-login-form-username-field').click({ force: true });
        cy.get('#cmx-login-form-username-field').clear();
        cy.get('#cmx-login-form-username-field').type('da.us.qa@yopmail.com');
    });
    it('Authentication - Fill password', function () {
        cy.get('#cmx-login-form-password-field').click({ force: true });
        cy.get('#cmx-login-form-password-field').clear();
        cy.get('#cmx-login-form-password-field').type('Amazon123!');
    });
    it('Authentication - Submit', function () {
        cy.get('#cmx-login-form-submit-btn').click({ force: true });
        cy.wait(3000);
    });
    it('Go to Dashboard', function () {
        cy.visit('http://localhost:57928/dashboard');
        cy.get('.layout').children().should('have.length', 2);
        cy.get('.layout').click({ force: true });
    });
    it('Go to User Management', function () {
        cy.get('#cmx-card-app-UserProvisioning_App-DASHBOARD').click({ force: true });
        cy.wait(1000);
    });
    it('Go to Create User', function () {
        cy.get('#1').contains('User Management');
        cy.get('#1').should('have.class', 'nav-link');
        cy.get('#1').click({ force: true });
        cy.wait(100);
        cy.get('#cmx-btn-go-create-user-USER-MANAGEMENT').click({ force: true });
    });
    it('Select customer', function () {
        cy.url().should('eq', 'http://localhost:57928/user-management/create-user');
        cy.get('#cmx-createuser-dropdown-list').click({ force: true });
        cy.wait(100);
        cy.get('#cmx-dropdown-item-customer--CREATE-USER').click({ force: true });
    });
    it('Add name', function () {
        cy.get('#cmx-input-firs-name-CREATE-USER').clear();
        cy.get('#cmx-input-firs-name-CREATE-USER').type('John');
    });
    it('Add lastname', function () {
        cy.get('#cmx-input-last-name-CREATE-USER').clear();
        cy.get('#cmx-input-last-name-CREATE-USER').type('Doe');
    });
    it('Add position', function () {
        cy.get('#cmx-input-position-CREATE-USER').clear();
        cy.get('#cmx-input-position-CREATE-USER').type('Position');
    });
    it('Add email', function () {
        cy.get('#cmx-input-email-CREATE-USER').clear();
        cy.get('#cmx-input-email-CREATE-USER').type('mail@test.com');
    });
    it('Add phone', function () {
        cy.get('#cmx-input-phone-CREATE-USER').clear();
        cy.get('#cmx-input-phone-CREATE-USER').type('1554765235');
    });
    it('Add password', function () {
        cy.get('#cmx-input-temp-password-CREATE-USER').clear();
        cy.get('#cmx-input-temp-password-CREATE-USER').type('Gjrot4_d5-');
    });
    it('Click on Next if is enabled', function () {
        cy.get('#cmx-btn-next-CREATE-USER').click({ force: true });
    });
    it('Add new permission', function () {
        cy.get('#cmx-btn-add-permission-CREATE-USER').click({ force: true });
    });
    it('Select User Management Application', function () {
        cy.get('#cmx-dropdown-permission-application-dropdown').click({ force: true });
        cy.get('#cmx-item-app-UserProvisioning_App-CREATE-USER').contains('User Management');
        cy.get('#cmx-item-app-UserProvisioning_App-CREATE-USER').click({ force: true });
    });
    it('Select Creates Users role', function () {
        cy.get('#cmx-dropdown-permission-role-dropdown').click({ force: true });
        cy.get('#cmx-checkbox-role-SECMORGUSR-CREATE-USER').click({ force: true });
    });
    it('Click on Next if is enabled', function () {
        cy.get('#cmx-btn-next-CREATE-USER').click({ force: true });
    });
});