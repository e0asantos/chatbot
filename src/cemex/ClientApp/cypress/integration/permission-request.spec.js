describe('CEMEX Go APP', function () {
    it('starts the application', function () {
        cy.visit('http://localhost:57928/login');
    });
    it('Authenticate', function () {
        cy.get('#cmx-login-form-username-field').clear();
        cy.get('#cmx-login-form-username-field').type('da.us.qa@yopmail.com');
        cy.get('#cmx-login-form-password-field').clear();
        cy.get('#cmx-login-form-password-field').type('Amazon123!');
        cy.get('#cmx-login-form-submit-btn').click({ force: true });
        cy.wait(5000);
    });
    it('It is on Dashboard', function () {
        cy.url().should('eq', 'http://localhost:57928/dashboard');
    });
    it('Go to Profile', function () {
        cy.get('#cmx-header-profile-menu').click({ force: true });
        cy.get('#cmx-header-profile-link').click({ force: true });
        cy.wait(2000);
    });
    it('Go to Permission Request', function () {
        cy.get('#cmx-btn-go-permission-request-MYPROFILE').click({ force: true });
        cy.wait(1000);
        cy.url().should('eq', 'http://localhost:57928/profile/permission-request');
    });
    it('Add new permission', function () {
        cy.get('#cmx-btn-add-permission-PERMISSION-REQUEST').click({ force: true });
    });
    it('Select INVOICES AND PAYMENTS', function () {
        cy.get('#cmx-dropdown-add-permission-application-dropdown').click({ force: true });
        cy.get('#cmx-item-app-INVOICE_PAYMENTS-PERMISSION-REQUEST').contains('Invoice and Payments');
        cy.get('#cmx-item-app-INVOICE_PAYMENTS-PERMISSION-REQUEST').click({ force: true });
    });
    it('Validate no roles for INVOICES AND PAYMENTS', function () {
        cy.get('#cmx-dropdown-add-permission-role-dropdown').contains('No roles available');
    });
    it('Remove application', function () {
        cy.get('#cmx-btn-remove-application-1-PERMISSION-REQUEST').click({ force: true });
    });
});