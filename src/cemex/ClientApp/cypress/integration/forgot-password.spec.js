describe('Forgot Password', function () {
    it('starts the application', function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
        });
        cy.visit('http://localhost:57928/forgotPassword/');
    });
    it('Fill user email', function () {
        cy.get('#cmx-forgot-password-email-input').click({ force: true });
        cy.get('#cmx-forgot-password-email-input').clear();
        cy.get('#cmx-forgot-password-email-input').type('da.us.qa@yopmail.com');
    });
    it('Click on Send button', function () {
        cy.get('#cmx-forgot-password-submit-btn').click({ force: true });
        cy.wait(1000);
    });
    it('Success modal is displayed', function () {
        cy.get('#cmx-forgot-password-success-modal-icon').should('have.class', 'modal-success-icon');
        cy.wait(1000);
    });
    it('Close modal success modal', function () {
        cy.get('#cmx-panel-close-btn').click({ force: true });
    });
});