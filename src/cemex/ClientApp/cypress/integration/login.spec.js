describe('Login', function () {
    it('starts the application', function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
        });
        cy.visit('http://localhost:57928/login/');
    });
    it('Interaction step 0', function () {
        cy.get('#cmx-login-language-dropdown').click({ force: true });
        cy.wait(1000);
    });
    it('Interaction step 1', function () {
        cy.get('#cmx-login-language-item-es_MX').click({ force: true });
        cy.wait(1000);
    });
    it('Interaction step 2', function () {
        cy.get('#cmx-login-language-dropdown').click({ force: true });
        cy.wait(1000);
        cy.get('#cmx-login-language-item-en_US').click({ force: true });
    });
    it('Interaction step 3', function () {
        cy.get('#cmx-login-language-dropdown').click({ force: true });
    });
    it('Interaction step 4', function () {
        cy.get('#cmx-login-form-forgot-password-link').click({ force: true });
        cy.wait(1000);
        cy.url().should('eq', 'http://localhost:57928/forgotPassword');
    });
    it('Interaction step 5', function () {
        cy.get('#cmx-panel-close-btn').click({ force: true });
        cy.wait(1000);
        cy.url().should('eq', 'http://localhost:57928/login');
    });
    it('Authentication - Fill username', function () {
        cy.get('#cmx-login-form-username-field').click({ force: true });
        cy.get('#cmx-login-form-username-field').clear();
        cy.get('#cmx-login-form-username-field').type('da.us.qa@yopmail.com');
    });
    it('Authentication - Fill password', function () {
        cy.get('#cmx-login-form-password-field').click({ force: true });
        cy.get('#cmx-login-form-password-field').clear();
        cy.get('#cmx-login-form-password-field').type('Amazon123!');
    });
    it('Authentication - Submit', function () {
        cy.get('#cmx-login-form-submit-btn').click({ force: true });
        cy.wait(3000);
    });
    it('Go to Dashboard', function () {
        cy.url().should('eq', 'http://localhost:57928/dashboard');
    });
});