describe('Dashboard', function () {
    it('Starts the application', function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
        });
        cy.visit('http://localhost:57928/login/');
    });
    it('Authenticate', function () {
        cy.get('#cmx-login-form-username-field').clear();
        cy.get('#cmx-login-form-username-field').type('da.us.qa@yopmail.com');
        cy.get('#cmx-login-form-password-field').clear();
        cy.get('#cmx-login-form-password-field').type('Amazon123!');
        cy.get('#cmx-login-form-submit-btn').click({ force: true });
        cy.wait(5000);
    });
    it('It is on Dashboard', function () {
        cy.url().should('eq', 'http://localhost:57928/dashboard');
    });
    it('Toggle sidebar', function () {
        cy.get('.cmx-icon-left-double.font-true-white').click({ force: true });
        cy.get('.cmx-navbar__trigger').children().should('have.length', 2)
        cy.get('.cmx-navbar__trigger').click({ force: true });
    });
    it('Toggle menu option ( Commercial conditions )', function () {
        cy.get('#cmx-sidebar-app-QuotationPricing_App').click({ force: true });
        cy.wait(2000);
        cy.get('#cmx-sidebar-app-QuotationPricing_App').click({ force: true });
    });
    it('Open/close profile menu', function () {
        cy.get('#cmx-header-profile-menu').click({ force: true });
        cy.wait(2000);
        cy.get('#cmx-header-profile-menu').click({ force: true });
    });
    it('Go to Profile and back to Dashboard by clicking header logo.', function () {
        cy.get('#cmx-header-profile-menu').click({ force: true });
        cy.get('#cmx-header-profile-link').click({ force: true });
        cy.url().should('contain', '/profile');
        cy.wait(2000);
        cy.get('#cmx-header-logo-link').click({ force: true });
        cy.url().should('contain', '/dashboard');
    });
    it('Go to Settings and back to Dashboard by clicking header logo.', function () {
        cy.get('#cmx-header-profile-menu').click({ force: true });
        cy.get('#cmx-header-settings-link').click({ force: true });
        cy.url().should('contain', '/settings');
        cy.wait(2000);
        cy.get('#cmx-header-logo-link').click({ force: true });
        cy.url().should('contain', '/dashboard');
    });
    it('Log out.', function () {
        cy.get('#cmx-header-profile-menu').click({ force: true });
        cy.get('#cmx-header-logout-link').click({ force: true });
        cy.url().should('contain', '/login');
    });
});