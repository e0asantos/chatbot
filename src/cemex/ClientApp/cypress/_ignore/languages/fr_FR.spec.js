describe('LOGIN SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'fr_FR');
        });
        cy.visit('http://localhost:57928/login/');
    });
    it('should contain "Français" in language dropdown', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').contains('Français');
    });
    it('should contain "Bienvenue" for welcome message.', function () {
        cy.get('.login__content-title').contains('Bienvenue');
    });
    it('should contain "Nom d\'utilisateur" in the username form label', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > label ').contains('Nom d\'utilisateur');
    });
    it('should contain "Saisissez votre identifiant" on username input placeholder', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > input ').should('have.attr', 'placeholder', 'Saisissez votre identifiant');
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > input ').type('foo');
    });
    it('should contain "Mot de passe" in the password form label', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > label ').contains('Mot de passe');
    });
    it('should contain "Saisissez votre mot de passe" on password input placeholder', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > input ').should('have.attr', 'placeholder', 'Saisissez votre mot de passe');
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > input ').type('foo');
    });
    it('should contain "Mot de passe oublié?" to retrieve users password', function () {
        cy.get('[data-tid="cmx-login-form-forgot-password-link"]').contains('Mot de passe oublié?');
    });
    it('should contain "Connexion" as submit button', function () {
        cy.get('[data-tid="cmx-login-form-submit-btn"]').contains('Connexion');
        // not working, do not show the error message, this could be a cypress bug.
        cy.get('[data-tid="cmx-login-form"]').submit();
    });
    it('should contain "Le nom d\'utilisateur ou le mot de passe que vous avez écrit est incorrect. Essayez de nouveau." when user or password incorrect', function () {
        // Not working
        cy.get('.login__content-message-error').contains('Le nom d\'utilisateur ou le mot de passe que vous avez écrit est incorrect. Essayez de nouveau.');
    });
});

describe('FORGOT PASSWORD SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'fr_FR');
        });
        cy.visit('http://localhost:57928/forgotPassword/');
    });
    it('should contain "Mot de passe oublié?" as page title', function () {
        cy.get('.custom-panel__content__title').contains('Mot de passe oublié?');
    });
    it('should contain correct text as page description', function () {
        cy.get('.custom-panel__content__instruction').contains('Indiquez votre e-mail et nous vous enverrons un lien pour réinitialiser votre mot de passe.');
    });
    it('should contain "E-mail" for email label', function () {
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > label ').contains('E-mail');
    });
    it('should contain "Saisissez e-mail" for email input placeholder', function () {
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > input ').should('have.attr', 'placeholder', 'Saisissez e-mail');
    });
    it('should contain "Format e-mail incorrect" for email format error.', function () {
        cy.get('#cmx-forgot-password-email-input').type('foo');
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > span > error-message ').contains('Format e-mail incorrect');
    });
    it('should contain "E-mail requis" when no text is in the input', function () {
        cy.get('#cmx-forgot-password-email-input').clear();
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > span > error-message ').contains('E-mail requis');
    });
    it('should contain "Envoyer" as sumbit button', function () {
        // Cannot access cross origin frames. :(
        // cy.get('iframe').then(function($iframe) {
        //     const $body = $iframe.contents().find('body')
        //     cy
        //     .wrap($body)
        //     .find('#recaptcha-anchor-label > span')
        //     .contains('No soy un robot')
        // });
        cy.get('#cmx-forgot-password-submit-btn').contains('Envoyer');
    });
});

function login(window) {
    cy.request({
        method: 'POST',
        url: 'https://cemexqas.azure-api.net/v2/secm/oam/oauth2/token',
        form: true,
        body: {
            grant_type: 'password',
            scope: 'security',
            username: 'da.us.qa@yopmail.com',
            password: 'Amazon123!'
        }
    })
    .then(resp => {
        window.sessionStorage.setItem('access_token', resp.body.oauth2.access_token);
        window.sessionStorage.setItem('applications', JSON.stringify(resp.body.applications));
        window.sessionStorage.setItem('auth_token', resp.body.oauth2.access_token);
        window.sessionStorage.setItem('country', resp.body.country);
        window.sessionStorage.setItem('expires_in', resp.body.oauth2.expires_in);
        window.sessionStorage.setItem('jwt', resp.body.jwt);
        window.sessionStorage.setItem('language', `en_${resp.body.country}`);
        window.sessionStorage.setItem('refresh_token', resp.body.oauth2.refresh_token);
        window.sessionStorage.setItem('region', resp.body.oauth2.region);
        window.sessionStorage.setItem('role', resp.body.role);
        window.sessionStorage.setItem('token_data', JSON.stringify(resp.body));
        window.sessionStorage.setItem('userInfo', JSON.stringify(resp.body));
        window.sessionStorage.setItem('user_applications', JSON.stringify(resp.body.applications));
        window.sessionStorage.setItem('user_customer', JSON.stringify(resp.body.customer));
        window.sessionStorage.setItem('user_profile', JSON.stringify(resp.body.profile));
        window.sessionStorage.setItem('username', resp.body.profile.userAccount);
    });
}

describe('USER MANAGEMENT SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'fr_FR');
            login(win);
        });
        cy.visit('http://localhost:57928/user-management/inbox/');
    });
    it('should contain "Gestion des utilisateurs" as last breadcrumb item', function () {
        cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-breadcrumb > ul > li:nth-child(1) ').contains('Accueil');
        cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-breadcrumb > ul > li:nth-child(2) ').contains('Gestion des utilisateurs');
    });
    it('should contain "Gestion des utilisateurs" as page title', function () {
        cy.get('body > app > app-pages > div > div > div > user-management > div > h1 ').contains('Gestion des utilisateurs');
    });
    it('should contain "Demandes en attente" for pending requests.', function () {
        cy.get('#cmx-user-pending-request-USER-MANAFEMENT').contains('Demandes en attente');
    });
    it('should contain "Nombre d\'utilisateurs" for total users', function () {
        cy.get('body > app > app-pages > div > div > div > user-management > div > div:nth-child(4) > div > div:nth-child(2) > div > div:nth-child(2) > div:nth-child(2) ').contains('Nombre d\'utilisateurs');
    });
    it('should contain "Suivi des demandes" as first page tab', function () {
        cy.get('ul.nav-tabs > li:nth-child(1)').contains('Suivi des demandes');
    });
    it('should contain "Gestion des utilisateurs" as second page tab', function () {
        cy.get('ul.nav-tabs > li:nth-child(2)').contains('Gestion des utilisateurs');
    });

    describe('Inbox Section', function() {
        it('should contain "En cours" as first dropdown filter option', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').click({force:true});
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div:nth-child(2) > div > cmx-dropdown-item:nth-child(1) > div').contains('En cours');
        });
        it('should contain "Approuvée" as second dropdown filter option', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').click({force:true});
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div:nth-child(2) > div > cmx-dropdown-item:nth-child(2) > div').contains('Approuvée');
        });
        it('should contain "Rejetée" as third dropdown filter option', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').click({force:true});
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div:nth-child(2) > div > cmx-dropdown-item:nth-child(3) > div').contains('Rejetée');
        });
        it('should contain "Rechercher par utilisateur ou entreprise" on filter input placeholder', function () {
            cy.get('#cmx-input-search-request-USER-MANAGEMENT').should('have.attr', 'placeholder', 'Rechercher par utilisateur ou entreprise');
        });
        it('should contain "Statut" as first table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(1) > div > div ').contains('Statut');
        });
        it('should contain "N° demande" as second table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('N° demande');
        });
        it('should contain "Type de demande" as third table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('Type de demande');
        });
        it('should contain "Demandé par" as fourth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Demandé par');
        });
        it('should contain "Compte client" as fifth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('Compte client');
        });
        it('should contain "Date de la demande" as third table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(6) > div > div ').contains('Date de la demande');
        });

        describe('Request Dialog', function() {
            it('should contain "Données Request" as title when opening a request detail', function () {
                cy.get('#cmx-btn-request-detail-0-USER-MANAGEMENT').click({ force: true });
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(1) > div:nth-child(1) > h2 ').contains('Données Request');
            });
            it('should contains "Date créée" for request created date.', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(1) > div:nth-child(2) > div > div:nth-child(1) > h6 ').contains('Date créée');
            });
            it('should contains "N° demande" for request id.', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(1) > div:nth-child(2) > div > div:nth-child(2) > h6 ').contains('N° demande');
            });
            it('should contain "Nom" as first table column', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(5) > table > thead > tr > th:nth-child(1) ').contains('Nom');
            });
            it('should contain "Code chantier" as second table column', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(5) > table > thead > tr > th:nth-child(2) ').contains('Code chantier');
            });
            it('should contain "Rejeter" for reject button', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(3) > div > cmx-dialog-footer > div > button:nth-child(1) ').contains('Rejeter');
            });
            it('should contain "Approuver" for approve button', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(3) > div > cmx-dialog-footer > div > button:nth-child(2) ').contains('Approuver');
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > button > span:nth-child(1) ').click({force:true});
            });
        });
    })

    describe('Main Section', function() {
        it('should contain "Rechercher par utilisateur, entreprise ou e-mail" on filter input placeholder', function () {
            cy.get('div#1.nav-link').click({force: true});
            cy.get('#cmx-input-search-user-USER-MANAGEMENT').should('have.attr', 'placeholder', 'Rechercher par utilisateur, entreprise ou e-mail');
        });
        it('should contain "Nom d\'utilisateur" as first table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(1) > div > div ').contains('Nom d\'utilisateur');
        });
        it('should contain "Compte client" as second table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Compte client');
        });
        it('should contain "E-mail" as third table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('E-mail');
        });
        it('should contain "Numéro de téléphone" as fourth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Numéro de téléphone');
        });
        it('should contain "Accès aux modules" as fifth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('Accès aux modules');
        });
        it('should contain "Créer un nouvel utilisateur" for user creation button.', function () {
            cy.get('#cmx-btn-go-create-user-USER-MANAGEMENT').contains('Créer un nouvel utilisateur');
        });
    });
});

describe('EDIT USER SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'fr_FR');
        });
        cy.visit('http://localhost:57928/user-management/edit-user?id=omegaprodinterclerk@mailinator.com/');
    });
    it('should contain "Profil de l\'utilisateur" as last breadcrumb item.', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > cmx-breadcrumb > ul > li:nth-child(3) ').contains('Profil de l\'utilisateur');
    });
    it('should contain "Profil de l\'utilisateur" as page title', function () {
        cy.get('#cmx-edituser-pagetitle-lbl').contains('Profil de l\'utilisateur');
    });
    it('should contain "Numéro de téléphone" as user phone number', function () {
        cy.get('#cmx-profile-box-phonenumber-lbl').contains('Numéro de téléphone');
    });
    it('should contain "E-mail" as user email', function () {
        cy.get('#cmx-profile-box-email-lbl').contains('E-mail');
    });
    it('should contain "Entreprise" as user company', function () {
        cy.get('#cmx-profile-box-company-lbl').contains('Entreprise');
    });
    it('should contain "Pays" as user company', function () {
        cy.get('#cmx-profile-box-country-lbl').contains('Pays');
    });
    it('should contain "Code client" for custumer number.', function () {
        cy.get('#cmx-profile-box-customer-code-title-EDIT-USER').contains('Code client');
    });
    it('should contain "Désactiver utilisateur" for disable user button', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > profile-box > div > div:nth-child(3) > div:nth-child(2) > div > a:nth-child(1) > span ').contains('Désactiver utilisateur');
    });
    it('should contain "Historique des actions" for action log button', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > profile-box > div > div:nth-child(3) > div:nth-child(2) > div > a:nth-child(2) > span ').contains('Historique des actions');
    });
    it('should contain "Reset password" for reset password button', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > profile-box > div > div:nth-child(3) > div:nth-child(2) > div > a:nth-child(3) > span ').contains('notworking');
    });
    describe('Access to Applications Tab', function() {
        it('should contain "Accès aux modules" for first page tab', function () {
            cy.get('#0').contains('Accès aux modules');
        });
        it('should contain "Attribuer un accès" for assign accesses button', function () {
            cy.get('#cmx-btn-add-permission-EDIT-USER').contains('Attribuer un accès');
        });
        it('should contain "Envoyer" for assign accesses button', function () {
            cy.get('#cmx-btn-submit-EDIT-USER').contains('Envoyer');
        });
    });
    describe('Information Tab', function() {
        it('should contain "Informations" for second page tab', function () {
            cy.get('#1').contains('Informations');
        });
        it('should contain "Rechercher par nom" as input PH', function() {
            cy.get('#1').click({force: true});
            cy.get('#cmx-input-search-customer-EDIT-USER').should('have.attr', 'placeholder', 'Rechercher par nom');
        });
        it('should contain "Attribuer accès" for assign access button', function() {
            cy.get('#cmx-btn-data-access-EDIT-USER').contains('Attribuer accès');
        });
    });
});

describe('CREATE USER SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'fr_FR');
        });
        cy.visit('http://localhost:57928/user-management/create-user/');
    });

    it('should contain "Créer un nouvel utilisateur" in last breadcrumb item', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-breadcrumb > ul > li:nth-child(3) ').contains('Créer un nouvel utilisateur');
    });
    it('should contain "Coordonnées" as first process step', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(1) ').contains('Coordonnées');
    });
    it('should contain "Accès aux modules" as second process step', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(2) ').contains('Accès aux modules');
    });
    it('should contain "Accès aux comptes clients et chantiers" as third process step', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(3) ').contains('Accès aux comptes clients et chantiers');
    });
    it('should contain "Récapitulatif" as fourth process step  ', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(4) ').contains('Récapitulatif');
    });

    describe('Contact Information Section', function() {
        it('should contain "Code client" as first form input label and "Select client number" as PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > label ').contains('Code client');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > div > span ').contains('Sélectionnez un code client');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > div > span ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > div:nth-child(3) > div:nth-child(1) > div > input ').should('have.attr', 'placeholder', 'Sélectionnez un code client');
            cy.get('#cmx-dropdown-item-customer--CREATE-USER').click({force:true});
        });
        it('should contain "Entreprise" as second form input label', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(2) > cmx-form-field > label ').contains('Entreprise');
        });
        it('should contain "Pays" as third form input label', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(3) > cmx-form-field > label ').contains('Pays');
        });
        it('should contain "Prénom" as fourth form input label and "Ingrese nombre" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(1) > cmx-form-field > label ').contains('Prénom');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(1) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Saisissez le prénom');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(1) > cmx-form-field > input ').type('foo');
        });
        it('should contain "Nom de famille" as fifth form input label and "Ingrese apellido" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(2) > cmx-form-field > label ').contains('Nom de famille');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(2) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Saisissez le nom de famille');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(2) > cmx-form-field > input ').type('foo');
        });
        it('should contain "Fonction" as sixth form input label and "Ingrese rol" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(3) > cmx-form-field > label ').contains('Fonction');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(3) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Saisissez la fonction');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(3) > cmx-form-field > input ').type('foo');
        });
        it('should contain "E-mail" as seventh form input label and "Ingrese correo electrónico" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(1) > cmx-form-field > label ').contains('E-mail');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(1) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Saisissez l\'e-mail');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(1) > cmx-form-field > input ').type('foo@gmail.com');
        });
        it('should contain "Téléphone" as eighth form input label and "Ingrese teléfono" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(2) > cmx-form-field > label ').contains('Téléphone');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(2) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Saisissez le téléphone');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(2) > cmx-form-field > input ').type('123');
        });
        it('should contain "Mot de passe temporaire" as nineth input label, "Ingrese contraseña temporal" as input PH and correct tooltip message', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(3) > div > label ').contains('Mot de passe temporaire');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(3) > div > input ').should('have.attr', 'placeholder', 'Saisissez le mot de passe temporaire');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(3) > div > input ').type('foobazaO34?');
            cy.get('#cmx-tooltip-temp-password-CREATE-USER > span ').should('have.attr', 'aria-label', 'Votre nouveau mot de passe doit avoir entre 8 et 20 caractères et il est recommandé d\'y inclure des majuscules, des minuscules et des chiffres.');
        });
        it('should contain "Suivant" for submit button', function () {
            cy.get('#cmx-btn-next-CREATE-USER').contains('Suivant');
            cy.get('#cmx-btn-next-CREATE-USER').click({force: true});
        });
    });

    describe('Assign Applications and Roles', function() {
        it('should contain "Coordonnées" as page title', function () {
            cy.get('.contact-info__title').contains('Coordonnées');
        });
        it('should contain "Nom" as first column', function () {
            cy.get('#cmx-contact-info-name-lbl').contains('Nom');
        });
        it('should contain "Fonction" as second column', function () {
            cy.get('#cmx-contact-info-position-lbl').contains('Fonction');
        });
        it('should contain "Compte client" as third column', function () {
            cy.get('#cmx-contact-info-company-lbl').contains('Compte client');
        });
        it('should contain "Pays" as fourth column', function () {
            cy.get('#cmx-contact-info-country-lbl').contains('Pays');
        });
        it('should contain "E-mail" as fifth column', function () {
            cy.get('#cmx-contact-info-email-lbl').contains('E-mail');
        });
        it('should contain "Numéro de téléphone" as sixth column', function () {
            cy.get('#cmx-contact-info-phone-lbl').contains('Numéro de téléphone');
        });
        it('should contain "Attribuer un accès" as assign perm button', function () {
            cy.get('#cmx-btn-add-permission-CREATE-USER').contains('Attribuer un accès');
            cy.get('#cmx-btn-add-permission-CREATE-USER').click({ force: true });
        });
        it('should contain "Sélectionnez le module" as default perm option key', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').contains('Sélectionnez le module');
        });
        it('should contain "Sélectionnez les rôles" as default perm option value', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').contains('Sélectionnez les rôles');
        });

        it('should contain "Suivi" for track key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-Foreman_App-CREATE-USER').contains('Suivi');
            cy.get('#cmx-item-app-Foreman_App-CREATE-USER').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Affichage');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Modification des commandes en cours');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Informations des clients" for customer info key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-CustomerInfo_App-CREATE-USER').contains('Informations des clients');
            cy.get('#cmx-item-app-CustomerInfo_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click();
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option > cmx-checkbox > label ').contains('Création de points de livraison');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Mes conditions commerciales" for commercial cond key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-QuotationPricing_App-CREATE-USER').contains('Mes conditions commerciales');
            cy.get('#cmx-item-app-QuotationPricing_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Acheteur');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Affichage des demandes d\'offre de prix');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(3) > cmx-checkbox > label ').contains('Créer/Accepter');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(4) > cmx-checkbox > label ').contains('Termes et Conditions spécifiques');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(5) > cmx-checkbox > label ').contains('Conditions Générales d’Utilisation');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Mes commandes" for order and prod key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-OrderProductCat_App-CREATE-USER').contains('Mes commandes');
            cy.get('#cmx-item-app-OrderProductCat_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Création de commandes');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Créer des chantiers - Chef de chantier');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(3) > cmx-checkbox > label ').contains('Créer des commandes - Responsable achats');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Gestion des utilisateurs" for user manage key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-UserProvisioning_App-CREATE-USER').contains('Gestion des utilisateurs');
            cy.get('#cmx-item-app-UserProvisioning_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option > cmx-checkbox > label ').contains('Création et gestion des utilisateurs');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Factures et paiements" for invoice and pay key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-INVOICE_PAYMENTS-CREATE-USER').contains('Factures et paiements');
            cy.get('#cmx-item-app-INVOICE_PAYMENTS-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Comptable fournisseurs');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Responsable comptabilité fournisseurs');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(3) > cmx-checkbox > label ').contains('Comptable fournisseurs (paiements anticipés)');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(4) > cmx-checkbox > label ').contains('Paiements');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(5) > cmx-checkbox > label ').contains('Rapprochement');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(6) > cmx-checkbox > label ').contains('Accès à la demande de PRIVILÈGES');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').click({ force: true });
            cy.get('#cmx-btn-next-CREATE-USER').click({force: true});
        });
    });

    describe('Assign Data', function() {
        it('should contain "Accès aux comptes clients et chantiers" as page title', function () {
            cy.get('#cmx-createuser-pagetitle-lbl').contains('Accès aux comptes clients et chantiers');
        });

        describe('Legal Entities Section', function() {
            it('should contain "Comptes clients" as first page tab', function () {
                cy.get('#0').contains('Comptes clients');
                cy.get('#0').click({ force: true });
            });
            it('should contain "Nom" as first column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Nom');
            });
            it('should contain "Code client" as second column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('Code client');
            });
            it('should contain "Région / adresse" as third column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Région / adresse');
            });
            it('should contain "SIRET" as fifth column', function(){
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('SIRET');
            });
            it('should contain "Numéro de téléphone" as fourth column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(6) > div > div ').contains('Numéro de téléphone');
            });
            it('should contain "E-mail" as sixth column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(7) > div > div ').contains('E-mail');
            });
            it('should contain "Pays" as seventh column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(8) > div > div ').contains('Pays');
            });
            it('should contain "Rechercher par client ou région" as search input PH', function () {
                cy.get('#cmx-input-search-customer-CREATE-USER').should('have.attr', 'placeholder', 'Rechercher par client ou région');
            });
        });

        describe('Locations Section', function() {
            it('should contain "Chantiers" as second page tab', function () {
                cy.get('#1').contains('Chantiers');
                cy.get('#1').click({ force: true });
            });
            it('should contain "Nom" as first column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Nom');
            });
            it('should contain "Code chantier" as second column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('Code chantier');
            });
            it('should contain "Région / adresse" as third column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Région / adresse');
            });
            it('should contain "Compte client" as fourth column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('Compte client');
            });
            it('should contain "Rechercher par client ou chantier" as filter PH', function () {
                cy.get('#cmx-input-search-jobsite-CREATE-USER').should('have.attr', 'placeholder', 'Rechercher par client ou chantier');
                cy.get('#cmx-btn-next-CREATE-USER').click({force: true});
            });
        });
    });

    describe('Summary', function() {
        it('should contain "Coordonnées" as first table title', function () {
            cy.get('.contact-info__title').contains('Coordonnées');
        });
        it('should contain "Nom" as first column of first table', function () {
            cy.get('#cmx-contact-info-name-lbl').contains('Nom');
        });
        it('should contain "Fonction" as second column of first table', function () {
            cy.get('#cmx-contact-info-position-lbl').contains('Fonction');
        });
        it('should contain "Compte client" as third column of first table', function () {
            cy.get('#cmx-contact-info-company-lbl').contains('Compte client');
        });
        it('should contain "Pays" as fourth column of first table', function () {
            cy.get('#cmx-contact-info-country-lbl').contains('Pays');
        });
        it('should contain "E-mail" as fifth column of first table', function () {
            cy.get('#cmx-contact-info-email-lbl').contains('E-mail');
        });
        it('should contain "Numéro de téléphone" as sixth column of first table', function () {
            cy.get('#cmx-contact-info-phone-lbl').contains('Numéro de téléphone');
        });
        it('should contain "Accès aux données" as title of second table', function () {
            cy.get('#cmx-title-access-data-CREATE-USER').contains('Accès aux données');
        });
        it('should contain "Comptes clients" as subtitle of second table', function () {
            cy.get('#cmx-title-customer-CREATE-USER').contains('Comptes clients');
        });
        it('should contain "Code" as first table column', function () {
            cy.get('#cmx-title-customer-code-CREATE-USER').contains('Code');
        });
        it('should contain "Nom" as second table column', function () {
            cy.get('#cmx-title-customer-name-CREATE-USER').contains('Nom');
        });
        it('should contain "Envoyer" for last step submit button', function () {
            cy.get('#cmx-btn-submit-CREATE-USER').contains('Envoyer');
        });
    });

});

