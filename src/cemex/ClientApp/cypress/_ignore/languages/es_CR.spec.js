describe('LOGIN SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'es_CR');
        });
        cy.visit('http://localhost:57928/login/');
    });
    it('should contain "Español Costa Rica" in language dropdown', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').contains('Español México');
    });
    it('should contain "Bienvenido" for welcome message.', function () {
        cy.get('.login__content-title').contains('Bienvenido');
    });
    it('should contain "Usuario" in the username form label', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > label ').contains('Usuario');
    });
    it('should contain "Ingrese su usuario" on username input placeholder', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > input ').should('have.attr', 'placeholder', 'Ingrese su usuario');
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > input ').type('foo');
    });
    it('should contain "Contraseña" in the password form label', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > label ').contains('Contraseña');
    });
    it('should contain "Ingrese su contraseña" on password input placeholder', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > input ').should('have.attr', 'placeholder', 'Ingrese su contraseña');
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > input ').type('foo');
    });
    it('should contain link text "¿Olvidó su contraseña?" to retrieve users password', function () {
        cy.get('[data-tid="cmx-login-form-forgot-password-link"]').contains('¿Olvidó su contraseña?');
    });
    it('should contain "Iniciar Sesión" as submit button', function () {
        cy.get('[data-tid="cmx-login-form-submit-btn"]').contains('Iniciar Sesión');
        // not working, do not show the error message, this could be a cypress bug.
        // cy.get('[data-tid="cmx-login-form"]').submit({force: true});
    });
    it('should contain "The user or password you entered are incorrect. Please try again." when user or password incorrect. **', function () {
        // Not working
        // cy.get('.login__content-message-error').contains('The user or password you entered are incorrect. Please try again.');
    });
});

describe('FORGOT PASSWORD SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'es_CR');
        });
        cy.visit('http://localhost:57928/forgotPassword/');
    });
    it('should contain "¿Olvidó su contraseña?" as page title', function () {
        cy.get('.custom-panel__content__title').contains('¿Olvidó su contraseña?');
    });
    it('should contain correct text as page description', function () {
        cy.get('.custom-panel__content__instruction').contains('Ingrese su dirección de correo electrónico y le enviaremos una liga para restablecer su contraseña.');
    });
    it('should contain "Correo electrónico" for email label', function () {
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > label ').contains('Correo electrónico');
    });
    it('should contain "Ingresa correo electrónico" for email input placeholder', function () {
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > input ').should('have.attr', 'placeholder', 'Ingresa correo electrónico');
    });
    it('should contain "Formato de correo electrónico inválido" for email format error.', function () {
        cy.get('#cmx-forgot-password-email-input').type('foo');
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > span > error-message ').contains('Formato de correo electrónico inválido');
    });
    it('should contain "Correo electrónico es requerido" when no text is in the input', function () {
        cy.get('#cmx-forgot-password-email-input').clear();
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > span > error-message ').contains('Correo electrónico es requerido');
    });
    it('should contain "Enviar" as sumbit button', function () {
        // Cannot access cross origin frames. :(
        // cy.get('iframe').then(function($iframe) {
        //     const $body = $iframe.contents().find('body')
        //     cy
        //     .wrap($body)
        //     .find('#recaptcha-anchor-label > span')
        //     .contains('No soy un robot')
        // });
        cy.get('#cmx-forgot-password-submit-btn').contains('Enviar');
    });
});

function login(window) {
    cy.request({
        method: 'POST',
        url: 'https://cemexqas.azure-api.net/v2/secm/oam/oauth2/token',
        form: true,
        body: {
            grant_type: 'password',
            scope: 'security',
            username: 'da.us.qa@yopmail.com',
            password: 'Amazon123!'
        }
    })
    .then(resp => {
        window.sessionStorage.setItem('access_token', resp.body.oauth2.access_token);
        window.sessionStorage.setItem('applications', JSON.stringify(resp.body.applications));
        window.sessionStorage.setItem('auth_token', resp.body.oauth2.access_token);
        window.sessionStorage.setItem('country', resp.body.country);
        window.sessionStorage.setItem('expires_in', resp.body.oauth2.expires_in);
        window.sessionStorage.setItem('jwt', resp.body.jwt);
        window.sessionStorage.setItem('language', `en_${resp.body.country}`);
        window.sessionStorage.setItem('refresh_token', resp.body.oauth2.refresh_token);
        window.sessionStorage.setItem('region', resp.body.oauth2.region);
        window.sessionStorage.setItem('role', resp.body.role);
        window.sessionStorage.setItem('token_data', JSON.stringify(resp.body));
        window.sessionStorage.setItem('userInfo', JSON.stringify(resp.body));
        window.sessionStorage.setItem('user_applications', JSON.stringify(resp.body.applications));
        window.sessionStorage.setItem('user_customer', JSON.stringify(resp.body.customer));
        window.sessionStorage.setItem('user_profile', JSON.stringify(resp.body.profile));
        window.sessionStorage.setItem('username', resp.body.profile.userAccount);
    });
}

describe('USER MANAGEMENT SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'es_CR');
            login(win);
        });
        cy.visit('http://localhost:57928/user-management/inbox/');
    });
    it('should contain "Consola de Administración de Usuarios" as last breadcrumb item', function () {
        cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-breadcrumb > ul > li:nth-child(1) ').contains('Inicio');
        cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-breadcrumb > ul > li:nth-child(2) ').contains('Consola de Administración de Usuarios');
    });
    it('should contain "Consola de Administración de Usuarios" as page title', function () {
        cy.get('body > app > app-pages > div > div > div > user-management > div > h1 ').contains('Consola de Administración de Usuarios');
    });
    it('should contain "Solicitudes pendientes" for pending requests.', function () {
        cy.get('#cmx-user-pending-request-USER-MANAFEMENT').contains('Solicitudes pendientes');
    });
    it('should contain "Total de usuarios" for total users', function () {
        cy.get('body > app > app-pages > div > div > div > user-management > div > div:nth-child(4) > div > div:nth-child(2) > div > div:nth-child(2) > div:nth-child(2) ').contains('Total de usuarios');
    });
    it('should contain "Inbox" as first page tab', function () {
        cy.get('ul.nav-tabs > li:nth-child(1)').contains('Inbox');
    });
    it('should contain "Administración de usuarios" as second page tab', function () {
        cy.get('ul.nav-tabs > li:nth-child(2)').contains('Administración de usuarios');
    });

    describe('Inbox Section', function() {
        it('should contain "Solicitudes activas" as first dropdown filter option', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').click({force:true});
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div:nth-child(2) > div > cmx-dropdown-item:nth-child(1) > div').contains('Solicitudes activas');
        });
        it('should contain "Aprobada" as second dropdown filter option', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').click({force:true});
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div:nth-child(2) > div > cmx-dropdown-item:nth-child(2) > div').contains('Aprobada');
        });
        it('should contain "Rechazada" as third dropdown filter option', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').click({force:true});
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div:nth-child(2) > div > cmx-dropdown-item:nth-child(3) > div').contains('Rechazada');
        });
        it('should contain "Buscar por usuario o compañía" on filter input placeholder', function () {
            cy.get('#cmx-input-search-request-USER-MANAGEMENT').should('have.attr', 'placeholder', 'Buscar por usuario o compañía');
        });
        it('should contain "Estado" as first table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(1) > div > div ').contains('Estado');
        });
        it('should contain "Solicitud" as second table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Solicitud');
        });
        it('should contain "Tipo de solicitud" as third table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('Tipo de solicitud');
        });
        it('should contain "Solicitado por" as fourth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Solicitado por');
        });
        it('should contain "Compañía" as fifth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('Compañía');
        });
        it('should contain "Fecha de solicitud" as third table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(6) > div > div ').contains('Fecha de solicitud');
        });

        describe('Request Dialog', function() {
            it('should contain "Accesos Request" as title when opening a request detail', function () {
                cy.get('#cmx-btn-request-detail-0-USER-MANAGEMENT').click({ force: true });
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(1) > div:nth-child(1) > h2 ').contains('Accesos Request');
            });
            it('should contains "Fecha de creación:" for request created date.', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(1) > div:nth-child(2) > div > div:nth-child(1) > h6 ').contains('Fecha de creación:');
            });
            it('should contains "Solicitud:" for request id.', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(1) > div:nth-child(2) > div > div:nth-child(2) > h6 ').contains('Solicitud:');
            });
            it('should contain "Nombre" as first table column', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(5) > table > thead > tr > th:nth-child(1) ').contains('Nombre');
            });
            it('should contain "Código de obra" as second table column', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(5) > table > thead > tr > th:nth-child(2) ').contains('Código de obra');
            });
            it('should contain "Rechazar" for reject button', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(3) > div > cmx-dialog-footer > div > button:nth-child(1) ').contains('Rechazar');
            });
            it('should contain "Aprobar" for approve button', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(3) > div > cmx-dialog-footer > div > button:nth-child(2) ').contains('Aprobar');
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > button > span:nth-child(1) ').click({force:true});
            });
        });
    })

    describe('Main Section', function() {
        it('should contain "Buscar por usuario, compañía o correo electrónico" on filter input placeholder', function () {
            cy.get('div#1.nav-link').click({force: true});
            cy.get('#cmx-input-search-user-USER-MANAGEMENT').should('have.attr', 'placeholder', 'Buscar por usuario, compañía o correo electrónico');
        });
        it('should contain "Usuario" as first table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(1) > div > div ').contains('Usuario');
        });
        it('should contain "Compañía" as second table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Compañía');
        });
        it('should contain "Correo electrónico" as third table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('Correo electrónico');
        });
        it('should contain "Teléfono" as fourth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Teléfono');
        });
        it('should contain "Acceso a aplicaciones" as fifth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('Acceso a aplicaciones');
        });
        it('should contain "Crear Usuario Nuevo" for user creation button.', function () {
            cy.get('#cmx-btn-go-create-user-USER-MANAGEMENT').contains('Crear Usuario Nuevo');
        });
    });
});

describe('EDIT USER SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'es_CR');
        });
        cy.visit('http://localhost:57928/user-management/edit-user?id=omegaprodinterclerk@mailinator.com/');
    });
    it('should contain "Perfil de Usuario" as last breadcrumb item.', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > cmx-breadcrumb > ul > li:nth-child(3) ').contains('Perfil de Usuario');
    });
    it('should contain "Perfil de Usuario" as page title', function () {
        cy.get('#cmx-edituser-pagetitle-lbl').contains('Perfil de Usuario');
    });
    it('should contain "Télefono" as user phone number', function () {
        cy.get('#cmx-profile-box-phonenumber-lbl').contains('Télefono');
    });
    it('should contain "Correo electrónico" as user email', function () {
        cy.get('#cmx-profile-box-email-lbl').contains('Correo electrónico');
    });
    it('should contain "Compañía" as user company', function () {
        cy.get('#cmx-profile-box-company-lbl').contains('Compañía');
    });
    it('should contain "País" as user company', function () {
        cy.get('#cmx-profile-box-country-lbl').contains('País');
    });
    it('should contain "Número de cliente" for custumer number.', function () {
        cy.get('#cmx-profile-box-customer-code-title-EDIT-USER').contains('Número de cliente');
    });
    it('should contain "Desactivar Usuario" for disable user button', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > profile-box > div > div:nth-child(3) > div:nth-child(2) > div > a:nth-child(1) > span ').contains('Desactivar Usuario');
    });
    it('should contain "Registro de acciones" for action log button', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > profile-box > div > div:nth-child(3) > div:nth-child(2) > div > a:nth-child(2) > span ').contains('Registro de acciones');
    });
    it('should contain "Reset password" for reset password button', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > profile-box > div > div:nth-child(3) > div:nth-child(2) > div > a:nth-child(3) > span ').contains('notworking');
    });
    describe('Access to Applications Tab', function() {
        it('should contain "Acceso a Aplicaciones" for first page tab', function () {
            cy.get('#0').contains('Acceso a Aplicaciones');
        });
        it('should contain "Asignar permiso" for assign accesses button', function () {
            cy.get('#cmx-btn-add-permission-EDIT-USER').contains('Asignar permiso');
        });
        it('should contain "Actualizar" for assign accesses button', function () {
            cy.get('#cmx-btn-submit-EDIT-USER').contains('Actualizar');
        });
    });
    describe('Information Tab', function() {
        it('should contain "Información" for second page tab', function () {
            cy.get('#1').contains('Información');
        });
        it('should contain "Buscar por nombre" as input PH', function() {
            cy.get('#1').click({force: true});
            cy.get('#cmx-input-search-customer-EDIT-USER').should('have.attr', 'placeholder', 'Buscar por nombre');
        });
        it('should contain "Asignar accessos" for assign access button', function() {
            cy.get('#cmx-btn-data-access-EDIT-USER').contains('Asignar accessos');
        });
    });
});

describe('CREATE USER SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'es_CR');
        });
        cy.visit('http://localhost:57928/user-management/create-user/');
    });

    it('should contain "Crear Usuario Nuevo" in last breadcrumb item', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-breadcrumb > ul > li:nth-child(3) ').contains('Crear Usuario Nuevo');
    });
    it('should contain "Información de contacto" as first process step', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(1) ').contains('Información de contacto');
    });
    it('should contain "Asignar aplicaciones y roles" as second process step', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(2) ').contains('Asignar aplicaciones y roles');
    });
    it('should contain "Asignar información" as third process step', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(3) ').contains('Asignar información');
    });
    it('should contain "Resumen" as fourth process step  ', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(4) ').contains('Resumen');
    });

    describe('Contact Information Section', function() {
        it('should contain "Número de cliente" as first form input label and "Select client number" as PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > label ').contains('Número de cliente');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > div > span ').contains('Seleccione número de cliente');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > div > span ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > div:nth-child(3) > div:nth-child(1) > div > input ').should('have.attr', 'placeholder', 'Seleccione número de cliente');
            cy.get('#cmx-dropdown-item-customer--CREATE-USER').click({force:true});
        });
        it('should contain "Compañía" as second form input label', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(2) > cmx-form-field > label ').contains('Compañía');
        });
        it('should contain "País" as third form input label', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(3) > cmx-form-field > label ').contains('País');
        });
        it('should contain "Nombre" as fourth form input label and "Ingrese nombre" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(1) > cmx-form-field > label ').contains('Nombre');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(1) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Ingrese nombre');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(1) > cmx-form-field > input ').type('foo');
        });
        it('should contain "Apellido" as fifth form input label and "Ingrese apellido" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(2) > cmx-form-field > label ').contains('Apellido');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(2) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Ingrese apellido');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(2) > cmx-form-field > input ').type('foo');
        });
        it('should contain "Rol" as sixth form input label and "Ingrese rol" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(3) > cmx-form-field > label ').contains('Rol');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(3) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Ingrese rol');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(3) > cmx-form-field > input ').type('foo');
        });
        it('should contain "Correo electrónico" as seventh form input label and "Ingrese correo electrónico" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(1) > cmx-form-field > label ').contains('Correo electrónico');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(1) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Ingrese correo electrónico');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(1) > cmx-form-field > input ').type('foo@gmail.com');
        });
        it('should contain "Teléfono" as eighth form input label and "Ingrese teléfono" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(2) > cmx-form-field > label ').contains('Teléfono');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(2) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Ingrese teléfono');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(2) > cmx-form-field > input ').type('123');
        });
        it('should contain "Contraseña temporal" as nineth input label, "Ingrese contraseña temporal" as input PH and correct tooltip message', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(3) > div > label ').contains('Contraseña temporal');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(3) > div > input ').should('have.attr', 'placeholder', 'Ingrese contraseña temporal');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(3) > div > input ').type('foobazaO34?');
            cy.get('#cmx-tooltip-temp-password-CREATE-USER > span ').should('have.attr', 'aria-label', 'Su nueva contraseña debe tener de 8 a 20 caracteres y es recomendable incluir letras mayúsculas, minúsculas y números.');
        });
        it('should contain "Siguiente" for submit button', function () {
            cy.get('#cmx-btn-next-CREATE-USER').contains('Siguiente');
            cy.get('#cmx-btn-next-CREATE-USER').click({force: true});
        });
    });

    describe('Assign Applications and Roles', function() {
        it('should contain "Información de contacto" as page title', function () {
            cy.get('.contact-info__title').contains('Información de contacto');
        });
        it('should contain "Nombre" as first column', function () {
            cy.get('#cmx-contact-info-name-lbl').contains('Nombre');
        });
        it('should contain "Rol" as second column', function () {
            cy.get('#cmx-contact-info-position-lbl').contains('Rol');
        });
        it('should contain "Compañía" as third column', function () {
            cy.get('#cmx-contact-info-company-lbl').contains('Compañía');
        });
        it('should contain "País" as fourth column', function () {
            cy.get('#cmx-contact-info-country-lbl').contains('País');
        });
        it('should contain "Correo electrónico" as fifth column', function () {
            cy.get('#cmx-contact-info-email-lbl').contains('Correo electrónico');
        });
        it('should contain "Teléfono" as sixth column', function () {
            cy.get('#cmx-contact-info-phone-lbl').contains('Teléfono');
        });
        it('should contain "Asignar permiso" as assign perm button', function () {
            cy.get('#cmx-btn-add-permission-CREATE-USER').contains('Asignar permiso');
            cy.get('#cmx-btn-add-permission-CREATE-USER').click({ force: true });
        });
        it('should contain "Seleccione aplicación" as default perm option key', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').contains('Seleccione aplicación');
        });
        it('should contain "Seleccione roles" as default perm option value', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').contains('Seleccione roles');
        });

        it('should contain "Track" for track key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-Foreman_App-CREATE-USER').contains('Track');
            cy.get('#cmx-item-app-Foreman_App-CREATE-USER').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Visualizar');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Modificar');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Información del Cliente" for customer info key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-CustomerInfo_App-CREATE-USER').contains('Información del Cliente');
            cy.get('#cmx-item-app-CustomerInfo_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click();
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option > cmx-checkbox > label ').contains('Gestión de obras');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Condiciones Comerciales" for commercial cond key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-QuotationPricing_App-CREATE-USER').contains('Condiciones Comerciales');
            cy.get('#cmx-item-app-QuotationPricing_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Comprador');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Mostrar solicitudes de cotización');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(3) > cmx-checkbox > label ').contains('Crear/Aceptar');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(4) > cmx-checkbox > label ').contains('Términos y Condiciones Especiales');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(5) > cmx-checkbox > label ').contains('Términos y Condiciones Generales');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Pedidos" for order and prod key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-OrderProductCat_App-CREATE-USER').contains('Pedidos');
            cy.get('#cmx-item-app-OrderProductCat_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Crear pedidos general');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Crear pedidos superintendente');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(3) > cmx-checkbox > label ').contains('Crear pedidos gerente compras');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Aprovisionamiento de Usuarios" for user manage key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-UserProvisioning_App-CREATE-USER').contains('Aprovisionamiento de Usuarios');
            cy.get('#cmx-item-app-UserProvisioning_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option > cmx-checkbox > label ').contains('Crear Usuarios');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Facturas y Pagos" for invoice and pay key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-INVOICE_PAYMENTS-CREATE-USER').contains('Facturas y Pagos');
            cy.get('#cmx-item-app-INVOICE_PAYMENTS-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Procesar pagos');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Supervisor de cuentas por pagar');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(3) > cmx-checkbox > label ').contains('Procesar anticipos');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(4) > cmx-checkbox > label ').contains('Transacciones en efectivo');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(5) > cmx-checkbox > label ').contains('Reconciliación');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(6) > cmx-checkbox > label ').contains('Acceso a LIEN Request');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').click({ force: true });
            cy.get('#cmx-btn-next-CREATE-USER').click({force: true});
        });
    });

    describe('Assign Data', function() {
        it('should contain "Asignar información" as page title', function () {
            cy.get('#cmx-createuser-pagetitle-lbl').contains('Asignar información');
        });

        describe('Legal Entities Section', function() {
            it('should contain "Entidades legales" as first page tab', function () {
                cy.get('#0').contains('Entidades legales');
                cy.get('#0').click({ force: true });
            });
            it('should contain "Nombre" as first column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Nombre');
            });
            it('should contain "Código SAP" as second column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('Código SAP');
            });
            it('should contain "Región / Dirección" as third column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Región / Dirección');
            });
            it('should contain "RFC" as fifth column', function(){
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('RFC');
            });
            it('should contain "Teléfono" as fourth column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(6) > div > div ').contains('Teléfono');
            });
            it('should contain "Correo electrónico" as sixth column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(7) > div > div ').contains('Correo electrónico');
            });
            it('should contain "País" as seventh column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(8) > div > div ').contains('País');
            });
            it('should contain "Buscar por compañía o región" as search input PH', function () {
                cy.get('#cmx-input-search-customer-CREATE-USER').should('have.attr', 'placeholder', 'Buscar por compañía o región');
            });
        });

        describe('Locations Section', function() {
            it('should contain "Obras" as second page tab', function () {
                cy.get('#1').contains('Obras');
                cy.get('#1').click({ force: true });
            });
            it('should contain "Nombre" as first column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Nombre');
            });
            it('should contain "Código de obra" as second column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('Código de obra');
            });
            it('should contain "Región / Dirección" as third column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Región / Dirección');
            });
            it('should contain "Entidad legal" as fourth column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('Entidad legal');
            });
            it('should contain "Buscar por compañía u obra" as filter PH', function () {
                cy.get('#cmx-input-search-jobsite-CREATE-USER').should('have.attr', 'placeholder', 'Buscar por compañía u obra');
                cy.get('#cmx-btn-next-CREATE-USER').click({force: true});
            });
        });
    });

    describe('Summary', function() {
        it('should contain "Información de contacto" as first table title', function () {
            cy.get('.contact-info__title').contains('Información de contacto');
        });
        it('should contain "Nombre" as first column of first table', function () {
            cy.get('#cmx-contact-info-name-lbl').contains('Nombre');
        });
        it('should contain "Rol" as second column of first table', function () {
            cy.get('#cmx-contact-info-position-lbl').contains('Rol');
        });
        it('should contain "Compañía" as third column of first table', function () {
            cy.get('#cmx-contact-info-company-lbl').contains('Compañía');
        });
        it('should contain "País" as fourth column of first table', function () {
            cy.get('#cmx-contact-info-country-lbl').contains('País');
        });
        it('should contain "Correo electrónico" as fifth column of first table', function () {
            cy.get('#cmx-contact-info-email-lbl').contains('Correo electrónico');
        });
        it('should contain "Teléfono" as sixth column of first table', function () {
            cy.get('#cmx-contact-info-phone-lbl').contains('Teléfono');
        });

        it('should contain "Acceso a información" as title of second table', function () {
            cy.get('#cmx-title-access-data-CREATE-USER').contains('Acceso a información');
        });
        it('should contain "Entidades legales" as subtitle of second table', function () {
            cy.get('#cmx-title-customer-CREATE-USER').contains('Entidades legales');
        });
        it('should contain "Código" as first table column', function () {
            cy.get('#cmx-title-customer-code-CREATE-USER').contains('Código');
        });
        it('should contain "Nombre" as second table column', function () {
            cy.get('#cmx-title-customer-name-CREATE-USER').contains('Nombre');
        });
        it('should contain "Crear" for last step submit button', function () {
            cy.get('#cmx-btn-submit-CREATE-USER').contains('Crear');
        });
    });

});

