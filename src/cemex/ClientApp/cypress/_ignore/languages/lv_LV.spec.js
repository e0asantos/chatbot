describe('LOGIN SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'lv_LV');
        });
        cy.visit('http://localhost:57928/login/');
    });
    it('should contain "Latviešu" in language dropdown', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').contains('Latviešu');
    });
    it('should contain "Laipni lūdzam!" for welcome message.', function () {
        cy.get('.login__content-title').contains('Laipni lūdzam!');
    });
    it('should contain "Lietotājvārds." in the username form label', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > label ').contains('Lietotājvārds.');
    });
    it('should contain "Ievadīt lietotājvārdu" on username input placeholder', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > input ').should('have.attr', 'placeholder', 'Ievadīt lietotājvārdu');
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > input ').type('foo');
    });
    it('should contain "Parole" in the password form label', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > label ').contains('Parole');
    });
    it('should contain "Ievadīt paroli" on password input placeholder', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > input ').should('have.attr', 'placeholder', 'Ievadīt paroli');
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > input ').type('foo');
    });
    it('should contain link text "Aizmirsāt paroli?" to retrieve users password', function () {
        cy.get('[data-tid="cmx-login-form-forgot-password-link"]').contains('Aizmirsāt paroli?');
    });
    it('should contain "Pieslēgties" as submit button', function () {
        cy.get('[data-tid="cmx-login-form-submit-btn"]').contains('Pieslēgties');
        // not working, do not show the error message, this could be a cypress bug.
        cy.get('[data-tid="cmx-login-form"]').submit();
    });
    it('should contain "Ievadīts nepareizs lietotāja vārds vai parole. Lūdzu mēģiniet vēlreiz." when user or password incorrect', function () {
        // Not working
        cy.get('.login__content-message-error').contains('Ievadīts nepareizs lietotāja vārds vai parole. Lūdzu mēģiniet vēlreiz.');
    });
});

describe('FORGOT PASSWORD SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'lv_LV');
        });
        cy.visit('http://localhost:57928/forgotPassword/');
    });
    it('should contain "Aizmirsāt paroli?" as page title', function () {
        cy.get('.custom-panel__content__title').contains('Aizmirsāt paroli?');
    });
    it('should contain correct text as page description', function () {
        cy.get('.custom-panel__content__instruction').contains('Ievadiet e-pasta adresi, lai saņemtu saiti paroles atjaunošanai.');
    });
    it('should contain "E - pasts" for email label', function () {
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > label ').contains('E - pasts');
    });
    it('should contain "Ievadīt e-pasta adresi" for email input placeholder', function () {
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > input ').should('have.attr', 'placeholder', 'Ievadīt e-pasta adresi');
    });
    it('should contain "Nepareizs e-pasta formāts" for email format error.', function () {
        cy.get('#cmx-forgot-password-email-input').type('foo');
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > span > error-message ').contains('Nepareizs e-pasta formāts');
    });
    it('should contain "Jāievada e-pasts" when no text is in the input', function () {
        cy.get('#cmx-forgot-password-email-input').clear();
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > span > error-message ').contains('Jāievada e-pasts');
    });
    it('should contain "Sūtīt" as sumbit button', function () {
        // Cannot access cross origin frames. :(
        // cy.get('iframe').then(function($iframe) {
        //     const $body = $iframe.contents().find('body')
        //     cy
        //     .wrap($body)
        //     .find('#recaptcha-anchor-label > span')
        //     .contains('No soy un robot')
        // });
        cy.get('#cmx-forgot-password-submit-btn').contains('Sūtīt');
    });
});

function login(window) {
    cy.request({
        method: 'POST',
        url: 'https://cemexqas.azure-api.net/v2/secm/oam/oauth2/token',
        form: true,
        body: {
            grant_type: 'password',
            scope: 'security',
            username: 'da.us.qa@yopmail.com',
            password: 'Amazon123!'
        }
    })
    .then(resp => {
        window.sessionStorage.setItem('access_token', resp.body.oauth2.access_token);
        window.sessionStorage.setItem('applications', JSON.stringify(resp.body.applications));
        window.sessionStorage.setItem('auth_token', resp.body.oauth2.access_token);
        window.sessionStorage.setItem('country', resp.body.country);
        window.sessionStorage.setItem('expires_in', resp.body.oauth2.expires_in);
        window.sessionStorage.setItem('jwt', resp.body.jwt);
        window.sessionStorage.setItem('language', `en_${resp.body.country}`);
        window.sessionStorage.setItem('refresh_token', resp.body.oauth2.refresh_token);
        window.sessionStorage.setItem('region', resp.body.oauth2.region);
        window.sessionStorage.setItem('role', resp.body.role);
        window.sessionStorage.setItem('token_data', JSON.stringify(resp.body));
        window.sessionStorage.setItem('userInfo', JSON.stringify(resp.body));
        window.sessionStorage.setItem('user_applications', JSON.stringify(resp.body.applications));
        window.sessionStorage.setItem('user_customer', JSON.stringify(resp.body.customer));
        window.sessionStorage.setItem('user_profile', JSON.stringify(resp.body.profile));
        window.sessionStorage.setItem('username', resp.body.profile.userAccount);
    });
}

describe('USER MANAGEMENT SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'lv_LV');
            login(win);
        });
        cy.visit('http://localhost:57928/user-management/inbox/');
    });
    it('should contain "Lietotāju pārvaldības konsole" as last breadcrumb item', function () {
        cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-breadcrumb > ul > li:nth-child(1) ').contains('Uz sākumu');
        cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-breadcrumb > ul > li:nth-child(2) ').contains('Lietotāju pārvaldības konsole');
    });
    it('should contain "Lietotāju pārvaldības konsole" as page title', function () {
        cy.get('body > app > app-pages > div > div > div > user-management > div > h1 ').contains('Lietotāju pārvaldības konsole');
    });
    it('should contain "Procesā esošie pieprasījumi" for pending requests.', function () {
        cy.get('#cmx-user-pending-request-USER-MANAFEMENT').contains('Procesā esošie pieprasījumi');
    });
    it('should contain "Kopējais lietotāju skaits" for total users', function () {
        cy.get('body > app > app-pages > div > div > div > user-management > div > div:nth-child(4) > div > div:nth-child(2) > div > div:nth-child(2) > div:nth-child(2) ').contains('Kopējais lietotāju skaits');
    });
    it('should contain "Iesūtne" as first page tab', function () {
        cy.get('ul.nav-tabs > li:nth-child(1)').contains('Iesūtne');
    });
    it('should contain "Lietotāju pārvaldība" as second page tab', function () {
        cy.get('ul.nav-tabs > li:nth-child(2)').contains('Lietotāju pārvaldība');
    });

    describe('Inbox Section', function() {
        it('should contain "Aktuālie pieprasījumi" as first dropdown filter option', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').click({force:true});
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div:nth-child(2) > div > cmx-dropdown-item:nth-child(1) > div').contains('Aktuālie pieprasījumi');
        });
        it('should contain "Apstiprināts" as second dropdown filter option', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').click({force:true});
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div:nth-child(2) > div > cmx-dropdown-item:nth-child(2) > div').contains('Apstiprināts');
        });
        it('should contain "Atteikts" as third dropdown filter option', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').click({force:true});
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div:nth-child(2) > div > cmx-dropdown-item:nth-child(3) > div').contains('Atteikts');
        });
        it('should contain "Meklēt pēc lietotāja vai uzņēmuma" on filter input placeholder', function () {
            cy.get('#cmx-input-search-request-USER-MANAGEMENT').should('have.attr', 'placeholder', 'Meklēt pēc lietotāja vai uzņēmuma');
        });
        it('should contain "Statuss" as first table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(1) > div > div ').contains('Statuss');
        });
        it('should contain "Pieprasījuma ID" as second table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Pieprasījuma ID');
        });
        it('should contain "Pieprasījuma veids" as third table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('Pieprasījuma veids');
        });
        it('should contain "Pieprasīja" as fourth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Pieprasīja');
        });
        it('should contain "Uzņēmums" as fifth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('Uzņēmums');
        });
        it('should contain "Pieprasījuma datums" as third table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(6) > div > div ').contains('Pieprasījuma datums');
        });

        describe('Request Dialog', function() {
            it('should contain "Dati Request" as title when opening a request detail', function () {
                cy.get('#cmx-btn-request-detail-0-USER-MANAGEMENT').click({ force: true });
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(1) > div:nth-child(1) > h2 ').contains('Dati Request');
            });
            it('should contains "Izveidošanas datums" for request created date.', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(1) > div:nth-child(2) > div > div:nth-child(1) > h6 ').contains('Izveidošanas datums');
            });
            it('should contains "Pieprasījuma ID" for request id.', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(1) > div:nth-child(2) > div > div:nth-child(2) > h6 ').contains('Pieprasījuma ID');
            });
            it('should contain "Vārds" as first table column', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(5) > table > thead > tr > th:nth-child(1) ').contains('Vārds');
            });
            it('should contain "Objekta kods" as second table column', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(5) > table > thead > tr > th:nth-child(2) ').contains('Objekta kods');
            });
            it('should contain "Atteikt" for reject button', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(3) > div > cmx-dialog-footer > div > button:nth-child(1) ').contains('Atteikt');
            });
            it('should contain "Apstiprināt" for approve button', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(3) > div > cmx-dialog-footer > div > button:nth-child(2) ').contains('Apstiprināt');
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > button > span:nth-child(1) ').click({force:true});
            });
        });
    })

    describe('Main Section', function() {
        it('should contain "Meklēt pēc lietotāja, uzņēmuma vai e-pasta" on filter input placeholder', function () {
            cy.get('div#1.nav-link').click({force: true});
            cy.get('#cmx-input-search-user-USER-MANAGEMENT').should('have.attr', 'placeholder', 'Meklēt pēc lietotāja, uzņēmuma vai e-pasta');
        });
        it('should contain "Lietotājvārds" as first table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(1) > div > div ').contains('Lietotājvārds');
        });
        it('should contain "Uzņēmums" as second table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Uzņēmums');
        });
        it('should contain "e-pasts" as third table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('e-pasts');
        });
        it('should contain "Tālrunis" as fourth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Tālrunis');
        });
        it('should contain "Piekļūt lietotnēm" as fifth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('Piekļūt lietotnēm');
        });
        it('should contain "Izveidot jaunu lietotāju" for user creation button.', function () {
            cy.get('#cmx-btn-go-create-user-USER-MANAGEMENT').contains('Izveidot jaunu lietotāju');
        });
    });
});

describe('EDIT USER SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'lv_LV');
        });
        cy.visit('http://localhost:57928/user-management/edit-user?id=omegaprodinterclerk@mailinator.com/');
    });
    it('should contain "Lietotāja profils" as last breadcrumb item.', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > cmx-breadcrumb > ul > li:nth-child(3) ').contains('Lietotāja profils');
    });
    it('should contain "Lietotāja profils" as page title', function () {
        cy.get('#cmx-edituser-pagetitle-lbl').contains('Lietotāja profils');
    });
    it('should contain "Tālruņa numurs" as user phone number', function () {
        cy.get('#cmx-profile-box-phonenumber-lbl').contains('Tālruņa numurs');
    });
    it('should contain "E - pasts" as user email', function () {
        cy.get('#cmx-profile-box-email-lbl').contains('E - pasts');
    });
    it('should contain "Uzņēmums" as user company', function () {
        cy.get('#cmx-profile-box-company-lbl').contains('Uzņēmums');
    });
    it('should contain "Valsts" as user company', function () {
        cy.get('#cmx-profile-box-country-lbl').contains('Valsts');
    });
    it('should contain "Klienta numurs" for custumer number.', function () {
        cy.get('#cmx-profile-box-customer-code-title-EDIT-USER').contains('Klienta numurs');
    });
    it('should contain "Atspējot lietotāju" for disable user button', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > profile-box > div > div:nth-child(3) > div:nth-child(2) > div > a:nth-child(1) > span ').contains('Atspējot lietotāju');
    });
    it('should contain "Darbību pārskats" for action log button', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > profile-box > div > div:nth-child(3) > div:nth-child(2) > div > a:nth-child(2) > span ').contains('Darbību pārskats');
    });
    it('should contain "Reset password" for reset password button', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > profile-box > div > div:nth-child(3) > div:nth-child(2) > div > a:nth-child(3) > span ').contains('notworking');
    });
    describe('Access to Applications Tab', function() {
        it('should contain "Piekļūt lietotnēm" for first page tab', function () {
            cy.get('#0').contains('Piekļūt lietotnēm');
        });
        it('should contain "Piešķirt lietošanas tiesības" for assign accesses button', function () {
            cy.get('#cmx-btn-add-permission-EDIT-USER').contains('Piešķirt lietošanas tiesības');
        });
        it('should contain "Iesniegt" for assign accesses button', function () {
            cy.get('#cmx-btn-submit-EDIT-USER').contains('Iesniegt');
        });
    });
    describe('Information Tab', function() {
        it('should contain "Informācija" for second page tab', function () {
            cy.get('#1').contains('Informācija');
        });
        it('should contain "Meklēt pēc nosaukuma" as input PH', function() {
            cy.get('#1').click({force: true});
            cy.get('#cmx-input-search-customer-EDIT-USER').should('have.attr', 'placeholder', 'Meklēt pēc nosaukuma');
        });
        it('should contain "Piešķirt piekļuves tiesības" for assign access button', function() {
            cy.get('#cmx-btn-data-access-EDIT-USER').contains('Piešķirt piekļuves tiesības');
        });
    });
});

describe('CREATE USER SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'lv_LV');
        });
        cy.visit('http://localhost:57928/user-management/create-user/');
    });

    it('should contain "Izveidot jaunu lietotāju" in last breadcrumb item', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-breadcrumb > ul > li:nth-child(3) ').contains('Izveidot jaunu lietotāju');
    });
    it('should contain "Kontaktinformācija" as first process step', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(1) ').contains('Kontaktinformācija');
    });
    it('should contain "Piešķirt lietotnes un lomas" as second process step', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(2) ').contains('Piešķirt lietotnes un lomas');
    });
    it('should contain "Piešķirt datus" as third process step', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(3) ').contains('Piešķirt datus');
    });
    it('should contain "Kopsavilkums" as fourth process step  ', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(4) ').contains('Kopsavilkums');
    });

    describe('Contact Information Section', function() {
        it('should contain "Klienta numurs" as first form input label and "Select client number" as PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > label ').contains('Klienta numurs');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > div > span ').click({ force: true });
            cy.get('#cmx-dropdown-item-customer--CREATE-USER').click({force:true});
        });
        it('should contain "Uzņēmums" as second form input label', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(2) > cmx-form-field > label ').contains('Uzņēmums');
        });
        it('should contain "Valsts" as third form input label', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(3) > cmx-form-field > label ').contains('Valsts');
        });
        it('should contain "Vārds" as fourth form input label and "Ievadīt vārdu" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(1) > cmx-form-field > label ').contains('Vārds');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(1) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Ievadīt vārdu');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(1) > cmx-form-field > input ').type('foo');
        });
        it('should contain "Uzvārds" as fifth form input label and "Ievadīt vārdu" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(2) > cmx-form-field > label ').contains('Uzvārds');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(2) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Ievadīt vārdu');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(2) > cmx-form-field > input ').type('foo');
        });
        it('should contain "Pozīcija" as sixth form input label and "Ievadīt amatu" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(3) > cmx-form-field > label ').contains('Pozīcija');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(3) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Ievadīt amatu');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(3) > cmx-form-field > input ').type('foo');
        });
        it('should contain "E - pasts" as seventh form input label and "Ievadīt e-pasta adresi" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(1) > cmx-form-field > label ').contains('E - pasts');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(1) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Ievadīt e-pasta adresi');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(1) > cmx-form-field > input ').type('foo@gmail.com');
        });
        it('should contain "Tālrunis" as eighth form input label and "Ievadīt tālruņa numuru" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(2) > cmx-form-field > label ').contains('Tālrunis');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(2) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Ievadīt tālruņa numuru');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(2) > cmx-form-field > input ').type('123');
        });
        it('should contain "Jūsu parolei jābūt vismaz 8 līdz 20 rakstzīmēm, un ieteicams iekļaut lielos un mazos burtus un ciparus.', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(3) > div > label ').contains('Pagaidu parole');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(3) > div > input ').should('have.attr', 'placeholder', 'Ievadīt pagaidu paroli');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(3) > div > input ').type('foobazaO34?');
            cy.get('#cmx-tooltip-temp-password-CREATE-USER > span ').should('have.attr', 'aria-label', 'Jūsu parolei jābūt vismaz 8 līdz 20 rakstzīmēm, un ieteicams iekļaut lielos un mazos burtus un ciparus.');
        });
        it('should contain "Nākamais" for submit button', function () {
            cy.get('#cmx-btn-next-CREATE-USER').contains('Nākamais');
            cy.get('#cmx-btn-next-CREATE-USER').click({force: true});
        });
    });

    describe('Assign Applications and Roles', function() {
        it('should contain "Información de contacto" as page title', function () {
            cy.get('.contact-info__title').contains('Información de contacto');
        });
        it('should contain "Nombre" as first column', function () {
            cy.get('#cmx-contact-info-name-lbl').contains('Nombre');
        });
        it('should contain "Rol" as second column', function () {
            cy.get('#cmx-contact-info-position-lbl').contains('Rol');
        });
        it('should contain "Compañía" as third column', function () {
            cy.get('#cmx-contact-info-company-lbl').contains('Compañía');
        });
        it('should contain "País" as fourth column', function () {
            cy.get('#cmx-contact-info-country-lbl').contains('País');
        });
        it('should contain "Correo electrónico" as fifth column', function () {
            cy.get('#cmx-contact-info-email-lbl').contains('Correo electrónico');
        });
        it('should contain "Teléfono" as sixth column', function () {
            cy.get('#cmx-contact-info-phone-lbl').contains('Teléfono');
        });
        it('should contain "Asignar permiso" as assign perm button', function () {
            cy.get('#cmx-btn-add-permission-CREATE-USER').contains('Asignar permiso');
            cy.get('#cmx-btn-add-permission-CREATE-USER').click({ force: true });
        });
        it('should contain "Seleccione aplicación" as default perm option key', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').contains('Seleccione aplicación');
        });
        it('should contain "Seleccione roles" as default perm option value', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').contains('Seleccione roles');
        });

        it('should contain "Track" for track key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-Foreman_App-CREATE-USER').contains('Track');
            cy.get('#cmx-item-app-Foreman_App-CREATE-USER').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Visualizar');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Modificar');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Información del Cliente" for customer info key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-CustomerInfo_App-CREATE-USER').contains('Información del Cliente');
            cy.get('#cmx-item-app-CustomerInfo_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click();
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option > cmx-checkbox > label ').contains('Gestión de obras');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Condiciones Comerciales" for commercial cond key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-QuotationPricing_App-CREATE-USER').contains('Condiciones Comerciales');
            cy.get('#cmx-item-app-QuotationPricing_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Comprador');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Mostrar solicitudes de cotización');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(3) > cmx-checkbox > label ').contains('Crear/Aceptar');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(4) > cmx-checkbox > label ').contains('Términos y Condiciones Especiales');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(5) > cmx-checkbox > label ').contains('Términos y Condiciones Generales');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Pedidos" for order and prod key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-OrderProductCat_App-CREATE-USER').contains('Pedidos');
            cy.get('#cmx-item-app-OrderProductCat_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Crear pedidos general');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Crear pedidos superintendente');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(3) > cmx-checkbox > label ').contains('Crear pedidos gerente compras');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Aprovisionamiento de Usuarios" for user manage key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-UserProvisioning_App-CREATE-USER').contains('Aprovisionamiento de Usuarios');
            cy.get('#cmx-item-app-UserProvisioning_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option > cmx-checkbox > label ').contains('Crear Usuarios');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Facturas y Pagos" for invoice and pay key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-INVOICE_PAYMENTS-CREATE-USER').contains('Facturas y Pagos');
            cy.get('#cmx-item-app-INVOICE_PAYMENTS-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Procesar pagos');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Supervisor de cuentas por pagar');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(3) > cmx-checkbox > label ').contains('Procesar anticipos');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(4) > cmx-checkbox > label ').contains('Transacciones en efectivo');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(5) > cmx-checkbox > label ').contains('Reconciliación');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(6) > cmx-checkbox > label ').contains('Acceso a LIEN Request');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').click({ force: true });
            cy.get('#cmx-btn-next-CREATE-USER').click({force: true});
        });
    });

    describe('Assign Data', function() {
        it('should contain "Asignar información" as page title', function () {
            cy.get('#cmx-createuser-pagetitle-lbl').contains('Asignar información');
        });

        describe('Legal Entities Section', function() {
            it('should contain "Entidades legales" as first page tab', function () {
                cy.get('#0').contains('Entidades legales');
                cy.get('#0').click({ force: true });
            });
            it('should contain "Nombre" as first column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Nombre');
            });
            it('should contain "Código SAP" as second column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('Código SAP');
            });
            it('should contain "Región / Dirección" as third column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Región / Dirección');
            });
            it('should contain "RFC" as fifth column', function(){
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('RFC');
            });
            it('should contain "Teléfono" as fourth column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(6) > div > div ').contains('Teléfono');
            });
            it('should contain "Correo electrónico" as sixth column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(7) > div > div ').contains('Correo electrónico');
            });
            it('should contain "País" as seventh column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(8) > div > div ').contains('País');
            });
            it('should contain "Buscar por compañía o región" as search input PH', function () {
                cy.get('#cmx-input-search-customer-CREATE-USER').should('have.attr', 'placeholder', 'Buscar por compañía o región');
            });
        });

        describe('Locations Section', function() {
            it('should contain "Obras" as second page tab', function () {
                cy.get('#1').contains('Obras');
                cy.get('#1').click({ force: true });
            });
            it('should contain "Nombre" as first column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Nombre');
            });
            it('should contain "Código de obra" as second column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('Código de obra');
            });
            it('should contain "Región / Dirección" as third column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Región / Dirección');
            });
            it('should contain "Entidad legal" as fourth column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('Entidad legal');
            });
            it('should contain "Buscar por compañía u obra" as filter PH', function () {
                cy.get('#cmx-input-search-jobsite-CREATE-USER').should('have.attr', 'placeholder', 'Buscar por compañía u obra');
                cy.get('#cmx-btn-next-CREATE-USER').click({force: true});
            });
        });
    });

    describe('Summary', function() {
        it('should contain "Información de contacto" as first table title', function () {
            cy.get('.contact-info__title').contains('Información de contacto');
        });
        it('should contain "Nombre" as first column of first table', function () {
            cy.get('#cmx-contact-info-name-lbl').contains('Nombre');
        });
        it('should contain "Rol" as second column of first table', function () {
            cy.get('#cmx-contact-info-position-lbl').contains('Rol');
        });
        it('should contain "Compañía" as third column of first table', function () {
            cy.get('#cmx-contact-info-company-lbl').contains('Compañía');
        });
        it('should contain "País" as fourth column of first table', function () {
            cy.get('#cmx-contact-info-country-lbl').contains('País');
        });
        it('should contain "Correo electrónico" as fifth column of first table', function () {
            cy.get('#cmx-contact-info-email-lbl').contains('Correo electrónico');
        });
        it('should contain "Teléfono" as sixth column of first table', function () {
            cy.get('#cmx-contact-info-phone-lbl').contains('Teléfono');
        });

        it('should contain "Acceso a información" as title of second table', function () {
            cy.get('#cmx-title-access-data-CREATE-USER').contains('Acceso a información');
        });
        it('should contain "Entidades legales" as subtitle of second table', function () {
            cy.get('#cmx-title-customer-CREATE-USER').contains('Entidades legales');
        });
        it('should contain "Código" as first table column', function () {
            cy.get('#cmx-title-customer-code-CREATE-USER').contains('Código');
        });
        it('should contain "Nombre" as second table column', function () {
            cy.get('#cmx-title-customer-name-CREATE-USER').contains('Nombre');
        });
        it('should contain "Crear" for last step submit button', function () {
            cy.get('#cmx-btn-submit-CREATE-USER').contains('Crear');
        });
    });

});

