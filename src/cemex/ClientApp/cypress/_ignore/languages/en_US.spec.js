describe('LOGIN SCREEN', function () {
    it('starts the application', function () {
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.window().then((win) => {
            win.sessionStorage.clear();
            win.localStorage.setItem('language', 'en_US');
        });
        cy.visit('http://localhost:57928/login/');
    });
    it('should contain "English US" in language dropdown', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').contains('English US');
    });
    it('should contain "Welcome" for welcome message.', function () {
        cy.get('.login__content-title').contains('Welcome');
    });
    it('should contain "Username" in the username form label', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > label ').contains('Username.');
    });
    it('should contain "Enter your username" on username input placeholder', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > input ').should('have.attr', 'placeholder', 'Enter your username');
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(1) > input ').type('foo');
    });
    it('should contain "Password" in the password form label', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > label ').contains('Password');
    });
    it('should contain "Enter your password" on password input placeholder', function () {
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > input ').should('have.attr', 'placeholder', 'Enter your password');
        cy.get('body > app > login > div > div:nth-child(2) > div:nth-child(2) > cmx-login > div > div:nth-child(2) > form > cmx-form-field:nth-child(3) > input ').type('foo');
    });
    it('should contain link text "Forgot password?" to retrieve users password', function () {
        cy.get('[data-tid="cmx-login-form-forgot-password-link"]').contains('Forgot password?');
    });
    it('should contain "Log In" as submit button', function () {
        cy.get('[data-tid="cmx-login-form-submit-btn"]').contains('Log In');
        // not working, do not show the error message, this could be a cypress bug.
        cy.get('[data-tid="cmx-login-form"]').submit({force: true});
    });
    it('should contain "The user or password you entered are incorrect. Please try again." when user or password incorrect.', function () {
        // Not working
        cy.get('.login__content-message-error').contains('The user or password you entered are incorrect. Please try again.');
    });
});

describe('FORGOT PASSWORD SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'en_US');
        });
        cy.visit('http://localhost:57928/forgotPassword/');
    });
    it('should contain "Forgot your password?" as page title', function () {
        cy.get('.custom-panel__content__title').contains('Forgot your password?');
    });
    it('should contain correct text as page description', function () {
        cy.get('.custom-panel__content__instruction').contains('Enter your e-mail address and we will send you a link to reset your password.');
    });
    it('should contain "Email" for email label', function () {
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > label ').contains('Email');
    });
    it('should contain "Enter email" for email input placeholder', function () {
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > input ').should('have.attr', 'placeholder', 'Enter email');
    });
    it('should contain "Invalid email format" for email format error.', function () {
        cy.get('#cmx-forgot-password-email-input').type('foo');
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > span > error-message ').contains('Invalid email format');
    });
    it('should contain "Email is required" when no text is in the input', function () {
        cy.get('#cmx-forgot-password-email-input').clear();
        cy.get('body > app > forgot-password > div > panel > div > div:nth-child(3) > form > cmx-form-field > span > error-message ').contains('Email is required');
    });
    it('should contain "Send" as sumbit button', function () {
        // Cannot access cross origin frames. :(
        // cy.get('iframe').then(function($iframe) {
        //     const $body = $iframe.contents().find('body')
        //     cy
        //     .wrap($body)
        //     .find('#recaptcha-anchor-label > span')
        //     .contains('No soy un robot')
        // });
        cy.get('#cmx-forgot-password-submit-btn').contains('Send');
    });
});

function login(window) {
    cy.request({
        method: 'POST',
        url: 'https://cemexqas.azure-api.net/v2/secm/oam/oauth2/token',
        form: true,
        body: {
            grant_type: 'password',
            scope: 'security',
            username: 'da.us.qa@yopmail.com',
            password: 'Amazon123!'
        }
    })
    .then(resp => {
        window.sessionStorage.setItem('access_token', resp.body.oauth2.access_token);
        window.sessionStorage.setItem('applications', JSON.stringify(resp.body.applications));
        window.sessionStorage.setItem('auth_token', resp.body.oauth2.access_token);
        window.sessionStorage.setItem('country', resp.body.country);
        window.sessionStorage.setItem('expires_in', resp.body.oauth2.expires_in);
        window.sessionStorage.setItem('jwt', resp.body.jwt);
        window.sessionStorage.setItem('language', `en_${resp.body.country}`);
        window.sessionStorage.setItem('refresh_token', resp.body.oauth2.refresh_token);
        window.sessionStorage.setItem('region', resp.body.oauth2.region);
        window.sessionStorage.setItem('role', resp.body.role);
        window.sessionStorage.setItem('token_data', JSON.stringify(resp.body));
        window.sessionStorage.setItem('userInfo', JSON.stringify(resp.body));
        window.sessionStorage.setItem('user_applications', JSON.stringify(resp.body.applications));
        window.sessionStorage.setItem('user_customer', JSON.stringify(resp.body.customer));
        window.sessionStorage.setItem('user_profile', JSON.stringify(resp.body.profile));
        window.sessionStorage.setItem('username', resp.body.profile.userAccount);
    });
}

describe('USER MANAGEMENT SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'en_US');
            login(win);
        });
        cy.visit('http://localhost:57928/user-management/inbox/');
    });
    it('should contain "User Management Console" as last breadcrumb item', function () {
        cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-breadcrumb > ul > li:nth-child(1) ').contains('Home');
        cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-breadcrumb > ul > li:nth-child(2) ').contains('User Management Console');
    });
    it('should contain "User Management Console" as page title', function () {
        cy.get('body > app > app-pages > div > div > div > user-management > div > h1 ').contains('User Management Console');
    });
    it('should contain "Pending Requests" for pending requests.', function () {
        cy.get('#cmx-user-pending-request-USER-MANAFEMENT').contains('Pending Requests');
    });
    it('should contain "Total Users" for total users', function () {
        cy.get('body > app > app-pages > div > div > div > user-management > div > div:nth-child(4) > div > div:nth-child(2) > div > div:nth-child(2) > div:nth-child(2) ').contains('Total Users');
    });
    it('should contain "Inbox" as first page tab', function () {
        cy.get('ul.nav-tabs > li:nth-child(1)').contains('Inbox');
    });
    it('should contain "User Management" as second page tab', function () {
        cy.get('ul.nav-tabs > li:nth-child(2)').contains('User Management');
    });

    describe('Inbox Section', function() {
        it('should contain "Active Requests" as first dropdown filter option', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').click({force:true});
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div:nth-child(2) > div > cmx-dropdown-item:nth-child(1) > div').contains('Active Requests');
        });
        it('should contain "Approved" as second dropdown filter option', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').click({force:true});
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div:nth-child(2) > div > cmx-dropdown-item:nth-child(2) > div').contains('Approved');
        });
        it('should contain "Rejected" as third dropdown filter option', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div > span ').click({force:true});
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(2) > div:nth-child(1) > cmx-dropdown > div > div:nth-child(2) > div > cmx-dropdown-item:nth-child(3) > div').contains('Rejected');
        });
        it('should contain "Search by user or company" on filter input placeholder', function () {
            cy.get('#cmx-input-search-request-USER-MANAGEMENT').should('have.attr', 'placeholder', 'Search by user or company');
        });
        it('should contain "Status" as first table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(1) > div > div ').contains('Status');
        });
        it('should contain "Request ID" as second table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Request ID');
        });
        it('should contain "Type of Request" as third table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('Type of Request');
        });
        it('should contain "Requested by" as fourth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Requested by');
        });
        it('should contain "Company" as fifth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('Company');
        });
        it('should contain "Request Date" as third table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(1) > div:nth-child(4) > requests-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(6) > div > div ').contains('Request Date');
        });

        describe('Request Dialog', function() {
            it('should contain "Data Request" as title when opening a request detail', function () {
                cy.get('#cmx-btn-request-detail-0-USER-MANAGEMENT').click({ force: true });
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(1) > div:nth-child(1) > h2 ').contains('Data Request');
            });
            it('should contains "Created Date:" for request created date.', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(1) > div:nth-child(2) > div > div:nth-child(1) > h6 ').contains('Created Date:');
            });
            it('should contains "Request ID:" for request id.', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(1) > div:nth-child(2) > div > div:nth-child(2) > h6 ').contains('Request ID:');
            });
            it('should contain "Name" as first table column', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(5) > table > thead > tr > th:nth-child(1) ').contains('Name');
            });
            it('should contain "Location Code" as second table column', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > cmx-dialog-body > div:nth-child(5) > table > thead > tr > th:nth-child(2) ').contains('Location Code');
            });
            it('should contain "Reject" for reject button', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(3) > div > cmx-dialog-footer > div > button:nth-child(1) ').contains('Reject');
            });
            it('should contain "Approve" for approve button', function () {
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > div:nth-child(3) > div > cmx-dialog-footer > div > button:nth-child(2) ').contains('Approve');
                cy.get('body > app > app-pages > div > div > div > user-management > cmx-dialog:nth-child(2) > div:nth-child(2) > button > span:nth-child(1) ').click({force:true});
            });
        });
    })

    describe('Main Section', function() {
        it('should contain "Search by user, company or email" on filter input placeholder', function () {
            cy.get('div#1.nav-link').click({force: true});
            cy.get('#cmx-input-search-user-USER-MANAGEMENT').should('have.attr', 'placeholder', 'Search by user, company or email');
        });
        it('should contain "User Name" as first table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(1) > div > div ').contains('User Name');
        });
        it('should contain "Company" as second table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Company');
        });
        it('should contain "E-mail" as third table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('E-mail');
        });
        it('should contain "Phone number" as fourth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Phone number');
        });
        it('should contain "Access to apps" as fifth table column', function () {
            cy.get('body > app > app-pages > div > div > div > user-management > div > cmx-tabs > div > cmx-tab:nth-child(2) > div:nth-child(4) > users-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('Access to apps');
        });
        it('should contain "Create New User" for user creation button.', function () {
            cy.get('#cmx-btn-go-create-user-USER-MANAGEMENT').contains('Create New User');
        });
    });

});

describe('EDIT USER SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'en_US');
        });
        cy.visit('http://localhost:57928/user-management/edit-user?id=omegaprodinterclerk@mailinator.com/');
    });
    it('should contain "User Profile" as last breadcrumb item.', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > cmx-breadcrumb > ul > li:nth-child(3) ').contains('User Profile');
    });
    it('should contain "User Profile" as page title', function () {
        cy.get('#cmx-edituser-pagetitle-lbl').contains('User Profile');
    });
    it('should contain "Phone Number" as user phone number', function () {
        cy.get('#cmx-profile-box-phonenumber-lbl').contains('Phone Number');
    });
    it('should contain "Email" as user email', function () {
        cy.get('#cmx-profile-box-email-lbl').contains('Email');
    });
    it('should contain "Company" as user company', function () {
        cy.get('#cmx-profile-box-company-lbl').contains('Company');
    });
    it('should contain "Country" as user company', function () {
        cy.get('#cmx-profile-box-country-lbl').contains('Country');
    });
    it('should contain "Customer Number" for custumer number.', function () {
        cy.get('#cmx-profile-box-customer-code-title-EDIT-USER').contains('Customer Number');
    });
    it('should contain "Disable User" for disable user button', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > profile-box > div > div:nth-child(3) > div:nth-child(2) > div > a:nth-child(1) > span ').contains('Disable User');
    });
    it('should contain "Action Log" for action log button', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > profile-box > div > div:nth-child(3) > div:nth-child(2) > div > a:nth-child(2) > span ').contains('Action Log');
    });
    it('should contain "Reset password" for reset password button', function () {
        cy.get('body > app > app-pages > div > div > div > edit-user > div:nth-child(1) > profile-box > div > div:nth-child(3) > div:nth-child(2) > div > a:nth-child(3) > span ').contains('Reset password');
    });
    describe('Access to Applications Tab', function() {
        it('should contain "Access to Applications" for first page tab', function () {
            cy.get('#0').contains('Access to Applications');
        });
        it('should contain "Assign Permission" for assign accesses button', function () {
            cy.get('#cmx-btn-add-permission-EDIT-USER').contains('Assign Permission');
        });
        it('should contain "Submit" for assign accesses button', function () {
            cy.get('#cmx-btn-submit-EDIT-USER').contains('Submit');
        });
    });
    describe('Information Tab', function() {
        it('should contain "Information" for second page tab', function () {
            cy.get('#1').contains('Information');
        });
        it('should contain "Search by name" as input PH', function() {
            cy.get('#1').click({force: true});
            cy.get('#cmx-input-search-customer-EDIT-USER').should('have.attr', 'placeholder', 'Search by name');
        });
        it('should contain "Assign Accesses" for assign access button', function() {
            cy.get('#cmx-btn-data-access-EDIT-USER').contains('Assign Accesses');
        });
    });
});

describe('CREATE USER SCREEN', function () {
    it('starts the application', function () {
        cy.window().then((win) => {
            win.localStorage.setItem('language', 'en_US');
        });
        cy.visit('http://localhost:57928/user-management/create-user/');
    });
    it('should contain "Create New User" in last breadcrumb item', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-breadcrumb > ul > li:nth-child(3) ').contains('Create New User');
    });
    it('should contain "Contact Information" as first process step', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(1) ').contains('Contact Information');
    });
    it('should contain "Assign Applications and Roles" as second process step', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(2) ').contains('Assign Applications and Roles');
    });
    it('should contain "Assign Data" as third process step', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(3) ').contains('Assign Data');
    });
    it('should contain "Summary" as fourth process step  ', function () {
        cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > ul > li:nth-child(4) ').contains('Summary');
    });
    describe('Contact Information Section', function() {
        it('should contain "Client Number" as first form input label and "Select client number" as PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > label ').contains('Client Number');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > div > span ').contains('Select client number');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > div > span ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(1) > div > cmx-dropdown > div > div:nth-child(3) > div:nth-child(1) > div > input ').should('have.attr', 'placeholder', 'Select client number');
            cy.get('#cmx-dropdown-item-customer--CREATE-USER').click({force:true});
        });
        it('should contain "Company" as second form input label', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(2) > cmx-form-field > label ').contains('Company');
        });
        it('should contain "Country" as third form input label', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(3) > div:nth-child(3) > cmx-form-field > label ').contains('Country');
        });
        it('should contain "Name" as fourth form input label and "Enter name" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(1) > cmx-form-field > label ').contains('Name');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(1) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Enter name');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(1) > cmx-form-field > input ').type('foo');
        });
        it('should contain "Last Name" as fifth form input label and "Enter last name" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(2) > cmx-form-field > label ').contains('Last Name');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(2) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Enter last name');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(2) > cmx-form-field > input ').type('foo');
        });
        it('should contain "Position" as sixth form input label and "Enter position" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(3) > cmx-form-field > label ').contains('Position');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(3) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Enter position');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(5) > div:nth-child(3) > cmx-form-field > input ').type('foo');
        });
        it('should contain "Email" as seventh form input label and "Enter email" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(1) > cmx-form-field > label ').contains('Email');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(1) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Enter email');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(1) > cmx-form-field > input ').type('foo@gmail.com');
        });
        it('should contain "Phone" as eighth form input label and "Enter phone" as input PH', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(2) > cmx-form-field > label ').contains('Phone');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(2) > cmx-form-field > input ').should('have.attr', 'placeholder', 'Enter phone');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(2) > cmx-form-field > input ').type('123');
        });
        it('should contain "Temporary Password" as nineth input label, "Enter temporary password" as input PH and correct tooltip message', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(3) > div > label ').contains('Temporary Password');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(3) > div > input ').should('have.attr', 'placeholder', 'Enter temporary password');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(7) > div:nth-child(3) > div > input ').type('foobazaO34?');
            cy.get('#cmx-tooltip-temp-password-CREATE-USER > span ').should('have.attr', 'aria-label', 'Your password must have at least from 8 to 20 characters and is recommended to include an uppercase, lowercase letters, and numbers.');
        });
        it('should contain correct text for contact me checkbox', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(9) > div > cmx-checkbox > label ').contains('I want CEMEX to use my email and phone as means to contact me.');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(1) > form > div:nth-child(9) > div > cmx-checkbox > label ').click({ force: true });
        });
        it('should contain "Next" for submit button', function () {
            cy.get('#cmx-btn-next-CREATE-USER').contains('Next');
            cy.get('#cmx-btn-next-CREATE-USER').click({force: true});
        });
    });

    describe('Assign Applications and Roles', function() {
        it('should contain "Contact Information" as page title', function () {
            cy.get('.contact-info__title').contains('Contact Information');
            cy.get('.contact-info__title').should('have.class', 'contact-info__title');
            cy.get('.contact-info__title').click({ force: true });
        });
        it('should contain "Name" as first column', function () {
            cy.get('#cmx-contact-info-name-lbl').contains('Name');
            cy.get('#cmx-contact-info-name-lbl').should('have.class', 'contact-info__label');
            cy.get('#cmx-contact-info-name-lbl').click({ force: true });
        });
        it('should contain "Position" as second column', function () {
            cy.get('#cmx-contact-info-position-lbl').contains('Position');
            cy.get('#cmx-contact-info-position-lbl').should('have.class', 'contact-info__label');
            cy.get('#cmx-contact-info-position-lbl').click({ force: true });
        });
        it('should contain "Company" as third column', function () {
            cy.get('#cmx-contact-info-company-lbl').contains('Company');
            cy.get('#cmx-contact-info-company-lbl').should('have.class', 'contact-info__label');
            cy.get('#cmx-contact-info-company-lbl').click({ force: true });
        });
        it('should contain "Country" as fourth column', function () {
            cy.get('#cmx-contact-info-country-lbl').contains('Country');
            cy.get('#cmx-contact-info-country-lbl').should('have.class', 'contact-info__label');
            cy.get('#cmx-contact-info-country-lbl').click({ force: true });
        });
        it('should contain "E-mail" as fifth column', function () {
            cy.get('#cmx-contact-info-email-lbl').contains('E-mail');
        });
        it('should contain "Phone number" as sixth column', function () {
            cy.get('#cmx-contact-info-phone-lbl').contains('Phone number');
        });
        it('should contain "Assign Permission" as assign perm button', function () {
            cy.get('#cmx-btn-add-permission-CREATE-USER').contains('Assign Permission');
            cy.get('#cmx-btn-add-permission-CREATE-USER').click({ force: true });
        });
        it('should contain "Select application" as default perm option key', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').contains('Select application');
        });
        it('should contain "Select roles" as default perm option value', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').contains('Select roles');
        });

        it('should contain "Track" for track key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-Foreman_App-CREATE-USER').contains('Track');
            cy.get('#cmx-item-app-Foreman_App-CREATE-USER').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Display user');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Modification User');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Customer Information" for customer info key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-CustomerInfo_App-CREATE-USER').contains('Customer Information');
            cy.get('#cmx-item-app-CustomerInfo_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click();
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option > cmx-checkbox > label ').contains('Job Site Creation');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Commercial Conditions" for commercial cond key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-QuotationPricing_App-CREATE-USER').contains('Commercial Conditions');
            cy.get('#cmx-item-app-QuotationPricing_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Buyer');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Display Quotation Requests');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(3) > cmx-checkbox > label ').contains('Create/Accept');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(4) > cmx-checkbox > label ').contains('Special Terms and Conditions');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(5) > cmx-checkbox > label ').contains('General Terms and Conditions');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Order and Product Catalog" for order and prod key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-OrderProductCat_App-CREATE-USER').contains('Order and Product Catalog');
            cy.get('#cmx-item-app-OrderProductCat_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Owner Create Orders');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Foreman Create Orders');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(3) > cmx-checkbox > label ').contains('Purchase manager Create Orders');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "User Management" for user manage key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-UserProvisioning_App-CREATE-USER').contains('User Management');
            cy.get('#cmx-item-app-UserProvisioning_App-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option > cmx-checkbox > label ').contains('Creates users');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
        });

        it('should contain "Invoice and Payments" for invoice and pay key and its corresponding roles', function () {
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(2) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('#cmx-item-app-INVOICE_PAYMENTS-CREATE-USER').contains('Invoice and Payments');
            cy.get('#cmx-item-app-INVOICE_PAYMENTS-CREATE-USER').click({force:true});
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(2) ').click({ force: true });
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').contains('Accounts Payable Clerk');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(2) > cmx-checkbox > label ').contains('Accounts Payable Supervisor');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(3) > cmx-checkbox > label ').contains('Accounts Payable Clerk (Advanced Payments)');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(4) > cmx-checkbox > label ').contains('Buyer Cash');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(5) > cmx-checkbox > label ').contains('Reconciliation');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(6) > cmx-checkbox > label ').contains('LIEN Request Access');
            cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(2) > div:nth-child(2) > permission > div:nth-child(1) > div:nth-child(3) > cmx-select > div:nth-child(3) > div > cmx-simple-option:nth-child(1) > cmx-checkbox > label ').click({ force: true });
            cy.get('#cmx-btn-next-CREATE-USER').click({force: true});
        });

    });

    describe('Assign Data', function() {
        it('should contain "Assign Data" as page title', function () {
            cy.get('#cmx-createuser-pagetitle-lbl').contains('Assign Data');
        });
        describe('Legal Entities Section', function() {
            it('should contain "Legal Entities" as first page tab', function () {
                cy.get('#0').contains('Legal Entities');
                cy.get('#0').click({ force: true });
            });
            it('should contain "Name" as first column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Name');
            });
            it('should contain "SAP Code" as second column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('SAP Code');
            });
            it('should contain "Region / Address" as third column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Region / Address');
            });
            it('should contain "Phone number" as fourth column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(6) > div > div ').contains('Phone number');
            });
            it('should contain "VAT" as fifth column', function(){
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('VAT');
            });
            it('should contain "E-mail" as sixth column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(7) > div > div ').contains('E-mail');
            });
            it('should contain "Country" as seventh column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(1) > customers-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(8) > div > div ').contains('Country');
            });
            it('should contain "Search by customer or region" as search input PH', function () {
                cy.get('#cmx-input-search-customer-CREATE-USER').should('have.attr', 'placeholder', 'Search by customer or region');
            });
        });

        describe('Locations Section', function() {
            it('should contain "Locations" as second page tab', function () {
                cy.get('#1').contains('Locations');
                cy.get('#1').click({ force: true });
            });
            it('should contain "Name" as first Locations column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(2) > div > div ').contains('Name');
            });
            it('should contain "Location Code" as second Locations column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(3) > div > div ').contains('Location Code');
            });
            it('should contain "Region / Address" as third Locations column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(4) > div > div ').contains('Region / Address');
            });
            it('should contain "Legal Entity" as fourth Locations column', function () {
                cy.get('body > app > app-pages > div > div > div > create-user > div:nth-child(1) > cmx-steps > div > div:nth-child(2) > cmx-step:nth-child(3) > cmx-tabs > div > cmx-tab:nth-child(2) > jobsites-table > cmx-table > div:nth-child(1) > div > cmx-header-cell:nth-child(5) > div > div ').contains('Legal Entity');
            });
            it('should contain "Search by customer or jobsite" as filter PH', function () {
                cy.get('#cmx-input-search-jobsite-CREATE-USER').should('have.attr', 'placeholder', 'Search by customer or jobsite');
                cy.get('#cmx-btn-next-CREATE-USER').click({force: true});
            });
        });
    });

    describe('Summary', function() {
        it('should contain "Contact Information" as first table title', function () {
            cy.get('.contact-info__title').contains('Contact Information');
        });
        it('should contain "Name" as first column of first table', function () {
            cy.get('#cmx-contact-info-name-lbl').contains('Name');
        });
        it('should contain "Position" as second column of first table', function () {
            cy.get('#cmx-contact-info-position-lbl').contains('Position');
        });
        it('should contain "Company" as third column of first table', function () {
            cy.get('#cmx-contact-info-company-lbl').contains('Company');
        });
        it('should contain "Country" as fourth column of first table', function () {
            cy.get('#cmx-contact-info-country-lbl').contains('Country');
        });
        it('should contain "E-mail" as fifth column of first table', function () {
            cy.get('#cmx-contact-info-email-lbl').contains('E-mail');
        });
        it('should contain "Phone number" as sixth column of first table', function () {
            cy.get('#cmx-contact-info-phone-lbl').contains('Phone number');
        });
        it('should contain "Access to Data" as title of second table', function () {
            cy.get('#cmx-title-access-data-CREATE-USER').contains('Access to Data');
        });
        it('should contain "Legal Entities" as subtitle of second table', function () {
            cy.get('#cmx-title-customer-CREATE-USER').contains('Legal Entities');
        });
        it('should contain "Code" as first table column', function () {
            cy.get('#cmx-title-customer-code-CREATE-USER').contains('Code');
        });
        it('should contain "Name" as second table column', function () {
            cy.get('#cmx-title-customer-name-CREATE-USER').contains('Name');
        });
        it('should contain "Submit" for last step submit button', function () {
            cy.get('#cmx-btn-submit-CREATE-USER').contains('Submit');
        });
    });

});

