import { Component, OnDestroy, ViewChild, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

// CMX Dependencies
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4';

// Providers
import { CountlyService } from '@cemex-core/helpers-v1/dist';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist/';
import { PasswordService, ErrorService } from '../../services';

// Utils
import { DCMConstants } from '../../helpers/DCM.constants';
import { EmailValidators } from '../../helpers/validators';

// Models
import { ApiError } from '../../models/apiError';

// Components
import { LocalizationComponent } from './../../components/localization/localization.component';
import { RecaptchaComponent } from 'ng-recaptcha';

@Component({
    selector: 'app-forgot-password',
    templateUrl: 'forgot-password.component.html',
    styleUrls: ['forgot-password.component.scss']
})

export class ForgotPasswordComponent extends LocalizationComponent implements OnInit, OnDestroy {

    @ViewChild('successModal')
    public successModal: CmxDialogComponent;

    @ViewChild(RecaptchaComponent)
    public recaptcha: RecaptchaComponent;

    public form: FormGroup;
    public key = DCMConstants.GOOGLE_RECAPTCHA_KEY;
    public submitted = false;
    public hasError = false;

    public errMessage: string;
    public errCode: string;
    public emailSent = true;
    public captchaOk = false;
    private subscription: Subscription;

    constructor(
        public translationService: TranslationService,
        private formBuilder: FormBuilder,
        private passwordService: PasswordService,
        private router: Router,
        private countlyService: CountlyService,
        private errorService: ErrorService
    ) {
        super();
    }

    public ngOnInit(): void {
        this.startCountly();
        this.countlyService.addTrackingPage('Forgot Password');
        this.form = this.formBuilder.group({
            email: [{ value: '', disabled: false }, Validators.compose([Validators.required, EmailValidators.pattern])],
        });
    }

    public ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        this.countlyService.endSession();
    }

    public send(): void {
        this.submitted = true;
        const email = this.form.get('email').value;
        this.subscription = this.passwordService
            .requestPasswordChange(email)
            .subscribe((response) => {
                const event = {
                    key: 'forgotPasswordSuccess'
                };
                this.submitted = false;
                this.captchaOk = false;
                this.recaptcha.reset();
                this.form.markAsPristine();
                this.form.reset();

                this.countlyService.addTracking('add_event', event);

                if (response && !response['emailIsSent']) {
                    this.emailSent = false;
                    this.hasError = true;
                    this.errMessage = this.translationService.pt('views.forgotPassword.EmailNotDelivered');
                } else {
                    this.successModal.open();
                }
            },
                (error) => {
                    this.form.markAsPristine();
                    this.recaptcha.reset();
                    this.captchaOk = false;
                    this.submitted = false;
                    this.hasError = true;
                    this.errorService
                        .handleError(error)
                        .subscribe((apiErr: ApiError) => {
                            if (apiErr) {
                                this.errMessage = `${apiErr.errorCode}: ${this.translationService.pt(apiErr.errorDesc)}`;
                            }
                        });
                });
    }

    public resolved(captchaResponse: string): void {
        if (captchaResponse !== '') {
            this.captchaOk = true;
        } else {
            this.captchaOk = false;
            this.recaptcha.reset();
        }
    }

    public goToLogin(): void {
        this.router.navigate(['/login']);
    }

    private startCountly(): void {
        // As this is for a no autenticated process, no user info is needed but the email.
        const countlyKey = window['COUNTLY_KEY'];
        const countlyUrl = window['COUNTLY_URL'];

        this.countlyService.startService(countlyUrl, countlyKey);

        const userId = 0; // not an authenticated user

        if (this.countlyService.Countly) {
            this.countlyService.Countly.app_version = '2.0';
            this.countlyService.changeId(userId.toString());
            this.countlyService.init();
        }
    }

    public onTyping() {
        this.hasError = false;
        this.emailSent = true;
    }
}

