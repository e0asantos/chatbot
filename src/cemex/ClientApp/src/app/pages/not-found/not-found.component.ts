import { TranslationService } from '@cemex-core/angular-localization-v1/dist/';
import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent {

  constructor(public translationService: TranslationService) { }

}
