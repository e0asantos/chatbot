import { Component, OnDestroy, ViewChild, OnInit, ChangeDetectorRef, AfterViewChecked } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';

// Providers
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';
import { CountlyService } from '@cemex-core/helpers-v1/dist';
import { PasswordService, ErrorService } from '../../services';

// Components
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4';
import { LocalizationComponent } from '../../components/localization/localization.component';
import { PasswordTooltipComponent } from './../../components/password-tooltip/password-tooltip.component';

// Models
import { Language } from './../../models/language';
import { ApiError } from '../../models/apiError';
import { UserProfile } from '../../models/user-profile';

// Utils
import { PasswordValidator } from '../../helpers/validators';
import { DCMConstants } from '../../helpers/DCM.constants';

@Component({
    selector: 'app-reset-password',
    templateUrl: 'reset-password.component.html',
    styleUrls: ['reset-password.component.scss']
})
export class ResetPasswordComponent extends LocalizationComponent implements OnInit, OnDestroy, AfterViewChecked {

    @ViewChild('successModal')
    public successModal: CmxDialogComponent;

    @ViewChild('passwordWarningModal')
    public passwordWarningModal: CmxDialogComponent;

    @ViewChild('passtool')
    public passtool: PasswordTooltipComponent;

    public form: FormGroup;
    public tokenInactive = false;
    public submitted = false;
    public languages: Language[] = [];
    public currentLang: Language;
    public validPass = true;
    public errorMsg: string;
    public showError = false;
    public emailIsSent = true;
    private subscription: Subscription;
    private token = '';
    private tokenId = '';
    private tokenIsValid = true;
    private countryCode = '';
    private firstName = '';
    private lastName = '';
    private userAccount = '';

    constructor(
        public translationService: TranslationService,
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private passwordService: PasswordService,
        private router: Router,
        private countlyService: CountlyService,
        private cdr: ChangeDetectorRef,
        private errorService: ErrorService) {
        super();

        this.subscription = this.activatedRoute.queryParams.subscribe((params) => {
            this.tokenId = params['token'];
            this.countryCode = params['countryCode'];
        });

        this.getUserProfileByToken();

        this.translationService.getLanguages().subscribe(
            (data) => {
                this.languages = data;
                this.currentLang = this.languages.find((lang) => lang.countryCode === this.countryCode);
            }
        );

        this.startCountly();
        this.countlyService.addTrackingPage('Reset Password');

        this.form = this.formBuilder.group({
            password: ['', Validators.compose([
                Validators.required,
                Validators.pattern(DCMConstants.REGULAR_EXPRESSIONS.Password),
                Validators.maxLength(20),
                PasswordValidator.strength])],
            confirmPassword: ['', Validators.compose([
                Validators.required,
            ])],
        }, { validator: PasswordValidator.match('password', 'confirmPassword') });

    }

    public ngOnInit(): void {
        this.successModal.afterClosed().subscribe((result: boolean) => {
            if (this.emailIsSent) {
                this.router.navigate(['/login']);
            }
        });

        this.languages = window['CMX_LANGUAGES'];
        this.currentLang = this.languages.find((lang) => lang.countryCode === this.countryCode) || this.translationService.selectedLanguage;
        this.translationService.setLanguage(this.currentLang.languageISO || 'en_US');
        this.passtool.getRulePassword(this.countryCode);

    }

    public ngAfterViewChecked() {
        this.cdr.detectChanges();
    }

    public ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        this.countlyService.endSession();
    }

    public resetPassword(): void {
        this.submitted = true;
        const password = this.form.get('password').value;
        this.subscription = this.passwordService
            .changePassword(this.token, password, this.countryCode, this.userAccount)
            .subscribe((response) => {
                const event = {
                    key: 'resetPasswordSuccess'
                };
                this.countlyService.addTracking('add_event', event);
                this.form.reset();
                if (response && !response['emailIsSent']) {
                    this.emailIsSent = false;
                    this.errorMsg = this.translationService.pt('views.resetPassword.EmailNotDelivered');
                }
                this.successModal.open();
            },
                (error) => {
                    this.submitted = false;
                    this.showError = true;
                    this.form.get('confirmPassword').markAsUntouched();

                    let err;
                    try {
                        err = error.json();
                    } catch (error) {
                        err = error;
                    }
                    const errorMsg = err['message'] || err['detail'] || '';
                    if (errorMsg.toString().indexOf('otpkey') !== -1) {
                        this.errorMsg = this.translationService.pt('views.resetPassword.tokenInvalid');
                    } else if (errorMsg.toString().indexOf('attempts') !== -1) {
                        this.errorMsg = this.translationService.pt('views.resetPassword.tokenExceed');
                    } else {
                        this.errorService
                            .handleError(error)
                            .subscribe((apiError: ApiError) => {
                                if (apiError) {
                                    this.errorMsg = `${apiError.errorCode}: ${this.translationService.pt(apiError.errorDesc)}`;
                                }
                            });
                    }
                }
            );
    }

    public goToLogin(): void {
        this.router.navigate(['/login']);
    }

    private getUserProfileByToken(): void {
        this.passwordService
            .getUserProfile(this.tokenId, this.countryCode)
            .subscribe(
                (data: UserProfile) => {
                    this.firstName = data.firstName;
                    this.lastName = data.lastName;
                    this.userAccount = data.userAccount;
                    this.token = data.token;
                },
                (error) => {
                    this.showError = true;
                    this.errorService
                        .handleError(error)
                        .subscribe((apiError: ApiError) => {
                            this.tokenIsValid = false;
                            this.errorMsg = `${apiError.errorCode}: ${this.translationService.pt(apiError.errorDesc)}`;
                        });
                }
            );
    }

    public validPassword() {
        this.passtool.firstName = this.firstName !== null ? this.firstName.trim() : '';
        this.passtool.lastName = this.lastName !== null ? this.lastName.trim() : '';
        this.passtool.userAcount = this.userAccount !== null ? this.userAccount.trim() : '';
        this.passtool.active = true;
        setTimeout(function e(this) {
            const input = document.getElementById('cmx-input-password-RESET-PASSWOR');
            input.removeAttribute('readonly');
            window.scrollTo(0, document.body.scrollHeight);
            input.focus();
        }, 0);
    }

    public OnValidPassword() {
        this.passtool.firstName = this.firstName !== null ? this.firstName.trim() : '';
        this.passtool.lastName = this.lastName !== null ? this.lastName.trim() : '';
        this.passtool.userAcount = this.userAccount !== null ? this.userAccount.trim() : '';
        this.validPass = this.passtool.isPassValid();
        setTimeout(function e() {
            const input = document.getElementById('cmx-input-password-RESET-PASSWOR');
            input.setAttribute('readonly', 'true');
            input.blur();
        }, 0);
    }

    public getValid() {
        this.validPass = this.passtool.isPassValid();
        return (!this.form.valid || this.submitted || !this.validPass || !this.tokenIsValid);
    }

    private startCountly(): void {
        // As this is for a no autenticated process, no user info is needed but the email.
        const countlyKey = window['COUNTLY_KEY'];
        const countlyUrl = window['COUNTLY_URL'];

        this.countlyService.startService(countlyUrl, countlyKey);

        const userId = 0; // not an authenticated user

        if (this.countlyService.Countly) {
            this.countlyService.Countly.app_version = '2.0';
            this.countlyService.changeId(userId.toString());
            this.countlyService.init();
        }
    }
}
