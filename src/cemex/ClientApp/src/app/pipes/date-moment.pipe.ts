
import { Pipe, PipeTransform } from '@angular/core';
import { TranslationService, FormatterService, DateTimeActions } from '@cemex-core/angular-localization-v1/dist/';

declare var require: any;
let moment = require('moment');

@Pipe({
    name: 'cmxDateMoment',
    pure: false,
})
export class DateMomentPipe implements PipeTransform {
    constructor(private translationService: TranslationService,
        private formatterService: FormatterService) { }

    transform(dateTime, action: DateTimeActions = DateTimeActions.ConvertToLocal): string {
        return this.formatterService
            .formatDateTimeMoment(dateTime, this.translationService.selectedLanguage.languageISO.replace('_', '-'),
                this.translationService.selectedLanguage.formatDate, action);
    }
}