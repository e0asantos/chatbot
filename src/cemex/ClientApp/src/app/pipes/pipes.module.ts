import { NgModule } from '@angular/core';
import { SafeHtmlPipe } from './safe-html.pipe';
import { DateMomentPipe } from './date-moment.pipe';

@NgModule({
    exports: [SafeHtmlPipe, DateMomentPipe],
    declarations: [SafeHtmlPipe, DateMomentPipe],
})

export class PipesModule { }
