import { Observable } from 'rxjs/Observable';
import { CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { UserService } from '../services';
import 'rxjs/add/operator/map';
import { SessionService } from '@cemex-core/angular-services-v2/dist/';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private sessionService: SessionService, private userService: UserService) { }

    public canActivate(): boolean {
        if (this.sessionService.isLoggedIn) {
            this.sessionService.clearCookie('up_session');
            return true;
        } else {
            return this.hasSessionCookie();
        }
    }

    private hasSessionCookie(): boolean {
        const sessionCookie = this.sessionService.readCookie('up_session');
        let sessionCookieJson;
        if (sessionCookie) {
            localStorage.removeItem('language');
            // Session cookie exists, therefore generate session storage based on the cookie.
            try {
                sessionCookieJson = JSON.parse(sessionCookie);
                sessionStorage.setItem('access_token', sessionCookieJson.auth);
                sessionStorage.setItem('auth_token', sessionCookieJson.auth);
                sessionStorage.setItem('jwt', sessionCookieJson.jwt);
                sessionStorage.setItem('region', sessionCookieJson.region);

                this.sessionIsSet(sessionCookieJson).subscribe((data) => {
                    if (data) {
                        location.reload();
                    }
                });

                return true;

            } catch (error) {
                this.router.navigate(['/login']);
                return false;
            }
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }

    private sessionIsSet(sessionCookieJson?: any): Observable<boolean> {
        return this.userService.getUser()
            .map((data) => {
                sessionStorage.setItem('user_profile', JSON.stringify(data.profile));
                sessionStorage.setItem('user_customer', JSON.stringify(data.customer));
                sessionStorage.setItem('user_applications', JSON.stringify(data.applications));
                sessionStorage.setItem('country', data.countryCode);
                sessionStorage.setItem('applications', JSON.stringify(data.applications));
                sessionStorage.setItem('role', data.profile.userType);
                sessionStorage.setItem('username', data.userAccount);

                // for react apps
                const _tokenData = {
                    oauth2: {
                        access_token: sessionCookieJson.auth,
                        refresh_token: sessionCookieJson.refresh_token,
                        region: sessionCookieJson.region,
                        expires_in: sessionCookieJson.expires_in
                    },
                    jwt: sessionCookieJson.jwt,
                    profile: data.profile,
                    applications: data.applications,
                    customer: data.customer,
                    country: data.countryCode
                };

                sessionStorage.setItem('userInfo', JSON.stringify(_tokenData));
                sessionStorage.setItem('token_data', JSON.stringify(_tokenData));

                this.sessionService.setUserProfile();
                this.sessionService.clearCookie('up_session');

                return true;
            }).catch((err) => {
                return Observable.of(false);
            });
    }
}
