import { Injectable } from '@angular/core';
import {
    Router,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    CanActivateChild
} from '@angular/router';
import { AgreementService } from '../services';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

@Injectable()
export class TermsConditionsGuard implements CanActivateChild {

    constructor(
        private router: Router,
        private agreementService: AgreementService
    ) { }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

        // Dont apply on dashboard, it has its own validations
        if (state.url === '/dashboard') {
            return of(true);
        }

        // Cache agreement
        if (!this.agreementService.isTermsOfUseSigned) {
            this.router.navigate(['/dashboard']);
            return of(false);
        }
        return of(true);
    }
}
