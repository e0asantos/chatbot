import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// CEMEX Dependencies
import { CmxCheckboxModule } from '@cemex/cmx-checkbox-v4';
import { CmxDropdownModule } from '@cemex/cmx-dropdown-v4';

import { AddPermissionComponent } from './add-permission.component';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        CmxDropdownModule,
        CmxCheckboxModule
    ],
    declarations: [
        AddPermissionComponent,
    ],
    exports: [
        AddPermissionComponent,
    ]
})

export class AddPermissionModule { }
