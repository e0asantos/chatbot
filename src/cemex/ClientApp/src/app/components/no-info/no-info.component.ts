import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'no-info',
    templateUrl: 'no-info.component.html',
    styleUrls: ['no-info.component.scss']
})
export class NoInfoComponent {

    @Input() public title: string = '';
    @Input() public showButton: boolean = false;
    @Input() public buttonText = '';
    @Input() public templateId: string;

    @Output() public buttonClicked = new EventEmitter();

    private onClick() {
        this.buttonClicked.emit();
    }
}
