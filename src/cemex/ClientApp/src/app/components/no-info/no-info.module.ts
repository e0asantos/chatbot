import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Components
import { NoInfoComponent } from './no-info.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        NoInfoComponent,
    ],
    exports: [
        NoInfoComponent,
    ]
})

export class NoInfoModule { }
