import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';
import { WatcherService } from '@cemex/cmx-table-v1/dist/services';
import { Table, Column } from '@cemex/cmx-table-v1/dist';
import { Request } from '../../../models';

@Component({
    selector: 'requests-table',
    templateUrl: 'requests-table.component.html',
    styleUrls: ['./../table.component.scss'],
    providers: [WatcherService]
})

export class RequestTableComponent implements OnInit, OnChanges {

    public currentLang;

    @Input() public showCheckboxes = false;
    @Input() public isRTL = false;
    @Input() public isAdminMode = false;
    @Input() public templateId: string;

    @Input() set requests(value: Request[]) {
        this._requests.next(value);
    }

    get requests() {
        return this._requests.getValue();
    }

    @Output() public onOpenRequest = new EventEmitter();
    @Output() public onRequestsToggled = new EventEmitter();

    public requestTable = new Table();
    public requestsToCancel: number[] = [];
    public activeRequests: Request[] = [];
    private _requests = new BehaviorSubject<any[]>([]);

    constructor(public translationService: TranslationService) {
        this.currentLang = this.translationService.selectedLanguage;
        const requestTableColumns: Column[] = [
            new Column('views.global.lbl_requestID', 'requestId', true, true, 'requestId', 'requestId'),
            new Column('views.global.lbl_status', 'status', true, true, 'statusDesc', 'status'),
            new Column('views.global.lbl_requestedBy', 'requesterName', true, true, 'requesterName', 'requesterName'),
            new Column('views.global.lbl_company', 'customerDesc', true, true, 'customerDesc', 'customerDesc'),
            new Column('views.global.lbl_requestType', 'requestType', true, true, 'requestTypeDesc', 'requestType'),
            new Column('views.global.lbl_requestDate', 'requestDate', true, true, 'requestDate', 'requestDate'),
            new Column('views.global.lbl_attendedBy', 'authorizedByFullName', true, true, 'authorizedByFullName', 'authorizedByFullName')
        ];
        this.requestTable.setColumns(requestTableColumns);
    }

    public ngOnInit(): void {
        this._requests.subscribe(
            (data) => {
                /** ADD INDEX ( SCRIPTS IN QA) */
                data.forEach((e, index) => {
                    e.index = index;
                });
                this.activeRequests = this.requests.filter((req) => req.status === 'N' || req.status === 'P');
                this.setFilters();
            });
    }

    public ngOnChanges(): void {
        this.requestTable.getColumn('status').clearFilters();
        this.requestTable.getColumn('requestType').clearFilters();
        this.requestTable.getColumn('customerDesc').clearFilters();
        this.requestTable.getColumn('requesterName').clearFilters();
        this._requests.subscribe(
            () => {
                this.setFilters();
            });
    }

    public setFilters(): void {
        this.requests.forEach((data) => {
            this.requestTable.getColumn('status').addFilter(data);
            this.requestTable.getColumn('requestType').addFilter(data);
            this.requestTable.getColumn('customerDesc').addFilter(data);
            this.requestTable.getColumn('requesterName').addFilter(data);
        });
    }

    public openRequestDetail(requestDetail: any) {
        this.onOpenRequest.emit(requestDetail);
    }

    public toggleRequest(event: boolean, requestId?: number): void {
        if (typeof event === 'boolean') {
            if (requestId === undefined) {
                if (event) {
                    this.requestsToCancel =
                        this.requests.filter((r) => r.status === 'N' || r.status === 'P').map((r) => r.requestId);
                } else {
                    this.requestsToCancel = [];
                }
            } else {
                if (event) {
                    this.requestsToCancel.push(requestId);
                } else {
                    this.requestsToCancel = this.requestsToCancel.filter((r) => r !== requestId);
                }
            }
            this.onRequestsToggled.emit(this.requestsToCancel);
        }
    }

    public requestIsSelected(requestId: number): boolean {
        return this.requestsToCancel.findIndex((f) => f === requestId) > -1;
    }
}
