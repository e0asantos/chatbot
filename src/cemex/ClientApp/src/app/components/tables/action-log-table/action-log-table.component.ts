import { Component, OnInit, Input, ViewChild, OnChanges, EventEmitter, Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';
import { WatcherService } from '@cemex/cmx-table-v1/dist/services';
import { Table, Column, CmxTableComponent } from '@cemex/cmx-table-v1';
import { UserLog } from '../../../models';


@Component({
    selector: 'action-log-table',
    templateUrl: 'action-log-table.component.html',
    styleUrls: ['./../table.component.scss'],
    providers: [WatcherService]
})

export class ActionLogTableComponent implements OnInit, OnChanges {

    @ViewChild(CmxTableComponent) public table: CmxTableComponent;

    @Input() public isRTL = false;
    @Output() public onOpenNotes = new EventEmitter();
    @Input() set logs(value: UserLog[]) {
        this._logs.next(value);
    }

    get logs() {
        return this._logs.getValue();
    }

    public logTable = new Table();
    private _logs = new BehaviorSubject<UserLog[]>([]);

    constructor(public translationService: TranslationService) {
        const logTableColumns: Column[] = [
            new Column('views.actionLog.lblDate', 'entryDate', true, true, 'entryDate', 'entryDate'),
            new Column('views.actionLog.lblTime', 'entryDateTime', true, true, 'entryDate', 'entryDate'),
            new Column('views.actionLog.lblDescription', 'action', true, true, 'action', 'action'),
            new Column('views.actionLog.lblAttendedBy', 'executedBy', true, true, 'executedBy', 'executedBy')
        ];
        this.logTable.setColumns(logTableColumns);
    }

    public ngOnInit(): void {
        this._logs.subscribe(
            (data) => {
                /** ADD INDEX ( SCRIPTS IN QA) */
                data.forEach((e, index) => {
                    e.index = index;
                });
                this.setFilters();
            });
    }

    public ngOnChanges(): void {
        this._logs.subscribe(
            () => {
                this.setFilters();
            });
    }
    public setFilters(): void {
        this.logTable.getColumn('entryDate').addDateFilter();
    }

    public openNotes(event){
        this.onOpenNotes.emit(event);
    }

}
