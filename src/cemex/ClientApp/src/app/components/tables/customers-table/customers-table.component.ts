import { Component, OnInit, Input, Output, EventEmitter, OnChanges, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';
import { WatcherService } from '@cemex/cmx-table-v1/dist/services';
import { Column, Table, CmxTableComponent } from '@cemex/cmx-table-v1/dist';
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4';
import { CustomerDetail } from '../../../models';



@Component({
    selector: 'customers-table',
    templateUrl: 'customers-table.component.html',
    styleUrls: ['./../table.component.scss'],
    providers: [WatcherService]
})

export class CustomersTableComponent implements OnInit, OnChanges  {

    // tslint:disable-next-line:no-output-on-prefix
    @Output() public onCustomersToggled = new EventEmitter();

    @ViewChild(CmxTableComponent) public table: CmxTableComponent;
    @ViewChild('warningCustom') public warningCustom: CmxDialogComponent;

    @Input() set customers(value: CustomerDetail[]) {
        this._customers.next(value);
    }

    get customers() {
        return this._customers.getValue();
    }

    @Input() public templateId: string;
    @Input() public selectedCustomers: CustomerDetail[] = [];
    @Input() public showCheckboxes = false;
    @Input() public mainCustomer: number;
    @Input() public showDelete = false;
    @Input() public isRTL = false;
    @Input() public showFilter = true;

    public customerTable = new Table();
    private _customers = new BehaviorSubject<any[]>([]);

    private customerDelete: CustomerDetail;

    constructor(public t: TranslationService) {
        const columns: Column[] = [
            new Column('views.global.lbl_name', 'customerDesc', true, true, 'customerDesc', 'customerDesc'),
            new Column('views.global.lbl_sapCode', 'customerCode', true, true, 'customerCode', 'customerCode'),
            new Column('views.global.lbl_region_address', 'regionDesc', true, true, 'regionDesc', 'regionDesc'),
            new Column('views.global.lbl_vat', 'vat', true, true, 'vat', 'vat'),
            new Column('views.global.lbl_phone', 'phoneNumber', true, true, 'phoneNumber', 'phoneNumber'),
            new Column('views.global.lbl_email', 'email', true, true, 'email', 'email'),
            new Column('views.global.lbl_country', 'countryCode', true, true, 'countryCode', 'countryCode')
        ];
        this.customerTable.setColumns(columns);

    }

    public ngOnInit(): void {
        this._customers.subscribe(
            (data) => {
                /** ADD INDEX ( SCRIPTS IN QA) */
                data.forEach((e, index) => {
                    e.index = index;
                });
                this.setFilters();
            });
    }

    public ngOnChanges(): void {
        this.customerTable.getColumn('regionDesc').clearFilters();
        this._customers.subscribe(
            () => {
                this.setFilters();
            });
    }

    public setFilters(): void {
        this.customers.forEach((data) => {
            this.customerTable.getColumn('regionDesc').addFilter(data);
        });
    }

    public getActiveFilters() {
        return this.table.getActiveFiltersCopy().map(o => o.name).length > 0 ? true : false;
    }

    private isCustomerSelected(customerId: number) {
        return this.selectedCustomers.some((c) => c.customerId === customerId);
    }

    private isAllCustomerSelected() {
        const isActiveFilter = this.getActiveFilters();
        if (isActiveFilter) {
            if (this.selectedCustomers.length > 0 ) {
                const filterJobsites = this.table.filteredItems();
                const selectedCustomers = JSON.parse(JSON.stringify(this.selectedCustomers));
                this.selectedCustomers = [];

                filterJobsites.forEach(element => {
                    const item =   selectedCustomers.filter((j) => j.customerId === element.customerId)[0];
                    if (item !== undefined) {
                        this.selectedCustomers.push(item);
                    }
                });


            }
            this.onCustomersToggled.emit(this.selectedCustomers);
            return this.table.filteredItems().length === this.selectedCustomers.length;
        } else {
            if ( this.selectedCustomers.length === 0) {
                this.selectedCustomers = this.customers.filter((c) => c.customerId === this.mainCustomer);
            }
        }
        return this.customers.length === this.selectedCustomers.length;
    }

    private toggleCustomers(event: boolean, customer?: CustomerDetail) {
        if (typeof event === 'boolean') {
            const isActiveFilter = this.getActiveFilters();
            if (event && customer === undefined) {
                this.selectedCustomers = [];
                this.selectedCustomers = isActiveFilter ? this.table.filteredItems() : this.customers;
            } else if (event && customer !== undefined) {
                this.selectedCustomers.push(customer);
            } else if (!event && customer === undefined) {
                this.selectedCustomers = [];
                this.selectedCustomers = this.customers.filter((c) => c.customerId === this.mainCustomer);
            } else {
                this.selectedCustomers = this.selectedCustomers.filter((c) => c.customerId !== customer.customerId);
            }
            this.onCustomersToggled.emit(this.selectedCustomers);
        }
    }

    private openRemoveCustomer(element: CustomerDetail) {
        this.customerDelete = element;
        this.warningCustom.open();
    }

    public removeCustomer() {
        this.warningCustom.close();
        this.customers = this.customers.filter((r) => r.customerId !== this.customerDelete.customerId);
        this.selectedCustomers = this.selectedCustomers.filter((r) => r.customerId !== this.customerDelete.customerId);
        const object = {
            customers: [this.customerDelete],
            isDelete: true
        };
        this.onCustomersToggled.emit(object);
        this.refreshTable();
    }

    public refreshTable() {
        this.table.ngOnDestroy();
        this.table.ngAfterContentInit();
        this.selectedCustomers =  [];
    }

    private isNotOnlyDefault() {
        const customerDefault = this.customers.filter((c) => c.customerId === this.mainCustomer);
        if (this.customers.length === 1 && customerDefault.length === 1) {
            return false;
        }
        return true;
    }
}
