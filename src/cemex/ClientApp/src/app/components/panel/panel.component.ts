import { Component, Input, Output, EventEmitter } from '@angular/core';

// Components
import { LocalizationComponent } from '../localization/localization.component';

@Component({
    selector: 'panel',
    templateUrl: 'panel.component.html',
    styleUrls: ['panel.component.scss']
})

export class PanelComponent extends LocalizationComponent {

    @Input()
    public title = '';
    @Input()
    public instruction = '';
    @Output()
    public onClosingPanel = new EventEmitter();

    constructor() {
        super();
    }

    public closePanel(): void {
        this.onClosingPanel.emit();
    }

}
