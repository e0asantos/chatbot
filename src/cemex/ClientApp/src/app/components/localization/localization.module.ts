import { NgModule } from '@angular/core';
import { LocalizationComponent } from './localization.component';

@NgModule({
    imports: [],
    declarations: [
        LocalizationComponent
    ]
})

 export class LocalizationModule { }


