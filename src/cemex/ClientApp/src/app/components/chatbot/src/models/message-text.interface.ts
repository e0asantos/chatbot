export interface IMessageText {
  content: string;
  isErrorMessage?: boolean;
}
