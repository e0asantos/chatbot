import { Injectable } from '@angular/core';

@Injectable()
export class StoreService {

  public lastConversationId: string;

  private _conversationId: string;

  get accessToken(): string | null {
    return sessionStorage.getItem('access_token');
  }
  get countryCode(): string | null {
    return sessionStorage.getItem('country');
  }
  get customerCode(): string | null {
    const userLegalEntity: any = sessionStorage.getItem('user_legal_entity');
    const userLegalEntityObject: any = JSON.parse(userLegalEntity);
    return userLegalEntityObject && userLegalEntityObject.legalEntityTypeCode;
  }
  get legalEntityId(): string | null {
    const userLegalEntity: any = sessionStorage.getItem('user_legal_entity');
    const userLegalEntityObject: any = JSON.parse(userLegalEntity);
    return userLegalEntityObject && userLegalEntityObject.legalEntityId;
  }
  get jwt(): string | null {
    return sessionStorage.getItem('jwt');
  }
  get userLegalEntity(): string | null {
    return sessionStorage.getItem('user_legal_entity');
  }
  get userProfile(): string | null {
    return sessionStorage.getItem('user_profile');
  }
  get language(): string | null {
    return sessionStorage.getItem('language');
  }

  get conversationId(): string {
    const userInfo: any = sessionStorage.getItem('userInfo');
    const userInfoObject: any = JSON.parse(userInfo);
    if (userInfoObject) {
      return userInfoObject.sessionId;
    }
    return this._conversationId;
  }

  set conversationId(conversationId: string) {
    this._conversationId = conversationId;
  }

  public isUserLogged(): boolean {
    return sessionStorage.getItem('access_token') !== null;
  }

  public getUserInitials(): string {
    if (this.userProfile) {
      const userProfile: any = JSON.parse(this.userProfile);
      const { firstName, lastName } = userProfile;
      if (firstName && lastName) {
        return `${firstName.charAt(0)}${lastName.charAt(0)}`;
      }
    }
    return '??';
  }
}
