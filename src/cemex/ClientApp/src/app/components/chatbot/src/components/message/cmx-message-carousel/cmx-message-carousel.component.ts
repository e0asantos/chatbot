import {
  Component,
  HostBinding,
} from '@angular/core';
import { CmxMessageComplexComponent } from '../cmx-message-complex/cmx-message-complex.component';

@Component({
  selector: 'cmx-message-carousel',
  styles: ["\n    .roboto-bold{font-weight:700}.roboto-medium{font-weight:500}.roboto-regular{font-weight:400}.roboto-light{font-weight:300}.roboto-thin{font-weight:100}h1,.h1-role{font-family:\"Roboto\",sans-serif;font-size:3rem;line-height:3rem;font-weight:700;color:#001B3A}h2,.h2-role,h3,.h3-role{font-family:\"Roboto\",sans-serif;font-size:1.625rem;line-height:2rem;font-weight:400;color:#001B3A}h4,.h4-role{font-family:\"Roboto\",sans-serif;font-size:2rem;line-height:2rem;font-weight:500;color:#001B3A}h5,.h5-role{font-family:\"Roboto\",sans-serif;font-size:1.25rem;line-height:1.5rem;font-weight:400;color:#001B3A}h6,.h6-role{font-family:\"Roboto\",sans-serif;font-size:.875rem;line-height:1rem;font-weight:700;color:#001B3A;text-transform:uppercase}.cmx-block-background{content:\" \";position:fixed;top:0px;left:0px;background-color:black;width:100%;height:100%;opacity:0.3;z-index:9999999}.cmx-chat{z-index:1;font-family:\"Roboto\",sans-serif;width:100%;height:100%;overflow:hidden;box-shadow:0 5px 30px rgba(0,0,0,0.2);background:rgba(0,0,0,0.5);display:flex;justify-content:space-between;flex-direction:column}.cmx-chat *{box-sizing:border-box}.cmx-chat.cmx-chat-default{z-index:99999991;height:calc(100vh - 170px);position:fixed;top:140px;bottom:30px;right:0px;width:100%}@media (min-width: 568px) and (max-width: 1024px){.cmx-chat.cmx-chat-default{width:50%}}@media (min-width: 1025px){.cmx-chat.cmx-chat-default{width:33%}}.cmx-chat .cmx-chat-title{position:relative;z-index:2;background:#002A59;color:#fff;text-align:left;height:50px;border-bottom:2px solid #EE3D42;padding-left:86px}.cmx-chat .cmx-chat-title h1{color:#fff;height:54px;line-height:54px;white-space:nowrap;font-size:.875rem;margin:0;padding:0}.cmx-chat .cmx-chat-title h1 strong{font-size:.875rem}.cmx-chat .cmx-chat-title h2{color:rgba(255,255,255,0.5);font-size:.875rem;letter-spacing:1px}.cmx-chat .cmx-chat-title .cmx-avatar{position:absolute;top:15px;left:15px;z-index:2;margin:0px;width:60px;height:60px}.cmx-chat .cmx-chat-title .cmx-close{position:absolute;right:15px;top:18px;width:18px;height:18px;opacity:1}.cmx-chat .cmx-chat-title .cmx-close:hover{opacity:0.3}.cmx-chat .cmx-chat-title .cmx-close:before,.cmx-chat .cmx-chat-title .cmx-close:after{position:absolute;left:9px;content:' ';height:18px;width:2px;background-color:#fff}.cmx-chat .cmx-chat-title .cmx-close:before{transform:rotate(45deg)}.cmx-chat .cmx-chat-title .cmx-close:after{transform:rotate(-45deg)}.cmx-chat .cmx-messages{flex:1 1 auto;color:rgba(255,255,255,0.5);overflow:hidden;position:relative;width:100%;background-color:white}.cmx-chat .cmx-messages .cmx-messages-content{position:absolute;top:0;left:0;height:100%;width:100%;padding:20px 20px 0px 20px;overflow:auto;background-color:#F4F6F9}.cmx-chat .cmx-message-box{flex:0 1 40px;background-color:#F4F6F9;padding:15px;position:relative}.cmx-chat .cmx-message-box .cmx-message-input{background:none;border:none;outline:none;resize:none;margin:0;padding:10px;width:calc(100% - 87px);background-color:#fff;line-height:50px;height:50px;margin-right:7px;font-size:13px}.cmx-chat .cmx-message-box .cmx-message-input:disabled{cursor:not-allowed}.cmx-chat .cmx-message-box .cmx-message-input::placeholder{color:#D2D2D2}.cmx-chat .cmx-message-box .cmx-message-submit{border:none;background:#76E02C;border-radius:50%;height:50px;width:50px}.cmx-chat .cmx-message-box .cmx-message-submit:hover{background:#76E02C}.cmx-chat .cmx-message-box .cmx-message-submit svg{height:20px;width:20px;transform:rotate(-45deg)}.cmx-chat-icon{position:absolute;bottom:54px;right:54px;width:34px;height:36px}@keyframes bounce{0%{transform:matrix3d(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}4.7%{transform:matrix3d(0.45, 0, 0, 0, 0, 0.45, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}9.41%{transform:matrix3d(0.883, 0, 0, 0, 0, 0.883, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}14.11%{transform:matrix3d(1.141, 0, 0, 0, 0, 1.141, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}18.72%{transform:matrix3d(1.212, 0, 0, 0, 0, 1.212, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}24.32%{transform:matrix3d(1.151, 0, 0, 0, 0, 1.151, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}29.93%{transform:matrix3d(1.048, 0, 0, 0, 0, 1.048, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}35.54%{transform:matrix3d(0.979, 0, 0, 0, 0, 0.979, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}41.04%{transform:matrix3d(0.961, 0, 0, 0, 0, 0.961, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}52.15%{transform:matrix3d(0.991, 0, 0, 0, 0, 0.991, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}63.26%{transform:matrix3d(1.007, 0, 0, 0, 0, 1.007, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}85.49%{transform:matrix3d(0.999, 0, 0, 0, 0, 0.999, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}100%{transform:matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}}@keyframes ball{from{transform:translateY(0) scaleY(0.8)}to{transform:translateY(-10px)}}\n  "],
  templateUrl: '../cmx-message-complex/cmx-message-complex.component.html',
})
export class CmxMessageCarouselComponent extends CmxMessageComplexComponent {

  @HostBinding('class') protected class: string = 'cmx-message-carousel';

  private currentSlide: number = 0;

  public onLeftArrowClick(): void {
    if (this.currentSlide > 0) {
      this.currentSlide--;
      this.moveSlide();
    }
  }

  public onRightArrowClick(): void {
    const cmxContent: any = this.cmxContent;
    const summariesElement: any = cmxContent.nativeElement.querySelectorAll('.cmx-summary-body');
    const maxSlide: number = summariesElement.length - 1;
    if (this.currentSlide < maxSlide) {
      this.currentSlide++;
      this.moveSlide();
    }
  }

  public onSummaryClick(event: Event): void {
    const target: any = event && event.currentTarget;
    if (target) {
      const hiddenElement: any = target.querySelector('.cmx-hidden-value');

      const value: string = hiddenElement.getAttribute('value');
      const label: string = value;
      this.sendMessage.emit({ value, label });
      this.hideCarousel();
      this.data.hasSelected = true;
    }
  }

  public registerEvents(): void {
    const cmxContent: any = this.cmxContent;
    const leftArrow: any = cmxContent.nativeElement.querySelector('.cmx-icon-arrow-left-rounded');
    const rightArrow: any = cmxContent.nativeElement.querySelector('.cmx-icon-arrow-right-rounded-fill');
    const summariesElement: any = cmxContent.nativeElement.querySelectorAll('.cmx-summary-body');

    leftArrow.addEventListener('click', this.onLeftArrowClick.bind(this));
    rightArrow.addEventListener('click', this.onRightArrowClick.bind(this));
    summariesElement.forEach((summaryElement: any) => {
      summaryElement.addEventListener('click', this.onSummaryClick.bind(this));
    });
  }

  public initialize(): void {
    if (this.data.hasSelected) {
      this.hideCarousel();
    }
    const cmxContent: any = this.cmxContent;
    const carouselElement: any = cmxContent.nativeElement.querySelector('.cmx-carousel');
    const summariesElement: any = cmxContent.nativeElement.querySelectorAll('.cmx-summary-body');
    if (summariesElement.length) {
      const width: number = summariesElement[0].clientWidth;
      carouselElement.style.width = `${width * summariesElement.lenght}px)`;
    }
  }

  public unregisterEvents(): void {
    const cmxContent: any = this.cmxContent;
    const leftArrow: any = cmxContent.nativeElement.querySelector('.cmx-icon-arrow-left-rounded');
    const rightArrow: any = cmxContent.nativeElement.querySelector('.cmx-icon-arrow-right-rounded-fill');
    const summariesElement: any = cmxContent.nativeElement.querySelectorAll('.cmx-summary-body');

    leftArrow.removeEventListener('click', this.onLeftArrowClick);
    rightArrow.removeEventListener('click', this.onRightArrowClick);
    summariesElement.forEach((summaryElement: any) => {
      summaryElement.removeEventListener('click', this.onSummaryClick);
    });
  }

  public getCurrentSlide(): number {
    return this.currentSlide;
  }

  private hideCarousel(): void {
    const cmxContent: any = this.cmxContent;
    const div: any = cmxContent.nativeElement.querySelector('.cmx-carousel');
    const leftArrow: any = cmxContent.nativeElement.querySelector('.cmx-icon-arrow-left-rounded');
    const rightArrow: any = cmxContent.nativeElement.querySelector('.cmx-icon-arrow-right-rounded-fill');
    div.classList.add('cmx-hidden');
    leftArrow.classList.add('cmx-hidden');
    rightArrow.classList.add('cmx-hidden');

  }

  private moveSlide(): void {
    const removePx: any = (text: string): number => {
      const value: string = text.replace('px', '');
      return parseInt(value, 10);
    };
    const cmxContent: any = this.cmxContent;
    const carouselElement: any = cmxContent.nativeElement.querySelector('.cmx-carousel');
    const itemElement: any = cmxContent.nativeElement.querySelector('.cmx-summary-body');
    const width: number = itemElement.clientWidth;
    const style: any = window.getComputedStyle(itemElement);
    const plus: number = removePx(style.marginRight || '0');
    carouselElement.style.transform = `translateX(${-(width + plus) * this.currentSlide}px)`;
  }
}
