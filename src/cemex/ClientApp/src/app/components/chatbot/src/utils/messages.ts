export const MESSAGES: any = {
  en_US: `I am very sorry, now I can not attend to you.
    You can get more information at <a href="https://www.cemex.com/" target="_blank">https://www.cemex.com</a>.`,
  es_ES: `Lo siento mucho, ahora no te puedo atender.
    Puedes obtener más información en <a href="https://www.cemex.com/" target="_blank">https://www.cemex.com</a>.`,
};
