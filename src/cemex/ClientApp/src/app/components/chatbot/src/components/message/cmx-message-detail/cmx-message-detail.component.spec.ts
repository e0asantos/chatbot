import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmxMessageDetailComponent } from './cmx-message-detail.component';
import { CmxMessageBasicComponent } from '../cmx-message-basic/cmx-message-basic.component';
import { CmxBotAvatarComponent } from '../../cmx-bot-avatar/cmx-bot-avatar.component';

describe('CmxMessageDetailComponent', () => {
  let component: CmxMessageDetailComponent;
  let fixture: ComponentFixture<CmxMessageDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CmxMessageDetailComponent,
        CmxMessageBasicComponent,
        CmxBotAvatarComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmxMessageDetailComponent);
    fixture.componentInstance.message = {
      author: 'bot',
      data: {
        content: `Aquí te muestro el detalle del pedido
        <div class="cmx-detail-body">
          <h1>Pedido #0120022615</h1>
          <div>
            <div class="cmx-flag-status status__box--nst"></div>No Iniciado
          </div>
          <div class="cmx-detail-block">
            <div class="cmx-bold">Ubicación:</div>
            <div class="cmx-description"> A.B FAB</div>
            <div>
              <label>Dirección:</label> , CALZADA FEDERALISTAS S/N, ZAPOPAN, Jalisco, 45019
            </div>
            <div>
              <label>Código SAP:</label> 0066225867
            </div>
          </div>
          <div class="cmx-detail-block">
            <div class="cmx-bold">Punto de entrega:</div>
            <div class="cmx-description">A.B FAB</div>
            <div>
                <label>Dirección:</label> , CALZADA FEDERALISTAS S/N, ZAPOPAN, Jalisco, 45019
            </div>
            <div>
                <label>GPS:</label> 25° 45' 57.6"N 100° 9' 14.4"W
            </div>
            <div>
                <label>Código SAP:</label> 0066225867
            </div>
          </div>
          <div class="cmx-detail-block">
            <div>
                <span class="cmx-bold">Enviado:</span>
                <span class="cmx-create-order-date"> 23 ago., 2018 </span>
                <div class="cmx-hidden cmx-create-order-date-raw"> 2018-08-23T18:41:41</div>
            </div>
            <div>
                <span class="cmx-bold">Enviado por:</span>
                <span></span>
            </div>
            <div>
                <span class="cmx-bold">Orden de compra:</span>
                <span> </span>
            </div>
            <div>
                <span class="cmx-bold">Instrucciones de envío:</span>
                <span></span>
            </div>
          </div>
        </div>`,
      },
      id: '1',
      isUserLogged: false,
      sentAt: new Date(),
      showDate: true,
      type: 'detail',
      userInitials: 'US',
    };
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
