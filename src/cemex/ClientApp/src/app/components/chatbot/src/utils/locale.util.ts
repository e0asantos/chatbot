export function getPrimaryLanguage(locale: string): string {
  return locale.split('_')[0];
}
