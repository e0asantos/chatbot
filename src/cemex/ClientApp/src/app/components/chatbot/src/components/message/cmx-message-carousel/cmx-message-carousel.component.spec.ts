import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmxMessageCarouselComponent } from './cmx-message-carousel.component';
import { CmxMessageBasicComponent } from '../cmx-message-basic/cmx-message-basic.component';
import { CmxBotAvatarComponent } from '../../cmx-bot-avatar/cmx-bot-avatar.component';

describe('CmxMessageCarouselComponent', () => {
  let component: CmxMessageCarouselComponent;
  let fixture: ComponentFixture<CmxMessageCarouselComponent>;
  let onRightArrowClickSpy: any;
  let onLeftArrowClickSpy: any;
  let onSummaryClickSpy: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CmxMessageCarouselComponent,
        CmxMessageBasicComponent,
        CmxBotAvatarComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmxMessageCarouselComponent);
    fixture.componentInstance.message = {
      author: 'bot',
      data: {
        content:  `Aquí te muestro los últimos pedidos para la fecha seleccionada
        <div class="cmx-carousel-wrapper">
          <div class="cmx-carousel">
            <div class="cmx-summary-body">
              <div class="cmx-hidden-value" value="0120017940"></div>
              <h1>0120017940</h1>
              <div class="cmx-row">
                <span>Enviado:</span>
                <span>8/7/18</span>
              </div>
              <div class="cmx-row">
                <span>Orden de compra:</span>
                <span></span>
              </div>
              <div class="cmx-row cmx-status">
                <div class="cmx-flag-status status__box--blck"></div>Bloqueado
              </div>
              <div class="cmx-row">
                <span>Productos:</span>
                <span>Cemento</span>
              </div>
            </div>
            <div class="cmx-summary-body">
              <div class="cmx-hidden-value" value="0120017342"></div>
              <h1>0120017342</h1>
              <div class="cmx-row">
                <span>Enviado:</span>
                <span>10/7/18</span>
              </div>
              <div class="cmx-row">
                <span>Orden de compra:</span>
                <span></span>
              </div>
              <div class="cmx-row cmx-status">
                <div class="cmx-flag-status status__box--ohld"></div>No iniciado
              </div>
              <div class="cmx-row">
                <span>Productos:</span>
                <span>Cemento</span>
              </div>
            </div>
            <div class="cmx-summary-body">
              <div class="cmx-hidden-value" value="0120017940"></div>
              <h1>0120017940</h1>
              <div class="cmx-row">
                <span>Enviado:</span>
                <span>9/7/18</span>
              </div>
              <div class="cmx-row">
                <span>Orden de compra:</span>
                <span></span>
              </div>
              <div class="cmx-row cmx-status">
                <div class="cmx-flag-status status__box--cncl"></div>Cancelado
              </div>
              <div class="cmx-row">
                <span>Productos:</span>
                <span>Concreto</span>
              </div>
            </div>
          </div>
          <span class="cmx-icon-arrow-left-rounded" aria-hidden="true"></span>
          <span class="cmx-icon-arrow-right-rounded-fill" aria-hidden="true"></span>
        </div>`,
        hasSelected: false,
      },
      id: '1',
      isUserLogged: false,
      sentAt: new Date(),
      showDate: true,
      type: 'carousel',
      userInitials: 'US',
    };
    component = fixture.componentInstance;
    onRightArrowClickSpy = spyOn(component, 'onRightArrowClick').and.callThrough();
    onLeftArrowClickSpy = spyOn(component, 'onLeftArrowClick').and.callThrough();
    onSummaryClickSpy = spyOn(component, 'onSummaryClick').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should click event trigger on carousel item', () => {
    let divElement: any = component.cmxContent.nativeElement.querySelector('.cmx-carousel');
    expect(divElement.className).not.toContain('cmx-hidden');
    component.cmxContent.nativeElement.querySelector('.cmx-summary-body').click();
    expect(onSummaryClickSpy).toHaveBeenCalled();
    divElement = component.cmxContent.nativeElement.querySelector('.cmx-carousel');
    expect(divElement.className).toContain('cmx-hidden');
  });

  it('should click event trigger on carousel arrows', () => {
    const leftArrowElement: any = component.cmxContent.nativeElement.querySelector('.cmx-icon-arrow-left-rounded');
    const rightArrowElement: any = component.cmxContent.nativeElement.querySelector('.cmx-icon-arrow-right-rounded-fill');
    let currentSlide: number = component.getCurrentSlide();
    expect(currentSlide).toEqual(0);

    rightArrowElement.click();
    currentSlide = component.getCurrentSlide();
    expect(onRightArrowClickSpy).toHaveBeenCalled();
    expect(currentSlide).toEqual(1);

    leftArrowElement.click();
    currentSlide = component.getCurrentSlide();
    expect(onLeftArrowClickSpy).toHaveBeenCalled();
    expect(currentSlide).toEqual(0);
  });
});
