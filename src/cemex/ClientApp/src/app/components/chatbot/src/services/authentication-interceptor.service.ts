import { Injectable, Injector } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpHeaders,
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { StoreService } from './store.service';

@Injectable()
export class AuthenticationInterceptorService implements HttpInterceptor {

  constructor(
    private store: StoreService,
  ) {}

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // get the auth header from your auth service.

    const authToken: string |null = this.store.accessToken;
    const jwt: string |null = this.store.jwt;
    let headers: HttpHeaders = req.headers;

    if (authToken && jwt) {
      headers = req.headers.set('Authorization', `Bearer  ${authToken}`);
      headers = headers.set('jwt', `${jwt}`);
    }

    const authenticationRequest: HttpRequest<any> = req.clone({ headers });
    return next.handle(authenticationRequest);
  }

}
