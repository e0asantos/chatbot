import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmxMessageComplexComponent } from './cmx-message-complex.component';
import { CmxMessageBasicComponent } from '../cmx-message-basic/cmx-message-basic.component';
import { CmxBotAvatarComponent } from '../../cmx-bot-avatar/cmx-bot-avatar.component';
import { IMessage } from '../../../models/message.interface';

describe('CmxMessageComplexComponent', () => {
  let component: CmxMessageComplexComponent;
  let fixture: ComponentFixture<CmxMessageComplexComponent>;
  let registerEventsSpy: any;
  let initializeSpy: any;
  let unregisterEventsSpy: any;

  const botMessage: IMessage = {
    author: 'bot',
    data: {
      content: 'Hello world',
    },
    id: '1',
    isUserLogged: false,
    sentAt: new Date(),
    showDate: true,
    type: 'text',
    userInitials: 'US',
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CmxMessageBasicComponent,
        CmxMessageComplexComponent,
        CmxBotAvatarComponent,
      ],
    })
    .compileComponents();
  }));

  it('should create', () => {
    fixture = TestBed.createComponent(CmxMessageComplexComponent);
    component = fixture.componentInstance;
    fixture.componentInstance.message = botMessage;
    fixture.detectChanges();

    expect(component).toBeTruthy();
  });

  it('should call custom lifecycle method', () => {
    fixture = TestBed.createComponent(CmxMessageComplexComponent);
    component = fixture.componentInstance;
    fixture.componentInstance.message = botMessage;

    registerEventsSpy = spyOn(component, 'registerEvents').and.callThrough();
    initializeSpy = spyOn(component, 'initialize').and.callThrough();
    unregisterEventsSpy = spyOn(component, 'unregisterEvents').and.callThrough();

    fixture.detectChanges();

    expect(registerEventsSpy).toHaveBeenCalled();
    expect(initializeSpy).toHaveBeenCalled();

    fixture.destroy();

    expect(unregisterEventsSpy).toHaveBeenCalled();
  });

});
