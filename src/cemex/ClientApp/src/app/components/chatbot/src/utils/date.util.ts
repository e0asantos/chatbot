export function dateToUTC(date: Date): Date {
  return new Date(Date.UTC(
    date.getFullYear(),
    date.getMonth(),
    date.getDate(),
    date.getHours(),
    date.getMinutes(),
    date.getSeconds(),
  ));
}

export function formatLocalDate(utcDate: Date, language: string): string {
    const options: any = { year: 'numeric', month: 'short', day: 'numeric' };
    return utcDate.toLocaleDateString(language, options);
}

export function formatLocalTime(utcDate: Date, language: string): string {
  const options: any = { hour: '2-digit', minute: '2-digit' };
  return utcDate.toLocaleTimeString(language, options);
}
