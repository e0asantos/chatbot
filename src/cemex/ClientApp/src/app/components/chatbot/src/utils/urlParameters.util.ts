export function getUrlParameters(paramName: string): string | null {
  const  urlParams: URLSearchParams = new URLSearchParams(window.location.search);
  return urlParams.get(paramName);
}
