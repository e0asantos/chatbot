import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmxMessageBasicComponent } from './cmx-message-basic.component';
import { CmxBotAvatarComponent } from '../../cmx-bot-avatar/cmx-bot-avatar.component';
import { IMessage } from '../../../models/message.interface';

describe('CmxMessageBasicComponent', () => {
  let component: CmxMessageBasicComponent;
  let fixture: ComponentFixture<CmxMessageBasicComponent>;
  let element: HTMLElement;
  let testIndex: number = 0;
  const botMessage: IMessage = {
    author: 'bot',
    data: {
      content: 'Hello world',
    },
    id: '1',
    isUserLogged: false,
    sentAt: new Date(),
    showDate: true,
    type: 'text',
    userInitials: 'US',
  };
  const userMessage: IMessage = {
    author: 'user',
    data: {
      content: 'Hello world',
    },
    id: '1',
    isUserLogged: true,
    sentAt: new Date(),
    showDate: false,
    type: 'text',
    userInitials: 'US',
  };

  const INIT_VALUES: IMessage[] = [
    botMessage,
    userMessage,
    userMessage,
    botMessage,
    botMessage,
    userMessage,
    userMessage,
  ];
  const EXPECTED_VALUES: any[] = [
    {},
    { userNick: 'US' },
    { class: 'cmx-message-personal' },
    {},
    { isBot: true },
    { isUser: true },
    { showAvatar: true },
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CmxMessageBasicComponent,
        CmxBotAvatarComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmxMessageBasicComponent);
    component = fixture.componentInstance;
    fixture.componentInstance.message = INIT_VALUES[testIndex];
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  afterEach(() => {
    testIndex++;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init userNick value properly when user has logged', () => {
    const avatarElement: any = element.querySelector('.cmx-avatar');
    expect(avatarElement).toBeTruthy();
    const userNick: string = avatarElement.innerText;
    expect(userNick).toBeTruthy();
    expect(userNick).toEqual(EXPECTED_VALUES[testIndex].userNick);
  });

  it('should add cmx-message-personal style class when is user message', () => {
    const divElement: HTMLElement | null = element.querySelector('.cmx-message-personal');
    expect(divElement).toBeTruthy();
    expect(divElement!.className).toContain(EXPECTED_VALUES[testIndex].class);
  });

  it('should render date when have to showDate', () => {
    expect(element.querySelector('.cmx-timestamp')).toBeTruthy();
  });

  it('should return it a bot message', () => {
    expect(component.isBotMessage()).toEqual(EXPECTED_VALUES[testIndex].isBot);
  });

  it('should return it a user message', () => {
    expect(component.isUserMessage()).toEqual(EXPECTED_VALUES[testIndex].isUser);
  });

  it('should return has to show avatar', () => {
    expect(component.showAvatar()).toEqual(EXPECTED_VALUES[testIndex].showAvatar);
  });

});
