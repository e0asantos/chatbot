import { IMessageText } from './message-text.interface';
import { IMessageComplex } from './message-complex.interface';

export interface IMessage {
  data?: IMessageComplex | IMessageText;
  id: string;
  userInitials: string;
  type: string;
  author: string;
  sentAt: Date;
  showDate: boolean;
  isUserLogged: boolean;
}
