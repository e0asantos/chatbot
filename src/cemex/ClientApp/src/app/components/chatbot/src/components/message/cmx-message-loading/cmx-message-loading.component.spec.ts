import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmxMessageLoadingComponent } from './cmx-message-loading.component';
import { CmxMessageBasicComponent } from '../cmx-message-basic/cmx-message-basic.component';
import { CmxBotAvatarComponent } from '../../cmx-bot-avatar/cmx-bot-avatar.component';

describe('CmxMessageLoadingComponent', () => {
  let component: CmxMessageLoadingComponent;
  let fixture: ComponentFixture<CmxMessageLoadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CmxMessageLoadingComponent,
        CmxMessageBasicComponent,
        CmxBotAvatarComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmxMessageLoadingComponent);
    component = fixture.componentInstance;
    fixture.componentInstance.message = {
      author: 'bot',
      id: '1',
      isUserLogged: false,
      sentAt: new Date(),
      showDate: false,
      type: 'loading',
      userInitials: 'US',
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
