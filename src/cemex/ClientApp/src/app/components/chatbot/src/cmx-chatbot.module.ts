import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { CmxChatbotComponent } from './components/cmx-chatbot.component';
import { CmxMessageComponent } from './components/message/cmx-message/cmx-message.component';
import { CmxBotAvatarComponent } from './components/cmx-bot-avatar/cmx-bot-avatar.component';
import { CmxMessageTextComponent } from './components/message/cmx-message-text/cmx-message-text.component';
import { CmxMessageLoadingComponent } from './components/message/cmx-message-loading/cmx-message-loading.component';
import { CmxMessageListComponent } from './components/message/cmx-message-list/cmx-message-list.component';
import { CmxMessageBasicComponent } from './components/message/cmx-message-basic/cmx-message-basic.component';
import { CmxMessageDetailComponent } from './components/message/cmx-message-detail/cmx-message-detail.component';
import { CmxMessageComplexComponent } from './components/message/cmx-message-complex/cmx-message-complex.component';
import { CmxMessageCarouselComponent } from './components/message/cmx-message-carousel/cmx-message-carousel.component';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthenticationInterceptorService } from './services/authentication-interceptor.service';
import { MessagesService } from './services/messages.service';
import { StoreService } from './services/store.service';
import { ChatApiService } from './services/chat-api.service';
import { HistoryApiService } from './services/history-api.service';
import { ConversationService } from './services/conversation.service';
import { Broadcaster } from '@cemex-core/events-v1/dist/';

@NgModule({
  declarations: [
    CmxBotAvatarComponent,
    CmxChatbotComponent,
    CmxMessageBasicComponent,
    CmxMessageComponent,
    CmxMessageListComponent,
    CmxMessageLoadingComponent,
    CmxMessageDetailComponent,
    CmxMessageTextComponent,
    CmxMessageComplexComponent,
    CmxMessageCarouselComponent,
  ],
  exports: [
    CmxChatbotComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [
    ChatApiService,
    ConversationService,
    HistoryApiService,
    MessagesService,
    StoreService,
    Broadcaster,
    { multi: true, provide: HTTP_INTERCEPTORS, useClass: AuthenticationInterceptorService },
  ],
})
export class CmxChatbotModule {
}
