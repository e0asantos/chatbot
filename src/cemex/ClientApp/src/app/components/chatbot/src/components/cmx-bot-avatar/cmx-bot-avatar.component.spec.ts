import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmxBotAvatarComponent } from './cmx-bot-avatar.component';

describe('CmxBotAvatarComponent', () => {
  let component: CmxBotAvatarComponent;
  let fixture: ComponentFixture<CmxBotAvatarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CmxBotAvatarComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmxBotAvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
