import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class BasicApiService {

  protected url: string = 'https://de486ab8g9.execute-api.us-east-1.amazonaws.com/dev/botJS';

  protected servicePath: string = '';

  constructor(protected http: HttpClient) { }

  public GET(config: object): Observable<any> {
    console.log("get",config);
    config = {
      ... { method: 'GET' },
      ... config,
    };
    return this.callService({}, config);
  }

  public POST(payload: object, config: object): Observable<any> {
    console.log("post", payload);
    config = {
      ... { method: 'POST' },
      ... config,
    };
    const options: any = {
      body: payload,
    };
    return this.callService(options, config);
  }

  public PUT(payload: object, config: object): Observable<any> {
    console.log("put", payload);
    // return this.http.post<any>(this.getUrl('PUT'), payload, { headers: this.getHeaders() });
    return new Observable<any>();
  }

  protected getUrl(method?: string): string {
    return this.url;
  }

  protected getHeaders(): HttpHeaders {

    const headers: HttpHeaders = new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type':  'application/json',
      });
    return headers;
  }

  protected callService(options: any = {}, config: any = {}): Observable<any> {
    //lala
    options = {
      ... { headers: this.getHeaders(), responseType: "text/xml" },
      ... options,
    };
    const { method } = config;
    return this.http.request(method, this.getUrl(method), options);
  }

}
