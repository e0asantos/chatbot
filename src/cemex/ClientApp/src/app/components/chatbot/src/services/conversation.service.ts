import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { IMessage } from '../models/message.interface';
import { MessagesService, MESSAGE_SUBTYPE } from './messages.service';
import { HistoryApiService } from './history-api.service';
import { StoreService } from './store.service';

@Injectable()
export class ConversationService {

  private conversation$: Subject<IMessage[]>;

  private updateConversation$: Subject<any> = new Subject<any>();

  private conversation: IMessage[];

  private message$: Observable<any>;

  constructor(
    private messagesService: MessagesService,
    private historyService: HistoryApiService,
    private store: StoreService,
  ) {

    this.message$ = this.messagesService.getMessage();

    this.conversation = [];
    this.conversation$ = new BehaviorSubject<IMessage[]>(this.conversation);

    const onSuccess: any = (response: any) => {
      this.conversation = response;
      this.conversation$.next(this.conversation);
      // send first message to init chat
      if (this.getConversation().length === 0) {
        this.sendMessage('Hola');
      }
    };

    const onError: any = (error: any) => {
      console.log('getConversation ko', error);
      this.sendMessage('Hola');
    };

    this.initializeConversation().subscribe(onSuccess.bind(this), onError.bind(this));
  }

  public addMessage(message: IMessage): void {
    this.conversation.push(message);
    this.conversation$.next(this.conversation);
  }

  public getConversation$(): Observable<any> {
    return this.conversation$.asObservable();
  }

  public clearMessages(): void {
    this.conversation = [];
    this.conversation$.next(this.conversation);
  }

  public getConversation(): IMessage[] {
    return this.conversation;
  }

  public saveConversation(): void {
    const onSuccess: any = () => { console.log('saveConversation ok'); };
    const onError: any = (err: any) => { console.log('saveConversation ko', err); };
    // this.historyService.saveConversation(this.store.conversationId, this.conversation)
    //   .subscribe(onSuccess, onError);
  }

  public getMessage(): Observable<any> {
    return this.message$;
  }

  public getUpdateConversation$(): Observable<any> {
    return this.updateConversation$;
  }

  public createMessage(text: any, author: string, type: string = MESSAGE_SUBTYPE.TEXT): IMessage {
    this.updateMessagesServiceData();
    return this.messagesService.createMessage(text, author, type);
  }

  public sendMessage(text: string): void {
    this.updateMessagesServiceData();
    this.messagesService.sendMessage(text);
  }

  public updateConversation(): void {
    const conversationId: string = this.store.conversationId && this.store.conversationId.toString();
    const lastConversationId: string = this.store.lastConversationId && this.store.lastConversationId.toString();
    if (lastConversationId !== conversationId) {
      if (this.store.isUserLogged()) {
        this.initializeConversation().subscribe((response: any) => {
          this.conversation = response;
          this.updateConversation$.next(this.conversation);
        });
      } else {
        this.conversation = [];
        this.updateConversation$.next(this.conversation);
      }
    } else {
      this.updateConversation$.next(this.conversation);
    }
  }

  private initializeConversation(): Observable<any> {
    return this.historyService.getConversation(this.store.conversationId);
  }

  private updateMessagesServiceData(): void {
    if (this.store.language) {
      this.messagesService.language = this.store.language;
    }
    this.messagesService.isUserLogged = this.store.isUserLogged();
    this.messagesService.userInitials = this.store.getUserInitials();
  }
}
