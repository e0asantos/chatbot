import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { CmxMessageComponent } from './cmx-message.component';

describe('CmxMessageComponent', () => {
  let component: CmxMessageComponent;
  let fixture: ComponentFixture<CmxMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CmxMessageComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmxMessageComponent);
    component = fixture.componentInstance;
    fixture.componentInstance.message = {
      author: 'bot',
      data: {
        content: 'Hello world',
      },
      id: '1',
      isUserLogged: false,
      sentAt: new Date(),
      showDate: false,
      type: 'text',
      userInitials: 'US',
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
