import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ElementRef,
  HostBinding,
  ViewChildren,
  QueryList,
  AfterViewChecked,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef,
} from '@angular/core';
/* import {
  trigger,
  style,
  animate,
  transition,
} from '@angular/animations'; */
import { Observable } from 'rxjs/Observable';
import { IMessage } from '../models/message.interface';
import { MESSAGE_TYPE, MESSAGE_SUBTYPE } from '../services/messages.service';
import { ConversationService } from '../services/conversation.service';
import { ISubscription } from 'rxjs/Subscription';
import { Broadcaster } from '@cemex-core/events-v1/dist';
import { CHANGE_CHATBOT_DISPLAY } from '../cmx-chatbot.constants';

@Component({
  /* animations: [
    trigger('ngIfAnimation', [
      transition('void => *', [
        style({transform: 'translateY(100%)'}),
        animate(300),
      ]),
      transition('* => void', [
        animate(300, style({transform: 'translateY(100%)'})),
      ]),
    ]),
    trigger('showIcon', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('1.s ease-in-out', style({
          opacity: 1,
        })),
      ]),
      transition(':leave', [
        style({
          opacity: 1,
        }),
        animate('0.5s ease-in-out', style({
          opacity: 0,
        })),
      ]),
    ]),
  ], */
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'cmx-chatbot',
  styles: ["\n    .roboto-bold{font-weight:700}.roboto-medium{font-weight:500}.roboto-regular{font-weight:400}.roboto-light{font-weight:300}.roboto-thin{font-weight:100}h1,.h1-role{font-family:\"Roboto\",sans-serif;font-size:3rem;line-height:3rem;font-weight:700;color:#001B3A}h2,.h2-role,h3,.h3-role{font-family:\"Roboto\",sans-serif;font-size:1.625rem;line-height:2rem;font-weight:400;color:#001B3A}h4,.h4-role{font-family:\"Roboto\",sans-serif;font-size:2rem;line-height:2rem;font-weight:500;color:#001B3A}h5,.h5-role{font-family:\"Roboto\",sans-serif;font-size:1.25rem;line-height:1.5rem;font-weight:400;color:#001B3A}h6,.h6-role{font-family:\"Roboto\",sans-serif;font-size:.875rem;line-height:1rem;font-weight:700;color:#001B3A;text-transform:uppercase}.cmx-block-background{content:\" \";position:fixed;top:0px;left:0px;background-color:black;width:100%;height:100%;opacity:0.3;z-index:9999999}.cmx-chat{z-index:1;font-family:\"Roboto\",sans-serif;width:100%;height:100%;overflow:hidden;box-shadow:0 5px 30px rgba(0,0,0,0.2);background:rgba(0,0,0,0.5);display:flex;justify-content:space-between;flex-direction:column}.cmx-chat *{box-sizing:border-box}.cmx-chat.cmx-chat-default{z-index:99999991;height:calc(100vh - 170px);position:fixed;top:140px;bottom:30px;right:0px;width:100%}@media (min-width: 568px) and (max-width: 1024px){.cmx-chat.cmx-chat-default{width:50%}}@media (min-width: 1025px){.cmx-chat.cmx-chat-default{width:33%}}.cmx-chat .cmx-chat-title{position:relative;z-index:2;background:#002A59;color:#fff;text-align:left;height:50px;border-bottom:2px solid #EE3D42;padding-left:86px}.cmx-chat .cmx-chat-title h1{color:#fff;height:54px;line-height:54px;white-space:nowrap;font-size:.875rem;margin:0;padding:0}.cmx-chat .cmx-chat-title h1 strong{font-size:.875rem}.cmx-chat .cmx-chat-title h2{color:rgba(255,255,255,0.5);font-size:.875rem;letter-spacing:1px}.cmx-chat .cmx-chat-title .cmx-avatar{position:absolute;top:15px;left:15px;z-index:2;margin:0px;width:60px;height:60px}.cmx-chat .cmx-chat-title .cmx-close{position:absolute;right:15px;top:18px;width:18px;height:18px;opacity:1}.cmx-chat .cmx-chat-title .cmx-close:hover{opacity:0.3}.cmx-chat .cmx-chat-title .cmx-close:before,.cmx-chat .cmx-chat-title .cmx-close:after{position:absolute;left:9px;content:' ';height:18px;width:2px;background-color:#fff}.cmx-chat .cmx-chat-title .cmx-close:before{transform:rotate(45deg)}.cmx-chat .cmx-chat-title .cmx-close:after{transform:rotate(-45deg)}.cmx-chat .cmx-messages{flex:1 1 auto;color:rgba(255,255,255,0.5);overflow:hidden;position:relative;width:100%;background-color:white}.cmx-chat .cmx-messages .cmx-messages-content{position:absolute;top:0;left:0;height:100%;width:100%;padding:20px 20px 0px 20px;overflow:auto;background-color:#F4F6F9}.cmx-chat .cmx-message-box{flex:0 1 40px;background-color:#F4F6F9;padding:15px;position:relative}.cmx-chat .cmx-message-box .cmx-message-input{background:none;border:none;outline:none;resize:none;margin:0;padding:10px;width:calc(100% - 87px);background-color:#fff;line-height:50px;height:50px;margin-right:7px;font-size:13px}.cmx-chat .cmx-message-box .cmx-message-input:disabled{cursor:not-allowed}.cmx-chat .cmx-message-box .cmx-message-input::placeholder{color:#D2D2D2}.cmx-chat .cmx-message-box .cmx-message-submit{border:none;background:#76E02C;border-radius:50%;height:50px;width:50px}.cmx-chat .cmx-message-box .cmx-message-submit:hover{background:#76E02C}.cmx-chat .cmx-message-box .cmx-message-submit svg{height:20px;width:20px;transform:rotate(-45deg)}.cmx-chat-icon{position:absolute;bottom:54px;right:54px;width:34px;height:36px}@keyframes bounce{0%{transform:matrix3d(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}4.7%{transform:matrix3d(0.45, 0, 0, 0, 0, 0.45, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}9.41%{transform:matrix3d(0.883, 0, 0, 0, 0, 0.883, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}14.11%{transform:matrix3d(1.141, 0, 0, 0, 0, 1.141, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}18.72%{transform:matrix3d(1.212, 0, 0, 0, 0, 1.212, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}24.32%{transform:matrix3d(1.151, 0, 0, 0, 0, 1.151, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}29.93%{transform:matrix3d(1.048, 0, 0, 0, 0, 1.048, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}35.54%{transform:matrix3d(0.979, 0, 0, 0, 0, 0.979, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}41.04%{transform:matrix3d(0.961, 0, 0, 0, 0, 0.961, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}52.15%{transform:matrix3d(0.991, 0, 0, 0, 0, 0.991, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}63.26%{transform:matrix3d(1.007, 0, 0, 0, 0, 1.007, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}85.49%{transform:matrix3d(0.999, 0, 0, 0, 0, 0.999, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}100%{transform:matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)}}@keyframes ball{from{transform:translateY(0) scaleY(0.8)}to{transform:translateY(-10px)}}\n  "],
  templateUrl: './cmx-chatbot.component.html',
})
export class CmxChatbotComponent implements OnInit, AfterViewChecked, OnDestroy {

  @Input() public withIcon: boolean = false;

  @Input() public showIcon: boolean = true;

  @Input() public blockBackground: boolean = false;

  @Output() public closeChat: EventEmitter<any> = new EventEmitter();

  private text: string;

  private conversation$: Observable<IMessage[]>;

  private botLodingMessage: IMessage;

  private loadingBotMessage: boolean = false;

  private messageSubscription: ISubscription;

  private messageChangesSubscription: ISubscription;

  private updateConversationSubscription: ISubscription;

  @HostBinding('class') private class: string = 'cmx-chatbot';

  @ViewChildren('messages') private messages: QueryList<any>;

  constructor(
    private conversationService: ConversationService,
    public el: ElementRef,
    private eventBroadcaster: Broadcaster,
    private ref: ChangeDetectorRef,
  ) {
    this.botLodingMessage = this.conversationService.createMessage('', MESSAGE_TYPE.BOT, MESSAGE_SUBTYPE.LOADING);
  }

  public ngOnInit(): void {
    this.loadingBotMessage = false;
    this.clearText();
    this.conversation$ = this.conversationService.getConversation$();

    // after conversation update
    this.updateConversationSubscription = this.conversationService.getUpdateConversation$().subscribe(() => {
      console.log("updateConversationSubscription");
      const message: IMessage = this.conversationService.createMessage(this.text, MESSAGE_TYPE.USER);
      this.addMessage(message);
      this.loadingBotMessage = true;
      this.conversationService.sendMessage(this.text);
      this.clearText();
    });

    // after send message to bot
    this.messageSubscription = this.conversationService.getMessage().subscribe((message: IMessage) => {
      console.log("messageSubscription");
      this.loadingBotMessage = false;
      this.addMessage(message);
      this.conversationService.saveConversation();
      this.setInputFocus();
    });

    this.eventBroadcaster.on<string>(CHANGE_CHATBOT_DISPLAY).subscribe((response: any) => {
      this.showIcon = !response;
      this.ref.markForCheck();
    });

    this.setInputFocus();
  }

  public ngAfterViewChecked(): void {
    this.messageChangesSubscription = this.messages.changes.subscribe(this.scrollToBottom.bind(this));
  }

  public ngOnDestroy(): void {
    this.messageSubscription.unsubscribe();
    this.messageChangesSubscription.unsubscribe();
    this.updateConversationSubscription.unsubscribe();
  }

  private onEnter(): void {
    if (this.text && this.text.trim()) {
      this.conversationService.updateConversation();
    }
  }

  private sendHiddenText(data: any): void {
    const { value, label } = data;
    const message: IMessage = this.conversationService.createMessage(label, MESSAGE_TYPE.USER);
    this.addMessage(message);
    this.loadingBotMessage = true;
    this.conversationService.sendMessage(value);
  }

  private addMessage(message: IMessage): void {
    console.log("addMessage");
    this.conversationService.addMessage(message);
  }

  private clearText(): void {
    this.text = '';
  }

  private scrollToBottom(): void {
    const scrollPane: any = this.el.nativeElement.querySelector('.cmx-messages-content');
    if (scrollPane) {
      scrollPane.scrollTop = scrollPane.scrollHeight - scrollPane.clientHeight;
    }
  }

  private toggleShowIcon(): void {
    this.showIcon = !this.showIcon;
  }

  private onClickCloseButton(): boolean {
    this.toggleShowIcon();
    this.closeChat.emit(true);
    return false;
  }

  private onSendMessage(data: any): void {
    console.log("onSendMessage");
    this.sendHiddenText(data);
  }

  private setInputFocus(): void {
    const setFocus: any = () => {
      const inputElement: HTMLElement = this.el.nativeElement.querySelector('.cmx-message-input');
      if (inputElement) {
        inputElement.focus();
      }
    };
    setTimeout(setFocus.bind(this), 0);
  }
}
