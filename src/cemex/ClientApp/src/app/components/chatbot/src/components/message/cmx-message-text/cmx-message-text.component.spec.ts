import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmxMessageTextComponent } from './cmx-message-text.component';
import { CmxMessageBasicComponent } from '../cmx-message-basic/cmx-message-basic.component';
import { CmxBotAvatarComponent } from '../../cmx-bot-avatar/cmx-bot-avatar.component';
import { IMessage } from '../../../models/message.interface';

describe('CmxMessageTextComponent', () => {
  let component: CmxMessageTextComponent;
  let fixture: ComponentFixture<CmxMessageTextComponent>;
  let element: HTMLElement;
  const botMessage: IMessage = {
    author: 'bot',
    data: {
      content: 'Hello world',
    },
    id: '1',
    isUserLogged: false,
    sentAt: new Date(),
    showDate: true,
    type: 'text',
    userInitials: 'US',
  };

  const EXPECTED_VALUE: string = 'Hello world';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CmxMessageTextComponent,
        CmxMessageBasicComponent,
        CmxBotAvatarComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmxMessageTextComponent);
    component = fixture.componentInstance;
    fixture.componentInstance.message = botMessage;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render init value text', () => {
    const textElement: HTMLElement | null = element.querySelector('.cmx-text');
    expect(textElement).toBeTruthy();

    const renderText: any = textElement!.innerHTML;
    expect(renderText).toBeTruthy();
    expect(renderText).toEqual(EXPECTED_VALUE);
  });

});
