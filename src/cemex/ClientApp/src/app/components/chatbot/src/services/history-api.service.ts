import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BasicApiService } from './basic-api.service';
import { ConversationService } from './conversation.service';
import { Observable } from 'rxjs/Observable';
import { IMessage } from '../models/message.interface';

@Injectable()
export class HistoryApiService extends BasicApiService {

  protected servicePath: string = '/store';

  private conversationId: string;

  constructor(
    protected http: HttpClient,
  ) {
    super(http);
  }

  public getConversation(conversationId: string): Observable<any> {
    this.conversationId = conversationId;
    return this.GET({}).pipe(map((response: any) => {
      const conversation: string = response.conversation || '[]';
      return JSON.parse(conversation);
    }));
  }

  public saveConversation(conversationId: string, conversation: IMessage[]): Observable<any> {
    this.conversationId = conversationId;
    const conversationString: string = JSON.stringify(conversation);
    const payload: any = {
      conversation: conversationString,
      id: this.conversationId,
    };
    return this.PUT(payload, {});
  }

  protected getUrl(method: string): string {
    let url: string = super.getUrl();
    url = `${url}${this.servicePath}`;

    if (method === 'GET') {
      url = `${url}?id=${this.conversationId}`;
    }

    return url;
  }

  protected getHeaders(): HttpHeaders {
    return new HttpHeaders({ 'Content-Type': 'application/json' });
  }
}
