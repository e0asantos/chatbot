import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BasicApiService } from './basic-api.service';
import { StoreService } from './store.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ChatApiService extends BasicApiService {

  protected servicePath: string = '';

  private _serviceSource: string = '';

  private _enviroment: string = '';

  constructor(
    protected http: HttpClient,
    protected store: StoreService,
  ) {
    super(http);
  }

  public sendMessage(text: string, config: any = {}): Observable<any> {
    const payload: any = {
      id: this.store.conversationId,
      Body: text,
      From: "4524345"
    };
    return this.POST(payload, config).pipe(map((response: any) => {
      const { id } = response;
      this.store.conversationId = id;
      this.store.lastConversationId = id;
      return response;
    }));
  }

  set serviceSource(serviceSource: string) {
    this._serviceSource = serviceSource;
  }

  set enviroment(enviroment: string) {
    this._enviroment = enviroment;
  }

  protected getUrl(): string {
    let url: string = super.getUrl();
    // url = `${url}${this.servicePath}?service=${this._serviceSource}&enviroment=${this._enviroment}`;
    const customerCode: string | null = this.store.customerCode;
    const countryCode: string | null = this.store.countryCode;
    const legalEntityId: string | null = this.store.legalEntityId;

    if (customerCode) {
      url = `${url}&customerCode=${customerCode}`;
    }

    if (countryCode) {
      url = `${url}&countryCode=${countryCode}`;
    }

    if (legalEntityId) {
      url = `${url}&legalEntityId=${legalEntityId}`;
    }

    return url;
  }

}
