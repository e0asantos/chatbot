import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmxMessageListComponent } from './cmx-message-list.component';
import { CmxMessageBasicComponent } from '../cmx-message-basic/cmx-message-basic.component';
import { CmxBotAvatarComponent } from '../../cmx-bot-avatar/cmx-bot-avatar.component';

describe('CmxMessageListComponent', () => {
  let component: CmxMessageListComponent;
  let fixture: ComponentFixture<CmxMessageListComponent>;
  let onOptionClickSpy: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CmxMessageListComponent,
        CmxMessageBasicComponent,
        CmxBotAvatarComponent,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmxMessageListComponent);
    component = fixture.componentInstance;
    fixture.componentInstance.message = {
      author: 'bot',
      data: {
        content: 'Hello world<div><span value="Opcion 1">Hola</span><span value="Opcion 2">Adios</span></div>',
        hasSelected: false,
      },
      id: '1',
      isUserLogged: false,
      sentAt: new Date(),
      showDate: false,
      type: 'list',
      userInitials: 'US',
    };
    onOptionClickSpy = spyOn(component, 'onOptionClick').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should click event trigger', () => {
    let divElement: any = component.cmxContent.nativeElement.querySelector('div');
    expect(divElement.className).not.toContain('cmx-hidden');
    component.cmxContent.nativeElement.querySelector('span').click();
    expect(onOptionClickSpy).toHaveBeenCalled();
    divElement = component.cmxContent.nativeElement.querySelector('div');
    expect(divElement.className).toContain('cmx-hidden');
  });
});
