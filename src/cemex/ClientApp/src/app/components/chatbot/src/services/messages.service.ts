import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { IMessage } from '../models/message.interface';
import { IMessageText } from '../models/message-text.interface';
import { uuid } from '../utils/uuid.util';
import { ChatApiService } from './chat-api.service';
import { getUrlParameters } from '../utils/urlParameters.util';
import { getPrimaryLanguage } from '../utils/locale.util';
import { MESSAGES } from '../utils/messages';

export const MESSAGE_TYPE: any = {
  BOT: 'bot',
  USER: 'user',
};

export const MESSAGE_SUBTYPE: any = {
  CAROUSEL: 'carousel',
  DETAIL: 'detail',
  LIST: 'list',
  LOADING: 'loading',
  TEXT: 'text',
};

@Injectable()
export class MessagesService {

  public language: string;

  public isUserLogged: boolean;

  public userInitials: string;

  private message$: Subject<IMessage> = new Subject<IMessage>();

  private lastDisplaySendMessage: Date;

  constructor(
    private chatApiService: ChatApiService,
  ) {
    const service: string | null = getUrlParameters('service');
    const enviroment: string | null = getUrlParameters('enviroment');
    if (service) {
      this.chatApiService.serviceSource = service;
    }
    if (enviroment) {
      this.chatApiService.enviroment = enviroment;
    }
  }

  public sendMessage(text: string): void {
    this.chatApiService.sendMessage(text).subscribe(this.onSuccess.bind(this), this.onError.bind(this));
  }

  public getMessage(): Observable<any> {
    return this.message$.asObservable();
  }

  public createMessage(text: any, author: string, type: string): IMessage {
    if (type === MESSAGE_SUBTYPE.LOADING) {
      return {
        author: MESSAGE_TYPE.BOT,
        id: uuid(),
        isUserLogged: false,
        sentAt: new Date(),
        showDate: false,
        type: MESSAGE_SUBTYPE.LOADING,
        userInitials: '',
      };
    }
    const date: Date = new Date();
    const showDate: boolean = !this.isSameDate(date);
    if (showDate) {
      this.lastDisplaySendMessage = date;
    }

    return {
      author,
      data: this.getData(type, text),
      id: uuid(),
      isUserLogged: this.isUserLogged,
      sentAt: date,
      showDate,
      type,
      userInitials: this.userInitials,
    };
  }

  private createErrorMessage(text: any, author: string, type: string): IMessage {
    const message: IMessage = this.createMessage(text, author, type);
    if (message.data) {
      (message.data as IMessageText).isErrorMessage = true;
    }

    return message;
  }

  private guid() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
  }

  private onSuccess(response: any): void {
    console.log("onSuccess", response);
    let parser = new DOMParser();
    let xmlDoc = parser.parseFromString(response,"text/xml");
    const plainText = xmlDoc.getElementsByTagName("Message")[0].childNodes[0].nodeValue;
    // id: "37e2f600-c769-4ea6-9d6e-facf3fc1fcc8"
    // text: "Hola, mi nombre es Cemex Chatbot. ¿Qué puedo hacer por ti?"
    // type: "text"
    const { text: answer, type } = response;
    const message: IMessage = this.createMessage(plainText, MESSAGE_TYPE.BOT, "text");
    this.message$.next(message);
  }

  private onError(response: any): void {
    console.log("onError", response);
    const answer: string = this.getErrorTextByLanguage();
    const message: IMessage = this.createErrorMessage(answer, MESSAGE_TYPE.BOT, MESSAGE_SUBTYPE.TEXT);
    this.message$.next(message);
  }

  private isSameDate(date: Date): boolean {
    if (!this.lastDisplaySendMessage) {
      return false;
    }
    const currentDate: string = date.toDateString().split(',')[0];
    const lastDisplayDate: string = this.lastDisplaySendMessage.toDateString().split(',')[0];

    return currentDate === lastDisplayDate;
  }

  private getErrorTextByLanguage(): string {
    let languageISO: string = 'en_US';
    if (this.language) {
      const language: string = this.language;
      const prefix: string = getPrimaryLanguage(language);
      languageISO = prefix === 'es' ? 'es_ES' : 'en_US';
    }
    return MESSAGES[languageISO];
  }

  private getData(type: string, text: any): any {
    let data: any;

    if (type === MESSAGE_SUBTYPE.TEXT) {
      data = { content: text };
    } else if (type === MESSAGE_SUBTYPE.LIST || type === MESSAGE_SUBTYPE.DETAIL || type === MESSAGE_SUBTYPE.CAROUSEL) {
      data = {
        content: text,
        hasSelected: false,
      };
    }

    return data;
  }
}
