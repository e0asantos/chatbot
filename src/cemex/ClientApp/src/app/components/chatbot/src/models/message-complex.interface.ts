export interface IMessageComplex {
  content: string;
  hasSelected?: boolean;
}
