import {
  Component,
  OnInit,
  Input,
  HostBinding,
  ElementRef,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  Output,
  EventEmitter,
} from '@angular/core';
import { IMessage } from '../../../models/message.interface';
import { IMessageComplex } from '../../../models/message-complex.interface';

@Component({
  selector: 'cmx-message-complex',
  templateUrl: './cmx-message-complex.component.html',
})
export class CmxMessageComplexComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('cmxContent') public cmxContent: ElementRef;

  @Output() public sendMessage: EventEmitter<any> = new EventEmitter();

  public data: IMessageComplex;

  @Input() public message: IMessage;

  @HostBinding('class') protected class: string = 'cmx-message-complex';

  public ngOnInit(): void {
    this.data = this.message.data as IMessageComplex;
  }

  public ngAfterViewInit(): void {
    this.registerEvents();
    this.initialize();
  }

  public ngOnDestroy(): void {
    this.unregisterEvents();
  }

  public registerEvents(): void {
    // extend class implementation
  }

  public initialize(): void {
    // extend class implementation
  }

  public unregisterEvents(): void {
    // extend class implementation
  }

}
