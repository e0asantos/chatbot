import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ProfileBoxComponent } from './profile-box.component';
import { PasswordChangeModalModule } from '../../components/modals/password-change/password-change.modal.module';
import { CmxButtonModule } from '@cemex/cmx-button-v4';

@NgModule({
    imports: [
        CommonModule,
        FlexLayoutModule,
        PasswordChangeModalModule,
        CmxButtonModule,
        RouterModule
    ],
    exports: [
        ProfileBoxComponent
    ],
    declarations: [
        ProfileBoxComponent
    ]
})

export class ProfileBoxModule { }
