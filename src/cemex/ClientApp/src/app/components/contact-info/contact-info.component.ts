import { Component, Input } from '@angular/core';

// Providers
import { TranslationService } from '@cemex-core/angular-localization-v1/dist/';

// Models
import { ContactInfo } from '../../models';

@Component({
    selector: 'contact-info',
    templateUrl: 'contact-info.component.html',
    styleUrls: ['contact-info.component.scss']

})

export class ContactInfoComponent {

    public _contactInfo: ContactInfo;

    @Input() withNumber: boolean = false;

    @Input()
    get data(): ContactInfo {
        return this._contactInfo;
    }
    set data(value: ContactInfo) {
        this._contactInfo = value;
    }

    constructor(public translationService: TranslationService) { }
}
