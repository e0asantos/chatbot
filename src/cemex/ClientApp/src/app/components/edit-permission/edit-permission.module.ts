import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// CEMEX Dependencies
import { CmxCheckboxModule } from '@cemex/cmx-checkbox-v4';
import { CmxDropdownModule } from '@cemex/cmx-dropdown-v4';

// Components
import { EditPermissionComponent } from './edit-permission.component';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        CmxCheckboxModule,
        CmxDropdownModule,
    ],
    declarations: [
        EditPermissionComponent,
    ],
    exports: [
        EditPermissionComponent,
    ]
})
export class EditPermissionModule { }
