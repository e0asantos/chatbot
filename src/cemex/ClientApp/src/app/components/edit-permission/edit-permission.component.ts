import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

// Providers
import { TranslationService } from '@cemex-core/angular-localization-v1/dist/';

// Models
import { Role } from '../../models';

@Component({
    selector: 'edit-permission',
    templateUrl: 'edit-permission.component.html',
    styleUrls: ['edit-permission.component.scss']
})
export class EditPermissionComponent implements OnInit {

    @Input('permission')
    public permission;
    @Input()
    public counter: number;
    @Input()
    public templateId: string;
    @Input()
    public isRTL: boolean = false;
    @Output()
    public onRoleToggled = new EventEmitter();
    @Output()
    public onRemovePermission = new EventEmitter();
    @Input()
    public isDigitalAdmin: boolean;

    public rolePlaceholder = '';

    constructor(public translationService: TranslationService) {
    }

    public ngOnInit(): void {
        this.setRolePlaceholder();
    }

    public toggleRole(event: boolean, role: Role) {
        if (typeof event === 'boolean') {
            const roleHandler = {
                role,
                appId: this.permission.applicationId,
                added: event
            };

            const index = this.permission.rolesViewModel.findIndex((r) => r.roleId === role.roleId);
            if (index !== -1) {
                this.permission.rolesViewModel[index].assigned = event;
            }
            this.setRolePlaceholder();
            this.onRoleToggled.emit(roleHandler);
        }
    }

    public removePermission() {
        this.onRemovePermission.emit(this.permission);
    }

    private setRolePlaceholder(translate?: boolean): void {
        if (this.permission.rolesViewModel.length > 0) {
            const assignedRoles = this.permission.rolesViewModel.filter((role) => role.assigned === true);
            if (assignedRoles.length > 0) {
                this.rolePlaceholder = assignedRoles.map((r) => {
                    return this.translationService.pt(r.roleCode);
                }).join(', ');
            } else {
                this.rolePlaceholder = this.translationService.pt('forms.placeholder.roles');
            }
        }
        if (this.isDigitalAdmin) {
            this.rolePlaceholder = this.translationService.pt('views.profile.assigned.all.roles');
        }
    }
}
