import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Providers
import { PasswordService } from '../../../services/';

// CMX Dependencies
import { CmxDialogModule } from '@cemex/cmx-dialog-v4';
import { CmxButtonModule } from '@cemex/cmx-button-v4/dist';

import { LoaderModule } from '../../loader/loader.module';
import { PasswordChangeModal } from './password-change.modal';

@NgModule({
    imports: [
        CmxButtonModule,
        CommonModule,
        LoaderModule,
        CmxDialogModule
    ],
    exports: [
        PasswordChangeModal
    ],
    declarations: [
        PasswordChangeModal
    ],
    providers: [
        PasswordService
    ]
})
export class PasswordChangeModalModule { }
