import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

// CMX Dependencies
import { CmxButtonModule } from '@cemex/cmx-button-v4/dist';
import { CmxCheckboxModule } from '@cemex/cmx-checkbox-v4/dist';
import { CmxDialogModule } from '@cemex/cmx-dialog-v4/dist';
import { CmxSlideToggleModule } from '@cemex/cmx-slide-toggle-v4/dist';

// Internal Dependencies
import { TablesModule } from '../../../components/tables/table.module';
import { SearchModule } from '../../../components/search/search.module';

import { JobsitesModalComponent } from './jobsites.modal';

@NgModule({
    declarations: [JobsitesModalComponent],
    imports: [
        FormsModule,
        CommonModule,
        ReactiveFormsModule,
        TablesModule,
        SearchModule,
        CmxCheckboxModule,
        CmxDialogModule,
        CmxButtonModule,
        CmxSlideToggleModule
    ],
    exports: [JobsitesModalComponent]
})

export class JobsitesModalModule { }
