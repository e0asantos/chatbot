import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Components
import { SearchComponent } from './search.component';

@NgModule({
    imports: [
        FormsModule,
        CommonModule
    ],
    exports: [
        SearchComponent
    ],
    declarations: [
        SearchComponent
    ]
})
export class SearchModule { }
