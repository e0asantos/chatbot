import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PasswordTooltipComponent } from './password-tooltip.component';

@NgModule({
    declarations: [PasswordTooltipComponent],
    imports: [CommonModule],
    exports: [PasswordTooltipComponent]
})

export class PasswordTooltipModule {  }
