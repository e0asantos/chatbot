import { Component, OnChanges, Input, SimpleChange } from '@angular/core';

// Providers
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';

@Component({
    selector: 'password-bar',
    templateUrl: 'password-bar.component.html',
    styleUrls: ['password-bar.component.scss']
})
export class PasswordBarComponent implements OnChanges {
    @Input() public passwordToCheck: string;
    @Input() public barLabel: string;
    public bar0: string;
    public bar1: string;
    public bar2: string;
    public status: string = '';
    public statusColor: string = '';
    public enough = false;

    private colors = ['#E74B3B', '#F9992B', '#7AC943'];

    constructor(public t: TranslationService) {
    }

    public ngOnChanges(changes: { [propName: string]: SimpleChange }): void {
        const password = changes['passwordToCheck'].currentValue;
        this.setBarColors(5, '#DDD');
        if (password) {
            const c = this.getStrengthIndexAndColor(password);
            this.setBarColors(c.idx, c.col);
        } else {
            this.status = 'views.passwordStrength.lblWeak';
        }
    }

    private getColor(s: number) {
        let idx = 0;
        if (s <= 10) {
            idx = 0;
            this.status = 'views.passwordStrength.lblWeak';
            this.statusColor = this.colors[idx];
            this.enough = false;
        } else if (s <= 20 || s <= 30) {
            idx = 1;
            this.status = 'views.passwordStrength.lblMedium';
            this.statusColor = this.colors[idx];
            this.enough = false;
        } else {
            idx = 2;
            this.status = 'views.passwordStrength.lblStrong';
            this.statusColor = this.colors[idx];
            this.enough = true;
        }
        return {
            idx: idx + 1,
            col: this.colors[idx]
        };
    }

    private getStrengthIndexAndColor(password: string) {
        return this.getColor(this.measure(password));
    }

    private setBarColors(count: number, col: string) {
        for (let _n = 0; _n < count; _n++) {
            this['bar' + _n] = col;
        }
    }

    private measure(p: string) {
        let _force = 0;
        const _regex = /[@#$%^&+=!?]/g; // "

        const _lowerLetters = /[a-z]+/.test(p);
        const _upperLetters = /[A-Z]+/.test(p);
        const _numbers = /[0-9]+/.test(p);
        const _symbols = _regex.test(p);

        const _flags = [_lowerLetters, _upperLetters, _numbers, _symbols];

        let _passedMatches = 0;
        for (const _flag of _flags) {
            _passedMatches += _flag === true ? 1 : 0;
        }

        _force += 2 * p.length + ((p.length >= 10) ? 1 : 0);
        _force += _passedMatches * 10;

        // penality (short password)
        _force = (p.length <= 8) ? Math.min(_force, 10) : _force;

        // penality (poor variety of characters)
        _force = (_passedMatches === 1) ? Math.min(_force, 10) : _force;
        _force = (_passedMatches === 2) ? Math.min(_force, 20) : _force;
        _force = (_passedMatches === 3) ? Math.min(_force, 30) : _force;

        return _force;
    }
}
