import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Providers
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';

// Components
import { PasswordBarComponent } from './password-bar.component';

@NgModule({
    declarations: [
        PasswordBarComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        PasswordBarComponent
    ],
    providers: [TranslationService],
})
export class PasswordBardModule { }
