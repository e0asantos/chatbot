import { FormControl, AbstractControl, ValidatorFn } from '@angular/forms';

export class NumberValidators {

    public static isNumeric(c: FormControl): any {
        if (!isNaN(parseFloat(c.value)) && isFinite(c.value)) {
            return null;
        } else {
            return {
                notNumeric: true,
            };
        }
    }

    public static onlyTwoDecimals(c: FormControl): any {
        if (NumberValidators.regexTwoDecimals.test(c.value)) {
            return null;
        } else {
            return {
                patternInvalid: true
            };
        }
    }

    public static greaterThanZero(c: FormControl): any {
        if (parseFloat(c.value) > 0) {
            return null;
        } else {
            return {
                lessThanZero: true,
            };
        }
    }

    public static isPositive(c: FormControl): any {
        if (parseFloat(c.value) >= 0) {
            return null;
        } else {
            return {
                isNegative: true,
            };
        }
    }

    public static lessThan(max: number): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            const num = +control.value;
            if (isNaN(num) || num > max) {
                return {
                    isGreater: true
                };
            }
            return null;
        };
    }

    public static greaterThan(min: number): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            const num = +control.value;
            if (isNaN(num) || num < min) {
                return {
                    isLess: true
                };
            }
            return null;
        };
    }

    private static regexTwoDecimals = /^-?[0-9]+(\.[0-9]{1,2})?$/;
}
