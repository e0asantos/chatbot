import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

// CMX Dependencies
import { CmxDialogModule } from '@cemex/cmx-dialog-v4';
import { CmxButtonModule } from '@cemex/cmx-button-v4';
import { CmxCheckboxModule } from '@cemex/cmx-checkbox-v4';
import { CmxStepsModule } from '@cemex/cmx-steps-v4';

// Internal Dependencies
import { NoInfoModule } from './../../components/no-info/no-info.module';
import { PipesModule } from '../../pipes/pipes.module';

// Components
import { DashboardComponent } from './dashboard.component';

export const ROUTES: Routes = [
    { path: '', component: DashboardComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES),
        CommonModule,
        NoInfoModule,
        CmxDialogModule,
        CmxButtonModule,
        CmxCheckboxModule,
        CmxStepsModule,
        PipesModule
    ],
    declarations: [
        DashboardComponent
    ],
})

export class DashboardModule { }
