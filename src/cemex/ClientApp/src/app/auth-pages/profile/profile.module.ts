import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';

// CMX Dependencies
import { CmxDialogModule } from '@cemex/cmx-dialog-v4';
import { CmxDropdownModule } from '@cemex/cmx-dropdown-v4';
import { CmxTabsModule } from '@cemex/cmx-tabs-v4';
import { CmxButtonModule } from '@cemex/cmx-button-v4';
import { CmxFormFieldModule } from '@cemex/cmx-form-field-v4';
import { AlertModule } from '@cemex/cmx-alert-v4/dist';

// Internal Dependencies
import { TablesModule } from './../../components/tables/table.module';
import { ButtonsFooterModule } from './../../components/buttons-footer/buttons-footer.module';
import { SearchModule } from './../../components/search/search.module';
import { NoInfoModule } from '../../components/no-info/no-info.module';
import { ProfileBoxModule } from '../../components/profile-box/profile-box.module';
import { BreadcrumbsModule } from '../../components/breadcrumbs/breadcrumbs.module';
import { PipesModule } from '../../pipes/pipes.module';
import { JobsitesModalModule } from '../../components/modals/jobsites/jobsites.modal.module';

// Providers
import { JobsiteService, CustomerService, ApplicationService, RequestService } from '../../services';

// Utils
import { SharedFunctions, SearchFunctions } from '../../helpers/functions';

import { ProfileComponent } from './profile.component';

export const ROUTES: Routes = [
    { path: '', component: ProfileComponent },
    {
        path: 'permission-request',
        loadChildren: '../permission-request/permission-request.module#PermissionRequestModule'
    },
    {
        path: 'data-request',
        loadChildren: '../data-request/data-request.module#DataRequestModule'
    },
    {
        path: 'self-reset-password',
        loadChildren: '../self-reset-password/self-reset-password.module#SelfResetPasswordModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES),
        CommonModule,
        FlexLayoutModule,
        NoInfoModule,
        SearchModule,
        ButtonsFooterModule,
        TablesModule,
        ProfileBoxModule,
        CmxTabsModule,
        CmxDropdownModule,
        CmxButtonModule,
        CmxDialogModule,
        BreadcrumbsModule,
        CmxFormFieldModule,
        JobsitesModalModule,
        AlertModule,
        PipesModule
    ],
    declarations: [
        ProfileComponent
    ],
    exports: [
        ProfileComponent
    ],
    providers: [
        ApplicationService,
        CustomerService,
        JobsiteService,
        RequestService,
        SearchFunctions,
        SharedFunctions
    ],
})

export class ProfileModule { }
