import { Component, OnInit, ViewChild, ChangeDetectorRef, AfterViewChecked  } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// Providers
import { PasswordService, ErrorService } from '../../services';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';

// CMX Dependencies
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4/dist';

// Internal Dependencies
import { LocalizationComponent } from '../../components/localization/localization.component';
import { PasswordTooltipComponent } from '../../components/password-tooltip/password-tooltip.component';

// Utils
import { PasswordValidator } from '../../helpers/validators';
import { DCMConstants } from '../../helpers/DCM.constants';

// Models
import { EmailStatus, ApiError } from '../../models';

@Component({
    selector: 'app-self-reset-password',
    templateUrl: 'self-reset-password.component.html',
    styleUrls: ['self-reset-password.component.scss']
})
export class SelfResetPasswordComponent extends LocalizationComponent implements OnInit, AfterViewChecked {

    public form: FormGroup;
    public submitted = false;
    public wrongPassword = false;

    public breadCrumbs: any[] = [];
    public validPass = true;
    public emailSent = true;
    private userProfile = JSON.parse(sessionStorage.getItem('user_profile'));

    public showCurrentPassword: boolean;
    public showNewtPassword: boolean;
    public showConfirm: boolean;

    @ViewChild('successModal')
    private successModal: CmxDialogComponent;
    @ViewChild('passtool')
    private passtool: PasswordTooltipComponent;

    constructor(
        public translationService: TranslationService,
        private formBuilder: FormBuilder,
        private passwordService: PasswordService,
        private cdr: ChangeDetectorRef,
        private router: Router,
        private errorService: ErrorService) {
        super();
        this.form = this.formBuilder.group({
            currentPassword: ['', Validators.compose([
                Validators.required,
            ])],
            password: ['', Validators.compose([
                Validators.required,
                Validators.maxLength(20),
                Validators.pattern(DCMConstants.REGULAR_EXPRESSIONS.Password)
            ])],
            confirmPassword: ['', Validators.compose([
                Validators.required,
            ])],
        }, { validator: PasswordValidator.match('password', 'confirmPassword') });
    }

    public ngAfterViewChecked() {
        this.cdr.detectChanges();
    }

    public ngOnInit(): void {
        this.breadCrumbs = [
            { item: 'views.selfResetPassword.profileRoute', link: '/profile' },
            { item: 'views.selfResetPassword.title' },
        ];
        this.successModal.afterClosed().subscribe((result: boolean) => {
            this.router.navigate(['/profile']);
        });
        const countryCode = sessionStorage.getItem('country');
        this.passtool.getRulePassword(countryCode);
    }

    public save(): void {
        const data: any = {
            currentpassword: this.form.get('currentPassword').value,
            newpassword: this.form.get('password').value
        };
        this.submitted = true;
        this.passwordService
            .resetPassword(data)
            .subscribe((response) => {
                this.form.reset();
                const _userEmail = sessionStorage.getItem('username');
                if (response && !response['emailIsSent']) {
                    const emailStatus: EmailStatus = {
                        email: _userEmail,
                        sent: false
                    };
                    this.errorService.emailStatus = emailStatus;
                }
                this.successModal.open();
            },
                (error) => {
                    this.errorService
                        .handleError(error)
                        .subscribe((apiError: ApiError) => {
                            if (apiError && apiError.errorCode === 'O401') {
                                this.wrongPassword = true;
                            }
                        });
                    this.submitted = false;
                    this.form.get('confirmPassword').markAsUntouched();
                }
            );
    }

    public validPassword() {
        this.passtool.firstName = this.userProfile.firstName !== null ? this.userProfile.firstName.trim() : '';
        this.passtool.lastName = this.userProfile.lastName !== null ? this.userProfile.lastName.trim() : '';
        this.passtool.userAcount = this.userProfile.userAccount !== null ? this.userProfile.userAccount.trim() : '';
        this.passtool.active = true;
        setTimeout(function e(this) {
            const input = document.getElementById('cmx-input-password-SELF-RESET-PASSWORD');
            input.removeAttribute('readonly');
            window.scrollTo(0, document.body.scrollHeight);
            input.focus();
        }, 0);
    }

    public OnValidPassword() {
        this.passtool.firstName = this.userProfile.firstName !== null ? this.userProfile.firstName.trim() : '';
        this.passtool.lastName = this.userProfile.lastName !== null ? this.userProfile.lastName.trim() : '';
        this.passtool.userAcount = this.userProfile.userAccount !== null ? this.userProfile.userAccount.trim() : '';
        this.validPass = this.passtool.isPassValid();
        setTimeout(function e() {
            const input = document.getElementById('cmx-input-password-SELF-RESET-PASSWORD');
            input.setAttribute('readonly', 'true');
            input.blur();
        }, 0);
    }

    public getValid() {
        this.validPass = this.passtool.isPassValid();
        return (!this.form.valid || this.submitted || !this.validPass);
    }
}
