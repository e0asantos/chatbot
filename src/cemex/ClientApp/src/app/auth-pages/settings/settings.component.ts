import { Component, OnInit } from '@angular/core';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist/';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Language } from '../../models/language';
import { LocalizationComponent } from '../../components/localization/localization.component';
@Component({
    selector: 'settings',
    templateUrl: 'settings.component.html',
    styleUrls: ['settings.component.scss']
})

export class SettingsComponent extends LocalizationComponent implements OnInit {

    public languages = new BehaviorSubject<Language[]>([]);
    public placeholder = '';
    public breadCrumbs: any[] = [];

    public timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;

    constructor(public translationService: TranslationService) {
        super();
    }

    public ngOnInit(): void {
        this.breadCrumbs = [
            { item: 'views.userManagement.homeRoute', link: '/dashboard' },
            { item: 'views.settings.title' },
        ];
        this.placeholder = this.translationService.selectedLanguage.languageName;
        this.getLanguages();
    }

    public get userCountry(): string {
        let country = '';
        country = sessionStorage.getItem('country') || localStorage.getItem('country') || '';
        return country;
    }

    private getLanguages(): void {
        this.translationService.getLanguages().subscribe(
            (data) => {
                this.languages.next(data);
            },
            (error) => {
                console.warn('Could not set languages on middelware, setting them from window. Error: ' + error);
                const languages = window['CMX_LANGUAGES'];
                this.languages.next(languages);
            }
        );
    }

    private changeLanguage(lang: Language): void {
        this.placeholder = lang.languageName;
        this.translationService.setLanguage(lang.languageISO);
    }
}
