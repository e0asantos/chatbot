import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

// Providers
import { SessionService, Logger } from '@cemex-core/angular-services-v2/dist';
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';
import { CountlyService } from '@cemex-core/helpers-v1/dist';
import { RequestService, UserService, ErrorService } from '../../services';

// Utils
import { SharedFunctions } from '../../helpers/functions';

// CMX Dependencies
import { AlertComponent } from '@cemex/cmx-alert-v4/dist/components/alert.component';
import { CmxDropdownComponent } from '@cemex/cmx-dropdown-v4/dist/components/cmx-dropdown';
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4/';

// Components
import { LocalizationComponent } from './../../components/localization/localization.component';

// Models
import { Request, RequestStatus, UserProfile, EmailStatus, ApiError } from '../../models';

import * as _ from 'lodash';
import { DCMConstants } from '../../helpers/DCM.constants';

@Component({
    selector: 'app-user-management',
    templateUrl: 'user-management.component.html',
    styleUrls: ['user-management.component.scss']
})

export class UserManagementComponent extends LocalizationComponent implements OnDestroy, OnInit {

    @ViewChild('requestDetail')
    public requestDetail: CmxDialogComponent;

    @ViewChild('successModal')
    public successModal: CmxDialogComponent;

    @ViewChild('errorModal')
    public errorModal: CmxDialogComponent;

    @ViewChild('requestCancelConfirmDialog')
    public requestCancelConfirmDialog: CmxDialogComponent;

    @ViewChild('requestDropdown')
    public requestDropdown: CmxDropdownComponent;

    @ViewChild('emailAlert') private emailAlert: AlertComponent;

    public profilePicPath = 'dist/assets/images/no-profile.jpg';
    public selectedRequest: Request;
    public requestStatuses: RequestStatus[] = [
        { name: 'views.userManagement.activeRequests', code: 'all' },
        { name: 'views.global.statusApproved', code: 'a' },
        { name: 'views.global.statusRejected', code: 'r' }
    ];
    public requestStatusSelected = '';
    public user: UserProfile;
    public tabSelected = 'inbox';
    public requestSuccessMessage = '';
    public rejectComment = '';
    public pendingRequestTotal = 0;
    public submitRequestRejection = false;
    public isAdmin = true;
    public viewAllJobsites: true;
    public totalUsers: number;
    public currentAlertEmail = '';
    public errorMsg = '';

    public requests = new BehaviorSubject<Request[]>([]);
    public bkRequests = [];
    public users = [];
    public bkUsers = [];

    public breadCrumbs: any[] = [
        { item: 'views.userManagement.homeRoute', link: '/dashboard' },
        { item: 'views.userManagement.userManagementConsole' },
    ];
    public isSuperUser = false;
    public emailSent = true;
    public showAlert = false;

    private subscription: Subscription;
    private filterByArray = [];
    public searchTerm: string;
    public templateId: string;

    constructor(
        public translationService: TranslationService,
        public sharedFunctions: SharedFunctions,
        private sessionService: SessionService,
        private router: Router,
        private userService: UserService,
        private userRequestService: RequestService,
        private errorService: ErrorService,
        private countlyService: CountlyService) {

        super();

        this.user = this.sessionService.userProfile;

        const _userApps = sessionStorage.getItem('user_applications');
        const userApplications = (_userApps) ? JSON.parse(_userApps) : [];
        const userMgtApp = userApplications.filter((apps) => apps.applicationCode === DCMConstants.DCM_USER_MANAGEMENT_APP_CODE)[0];

        if (userMgtApp) {
            this.isSuperUser = userMgtApp.roles.findIndex((r) => r.roleCode === DCMConstants.DCM_SUPER_USER_CODE) !== -1;
        }

        this.getUsers();
        if (this.isSuperUser) {
            this.getRequests();
        }
    }

    public ngOnInit(): void {
        if (this.countlyService.Countly && this.countlyService.Countly.q) {
            this.countlyService.addTrackingPage('User Management');
        }
        this.checkEmailStatus();

        this.successModal
            .afterClosed()
            .subscribe(() => {
                window.scrollTo(0, 0);
                if (!this.emailSent) {
                    this.showAlert = true;
                }
                this.getRequests();
            });

        this.errorModal
            .afterClosed()
            .subscribe(() => this.getRequests());
    }

    public ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    // Compontent Main Functions
    public editUser(userAccount): void {
        this.router.navigate(['user-management/edit-user'], { queryParams: { id: userAccount } });
    }

    public readRequest(): void {
        this.subscription = this.userRequestService.readRequest(this.selectedRequest.requestId).subscribe(
            (error) => { }
        );
    }

    public approveRequest(): void {
        this.requestDetail.close();
        this.subscription = this.userRequestService
            .authorizeRequest(this.selectedRequest.requestId)
            .subscribe((response) => {
                // TODO PAPL: Api should return request object even on success.
                if (response && !response['emailIsSent']) {
                    const requesterName = this.selectedRequest.requesterName;
                    this.errorMsg =
                        `${this.translationService.pt('views.userManagement.requestApprovedEmailError')}: ${requesterName}`;
                    this.emailSent = false;
                }
                this.requestSuccessMessage = this.translationService.pt('views.userManagement.requests.successAuthorizedRequest');
                this.successModal.open();
            },
                (error) => this.throwError(error, true));
    }

    public rejectRequest(): void {
        this.requestDetail.close();
        this.requestCancelConfirmDialog.close();
        this.submitRequestRejection = true;
        this.subscription = this.userRequestService
            .rejectRequest(this.selectedRequest.requestId, this.rejectComment)
            .subscribe((response) => {
                // TODO PAPL: Api should return request object even on success.
                if (response && !response['emailIsSent']) {
                    const requesterName = this.selectedRequest.requesterName;
                    this.errorMsg =
                        `${this.translationService.pt('views.userManagement.requestRejectedEmailError')}: ${requesterName}`;
                    this.emailSent = false;
                }
                this.requestSuccessMessage = this.translationService.pt('views.userManagement.requests.successRejectedRequest');
                this.rejectComment = '';
                this.successModal.open();
                this.submitRequestRejection = false;
            },
                (error) => {
                    this.submitRequestRejection = false;
                    this.throwError(error, true);
                }
            );
    }

    public confirmRequestRejection(): void {
        this.requestDetail.close();
        this.requestCancelConfirmDialog.open();
    }

    // Modal Functions
    public openRequestDetail(selectedRequest: Request): void {
        this.selectedRequest = selectedRequest;
        if (selectedRequest.status === 'N') {
            this.readRequest();
        }
        this.selectedRequest = selectedRequest;
        this.requestDetail.open();
    }

    public getRolesString(appId: number) {
        const payload: any = this.selectedRequest.details;
        const selectedRoles = payload.roleList;
        let rolesList = '';
        if (appId !== undefined) {
            rolesList = selectedRoles.filter((r) => r.applicationId === appId).map((r) => {
                return this.translationService.pt(r.roleCode);
            }).join(' - ');
        }
        return this.sharedFunctions.formatRolesString(rolesList);
    }

    public onDomChange(change: MutationRecord): void {
        const firstNode = change.removedNodes[0];
        if (firstNode && firstNode.nodeName === 'CMX-ALERT') {
            this.showAlert = false;
        }
    }

    private checkEmailStatus(): void {
        const emailStatus = this.errorService.emailStatus;
        if (emailStatus) {
            this.showAlert = !emailStatus.sent;
            this.errorMsg =
                `${this.translationService.pt('views.userManagement.createUserEmailError')}: ${emailStatus.email}`;
        } else {
            this.showAlert = false;
            this.errorMsg = '';
        }
        this.errorService.resetEmailStatus();
    }

    // Filtering Functions
    private filterUsers(): void {
        if (this.searchTerm !== '') {
            const arrayFilter = {
                customerDesc: this.searchTerm,
                fullName: this.searchTerm,
                userAccount: this.searchTerm
            };
            this.users = this.sharedFunctions.filterData(this.bkUsers, arrayFilter);
        } else {
            this.users = this.bkUsers;
        }
    }

    private filterUsersRequests(): void {
        if (this.searchTerm !== '') {
            const arrayFilter = {
                customerDesc: this.searchTerm,
                requesterName: this.searchTerm
            };
            this.requests.next(this.sharedFunctions.filterData(this.bkRequests, arrayFilter));
        } else {
            this.requests.next(this.bkRequests);
        }
    }

    public updateSearchTerm(value: string, toFilter: string): void {
        this.searchTerm = value;
        if (toFilter === 'users') {
            this.filterUsers();
        } else if (toFilter === 'requests') {
            this.filterUsersRequests();
        }
    }

    // Get Data
    private getUsers(): void {
        this.subscription = this.userService.getUsers().subscribe(
            (data) => {
                if (data.length > 0) {
                    this.bkUsers = data;
                    this.users = data;
                    this.totalUsers = data.length;
                }
            },
            (error) => this.throwError(error));
    }

    private getRequests(status?: RequestStatus): void {
        if (!status) {
            status = this.requestStatuses[0];
        }

        this.requestStatusSelected = status.name;
        this.requests.next([]);
        this.bkRequests = [];

        this.userRequestService.getAllRequests(status.code).subscribe(
            (data) => {
                if (data.length > 0) {
                    let requests;
                    requests = data.map((request) => {
                        return {
                            requestId: request.authorizationRequestId,
                            userId: request.userId,
                            requesterName: request.userFullName,
                            customerDesc: request.customerDesc,
                            requestDate: request.requestDate,
                            authorizedBy: request.authorizedBy,
                            authorizedByFullName: request.authorizedByFullName,
                            notes: request.notes,
                            status: request.status,
                            statusDesc: this.translationService.pt(this.sharedFunctions.getStatusName(request.status)),
                            customerId: request.customerId,
                            details: (request.payload) ? JSON.parse(request.payload) : null,
                            requestType: request.requestType,
                            requestTypeDesc: this.translationService.pt(this.sharedFunctions.getRequestTypeName(request.requestType))
                        };
                    });
                    this.requests.next(requests);
                    this.bkRequests = requests;
                    if (status.code === 'all') {
                        this.pendingRequestTotal = requests.length;
                    }
                } else {
                    this.requests.next([]);
                    this.bkRequests = [];
                }
            },
            (error) => this.throwError(error));
    }

    private throwError(error, useModal?: boolean): void {
        this.errorService.handleError(error).subscribe((apiError: ApiError) => {
            this.errorMsg = `${apiError.errorCode}: ${this.translationService.pt(apiError.errorDesc)}`;
            if (useModal) {
                this.errorModal.open();
            } else {
                Logger.error(this.errorMsg);
            }
        });
    }
}
