import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

// Providers
import { UserService } from '../../services';

// CMX Dependencies
import { CmxDialogModule } from '@cemex/cmx-dialog-v4';
import { CmxButtonModule } from '@cemex/cmx-button-v4';

// Internal Dependencies
import { TablesModule } from '../../components/tables/table.module';
import { ButtonsFooterModule } from '../../components/buttons-footer/buttons-footer.module';
import { BreadcrumbsModule } from '../../components/breadcrumbs/breadcrumbs.module';
import { PipesModule } from '../../pipes/pipes.module';

import { ActionLogComponent } from './action-log.component';

export const ROUTES: Routes = [
    { path: '', component: ActionLogComponent }
];

@NgModule({
    declarations: [ActionLogComponent],
    imports: [
        CommonModule,
        BreadcrumbsModule,
        TablesModule,
        ButtonsFooterModule,
        CmxButtonModule,
        CmxDialogModule,
        PipesModule,
        RouterModule.forChild(ROUTES)],
    exports: [ActionLogComponent]
})

export class ActionLogModule { }
