import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PipesModule } from '../../pipes/pipes.module';
import { PrivacyComponent } from './privacy.component';

export const ROUTES: Routes = [
    { path: '', component: PrivacyComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES),
        CommonModule,
        PipesModule
    ],
    declarations: [
        PrivacyComponent
    ],
    exports: [
        PrivacyComponent
    ]
})
export class PrivacyModule { }
