import { Routes, RouterModule } from '@angular/router';

// Guards
import { AuthGuard } from '../guards/auth.guard';
import { AdminAuthGuard } from '../guards/admin-auth.guard';
import { SeveralErrorComponent } from './several-error/several-error.component';

import { AuthPagesComponent } from './auth-pages.component';
import { TermsConditionsGuard } from '../guards/terms-conditions.guard';
import { NgModule } from '@angular/core';

const pagesRoutes: Routes = [
    {
        path: '',
        component: AuthPagesComponent,
        canActivate: [AuthGuard],
        canActivateChild: [TermsConditionsGuard],
        children: [
            { path: 'several-error', component: SeveralErrorComponent },
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'profile',
                loadChildren: './profile/profile.module#ProfileModule'
            },
            {
                path: 'settings',
                loadChildren: './settings/settings.module#SettingsModule'
            },
            {
                path: 'user-management',
                loadChildren: './user-management/user-management.module#UserManagementModule',
                canActivate: [AuthGuard, AdminAuthGuard],
            },
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' }
        ]
    },
    {
        path: '',
        component: AuthPagesComponent,
        children: [
            {
                path: 'public/legal',
                loadChildren: './terms/terms.module#TermsModule'
            },
            {
                path: 'public/privacy',
                loadChildren: './privacy/privacy.module#PrivacyModule'
            },
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(pagesRoutes)
    ],
    exports: [RouterModule]
})

export class AuthRoutingModule { }
