import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

// CMX Dependencies
import { CmxButtonModule } from '@cemex/cmx-button-v4';
import { CmxDialogModule } from '@cemex/cmx-dialog-v4';

// Internal Dependencies
import { TablesModule } from './../../components/tables/table.module';
import { ButtonsFooterModule } from './../../components/buttons-footer/buttons-footer.module';
import { SearchModule } from './../../components/search/search.module';
import { NoInfoModule } from './../../components/no-info/no-info.module';
import { BreadcrumbsModule } from '../../components/breadcrumbs/breadcrumbs.module';

// Providers
import { CanDeactivateGuard } from './../../guards/can-deactivate.guard';

import { DataRequestComponent } from './data-request.component';

export const ROUTES: Routes = [
    { path: '', component: DataRequestComponent, canDeactivate: [CanDeactivateGuard] }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES),
        CommonModule,
        FlexLayoutModule,
        SearchModule,
        ButtonsFooterModule,
        TablesModule,
        NoInfoModule,
        BreadcrumbsModule,
        CmxButtonModule,
        CmxDialogModule
    ],
    exports: [
        DataRequestComponent
    ],
    declarations: [
        DataRequestComponent
    ],
    providers: [ CanDeactivateGuard ]
})
export class DataRequestModule { }
