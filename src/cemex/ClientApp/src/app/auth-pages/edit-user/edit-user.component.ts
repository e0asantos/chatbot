import { Component, OnInit, OnDestroy, ComponentFactoryResolver, ViewContainerRef, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// Providers
import { TranslationService } from '@cemex-core/angular-localization-v1/dist';
import { SessionService, Logger } from '@cemex-core/angular-services-v2/dist';
import { UserService, JobsiteService, CustomerService, ApplicationService, ErrorService } from '../../services';
import { SharedFunctions } from '../../helpers/functions';

// Components
import { CmxDialogComponent } from '@cemex/cmx-dialog-v4';
import { LocalizationComponent } from './../../components/localization/localization.component';
import { AddPermissionComponent } from '../../components/add-permission/add-permission.component';
import { EditPermissionComponent } from '../../components/edit-permission/edit-permission.component';

// Modal
import { JobsitesModalComponent } from '../../components/modals/jobsites/jobsites.modal';
import { LegalEntitiesModalComponent } from '../../components/modals/legal-entities/legal-entities.modal';

// Models
import { Application, Jobsite, CustomerDetail, UserProfile, Role, ApiError } from '../../models';
class AppViewModel {
    public applicationId: number;
    public applicationName: string;
    public applicationCode: string;
    public rolesViewModel: RolesViewModel[];
    public assigned: boolean;
    public canEdit: boolean;
}

class RolesViewModel {
    public roleId: number;
    public roleName: string;
    public roleCode: string;
    public assigned: boolean;
}

@Component({
    selector: 'app-edit-user',
    entryComponents: [EditPermissionComponent, AddPermissionComponent],
    templateUrl: 'edit-user.component.html',
    styleUrls: ['edit-user.component.scss']
})
export class EditUserComponent extends LocalizationComponent implements OnDestroy, OnInit {

    @ViewChild('permissionsContainer', { read: ViewContainerRef })
    public permissionsContainer: ViewContainerRef;

    @ViewChild('successModal')
    public successModal: CmxDialogComponent;

    @ViewChild('warningUserDisableModal')
    public warningUserDisableModal: CmxDialogComponent;

    @ViewChild('pendingChangesWarningModal')
    public pendingChangesWarningModal: CmxDialogComponent;

    @ViewChild('errorModal')
    public errorModal: CmxDialogComponent;

    @ViewChild('scrollableDiv') public scrollableDiv: ElementRef;

    @ViewChild('warningDelete')
    public warningDelete: CmxDialogComponent;

    @ViewChild('successLocEditUser')
    public successLocEditUser: CmxDialogComponent;

    @ViewChild('jobsiteModal')
    public jobsiteModal: JobsitesModalComponent;

    @ViewChild('legalEntitiesModal')
    public legalEntitiesModal: LegalEntitiesModalComponent;

    public loggedUserId;
    public customerId;
    public user: UserProfile;
    public profilePicPath = '/dist/assets/images/no-profile.jpg';
    public userAccount: string;

    public userApplications = new BehaviorSubject<Application[]>([]);
    public allApplications = new BehaviorSubject<Application[]>([]);
    public adminApplications = new BehaviorSubject<Application[]>([]);

    public counter = 0;
    public visibleAppsForAdmin = 0;
    public tabSelected = 'apps';
    public userCustomers: CustomerDetail[] = [];
    public customers: CustomerDetail[] = [];
    public backupUserCustomers: CustomerDetail[] = [];
    public selectedCustomersDelet: CustomerDetail[] = [];
    public userJobsites: Jobsite[] = [];
    public backupUserJobsites: Jobsite[] = [];
    public jobsites: Jobsite[] = [];
    public newJobsites: Jobsite[] = [];
    public deleteJobsites: Jobsite[] = [];
    public showCustomers = true;
    public modelChanged = false;
    public showJobsites = false;
    public hasChanged = false;
    public successMessage = '';
    public permissonIsCompleted = true;
    public logEntries = [];
    public actionLogOpen = false;
    public renderActionLog = false;
    public breadCrumbs: any[] = [];
    public selectedJobsiteDelet: Jobsite[] = [];
    public selectallCheckedJobsites = false;
    public isDigitalAdmin = false;
    public destinationRoute: string;
    public destinationParams: any;
    public templateMsg = '';
    public isMoreLocation = false;
    public errorMsg = '';

    private subscription: Subscription;
    private permissionViewModel = [];
    private permissionsRemoved = [];
    private userStatusChanged = false;
    private searchTerm: string;
    private backSelectallCheckedJobsites = false;
    private reload = false;
    private isDeleteCustomer = false;

    constructor(
        public translationService: TranslationService,
        private router: Router,
        private route: ActivatedRoute,
        private userService: UserService,
        private applicationService: ApplicationService,
        private componentFactoryResolver: ComponentFactoryResolver,
        private viewContainerRef: ViewContainerRef,
        private customerService: CustomerService,
        private jobsitesService: JobsiteService,
        private sessionService: SessionService,
        private sharedFunctions: SharedFunctions,
        private errorService: ErrorService) {
        super();
        this.route.queryParams.subscribe((params) => {
            this.userAccount = params['id'];
            if (!this.userAccount) {
                this.router.navigate(['user-management']);
            }
        });
        this.loggedUserId = this.sessionService.userProfile.userId;
    }

    public ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    public ngOnInit(): void {
        this.breadCrumbs = [
            { item: 'views.userManagement.userManagement', link: '/user-management' },
            { item: 'views.userManagement.edit.userProfile' },
        ];

        this.getUserInformation();

        this.successModal
            .afterClosed()
            .subscribe(() => {
                if (this.reload) {
                    window.location.reload();
                } else if (this.userStatusChanged) {
                    this.router.navigate(['user-management']);
                }
            });

        this.successLocEditUser.afterClosed().subscribe((result: boolean) => {
            this.selectedJobsiteDelet = [];
            this.selectedCustomersDelet = [];
            this.userCustomers = [];
            this.userJobsites = [];
            this.backupUserCustomers = [];
            this.backupUserJobsites = [];

            this.userService
                .getUserDetails(this.userAccount)
                .subscribe((data) => {
                    if (data.profile.userSelectedId) {
                        this.selectallCheckedJobsites = data.profile.userSelectedId.trim() === 'A' ? true : false;
                        this.backSelectallCheckedJobsites = data.profile.userSelectedId.trim() === 'A' ? true : false;
                    }
                },
                    (error) => {
                        this.router.navigate(['user-management']);
                    }
                );
            this.getCustomersAndJobsitesForUser();
        });
    }

    public openActionLog(): void {
        this.router.navigate(['user-management/edit-user/action-log'], {
            queryParams: { id: this.user.userAccount, data: this.user.userId }
        });
    }

    public submitPermissions(): void {
        const applicationList = [];
        const roleList = [];

        this.permissionViewModel.map((permission) => {
            if (permission.assigned) {
                applicationList.push({
                    applicationId: permission.applicationId,
                    applicationCode: permission.applicationCode
                });

                permission.rolesViewModel.map((role) => {
                    if (role.assigned) {
                        roleList.push({
                            roleId: role.roleId,
                            roleCode: role.roleCode
                        });
                    }
                });
            }
        });

        const userDataPatch = {
            applicationList,
            roleList,
            overwriteListData: true
        };

        this.userService
            .updateUser(this.user.userId, userDataPatch)
            .subscribe((result) => {
                this.reload = true;
                this.successMessage = this.translationService.pt('views.userManagement.edit.userPermissionsUpdate');
                this.successModal.open();
                this.hasChanged = false;
            },
                (error) => {
                    this.errorMsg = this.translationService.pt('views.userManagement.edit.userPermissionsUpdateError');
                    this.errorModal.open();
                }
            );
        window.scrollTo(0, 0);
    }

    public get canAssignNewPermission(): boolean {
        const unassignedApps = this.permissionViewModel.filter((p) => !p.assigned && p.canEdit);
        return unassignedApps.length > 0 && this.permissonIsCompleted;
    }

    public openConfirmUserStatusUpdate(): void {
        this.warningUserDisableModal.open();
    }

    public updateUserStatus(): void {
        const _body = {
            userStatus: 'I'
        };

        this.userService
            .updateUser(this.user.userId, _body)
            .subscribe((response: Response) => {
                this.warningUserDisableModal.close();
                this.successMessage = this.translationService.pt('views.userManagement.edit.userDisabledSuccess');
                this.reload = false;
                this.successModal.open();
                this.userStatusChanged = true;
            },
                (error) => this.throwError(error));
    }

    public updateSearchTerm(value: string): void {
        this.searchTerm = value;
        if (this.tabSelected === 'locations') {
            this.filterJobsites();
        } else if (this.tabSelected === 'customers') {
            this.filterCustomers();
        }
    }

    private getUserInformation(): void {
        this.userService
            .getUserDetails(this.userAccount)
            .subscribe((data) => {
                if (!data) {
                    this.router.navigate(['user-management']);
                }
                const aux = data as UserProfile;
                this.user = {
                    userId: aux.userId,
                    userAccount: aux.userAccount,
                    userPosition: aux.profile.userPosition,
                    customerDesc: aux.customer.customerDesc,
                    customerCode: aux.customer.customerCode,
                    countryCode: aux.countryCode,
                    phoneNumber: aux.phone,
                    firstName: aux.profile.firstName,
                    lastName: aux.profile.lastName,
                    customerId: aux.customerId,
                    userType: aux.userType,
                    status: aux.status,
                    isDigitalAdmin: data.profile.isDigitalAdmin || false
                };
                this.customerId = data.profile.customerId;
                if (data.profile.userSelectedId) {
                    this.selectallCheckedJobsites = data.profile.userSelectedId.trim() === 'A' ? true : false;
                    this.backSelectallCheckedJobsites = data.profile.userSelectedId.trim() === 'A' ? true : false;
                }

                this.isDigitalAdmin = data.profile.isDigitalAdmin || false;
                this.getCustomersAndJobsitesForUser();
                this.setPermissionsViewModel();
            },
                (error) => {
                    this.errorService
                        .handleError(error)
                        .subscribe((apiError: ApiError) => {
                            this.errorMsg = `${apiError.errorCode}: ${this.translationService.pt(apiError.errorDesc)}`;
                            Logger.error(this.errorMsg);
                        });
                }
            );
    }

    private setPermissionsViewModel(): void {
        // Get User Permissions
        this.applicationService
            .getApplicationsAndRolesForUser(this.user.userId)
            .subscribe((data) => {
                if (data) {
                    const appsWithRoles = data.filter((app) => app.roles.length > 0);
                    this.userApplications.next(appsWithRoles);
                    this.getAllPermissions();
                }
            },
            (error) => this.throwError(error));
    }

    private getAdminPermissions(): void {
        this.applicationService
            .getUserApplicationsAndRoles()
            .subscribe((data) => {
                if (data) {
                    const appsWithRoles = data.filter((app) => app.roles.length > 0);
                    this.adminApplications.next(appsWithRoles);
                    this.buildPermisionViewModel();
                }
            },
                (error) => this.throwError(error));
    }

    // Get Permissions Catalog
    private getAllPermissions(): void {
        this.applicationService
            .getAllApplicationsAndRoles()
            .subscribe((data) => {
                if (data) {
                    const appsWithRoles = data.filter((app) => app.roles.length > 0);
                    this.allApplications.next(appsWithRoles);
                    this.getAdminPermissions();
                }
            },
            (error) => this.throwError(error));
    }

    // Build View Model based on All Permissions, User Permissions and Admin Permissions.
    private buildPermisionViewModel(): void {
        this.permissionViewModel = [];
        const _allApplications = this.allApplications.getValue();

        this.permissionViewModel = _allApplications.map((app) => {
            return {
                applicationName: app.applicationName,
                applicationId: app.applicationId,
                applicationCode: app.applicationCode,
                assigned: this.userHasApplication(app.applicationId),
                canEdit: this.adminHasApplication(app.applicationId),
                rolesViewModel: (app.roles && app.roles.length > 0) ? app.roles.map((role) => {
                    return {
                        roleId: role.roleId,
                        roleName: role.roleName,
                        roleCode: role.roleCode,
                        assigned: this.userHasRole(app.applicationId, role.roleId)
                    } as RolesViewModel;
                }) : []
            } as AppViewModel;
        });

        this.permissionViewModel.map((permissionVm) => {
            if (permissionVm.assigned && permissionVm.canEdit) {
                this.editPermissionRow(permissionVm);
            }
        });

        this.visibleAppsForAdmin = this.permissionViewModel.filter((permission) => permission.assigned && permission.canEdit).length;

        window.scrollTo(0, 0);
    }

    private userHasApplication(applicationId: number): boolean {
        const _userApplications: Application[] = this.userApplications.getValue();
        return _userApplications.findIndex((userApp) => userApp.applicationId === applicationId) > -1
            || this.permissionViewModel.findIndex((permission) => permission.applicationId === applicationId) > -1;
    }

    private adminHasApplication(applicationId: number): boolean {
        const _adminApplications: Application[] = this.adminApplications.getValue();
        return _adminApplications.findIndex(adminApp => adminApp.applicationId === applicationId) > -1;
    }

    private userHasRole(permissionId: number, roleId: number): boolean {
        const _userPermissions: Application[] = this.userApplications.getValue();

        const permissionSelected: Application =
            _userPermissions.find((userPermission => userPermission.applicationId === permissionId));

        return (permissionSelected) ?
            permissionSelected.roles.findIndex((permissionRole) => permissionRole.roleId === roleId) > -1 : false;
    }

    private editPermissionRow(permission) {
        this.scrollableDiv.nativeElement.scrollIntoView(true);
        const factory = this.componentFactoryResolver.resolveComponentFactory(EditPermissionComponent);
        const ref = this.viewContainerRef.createComponent(factory);
        this.permissionsContainer.insert(ref.hostView);

        this.counter += 1;

        permission.rolesViewModel = permission.rolesViewModel;

        (ref.instance as EditPermissionComponent).isRTL = this.isRTL;
        (ref.instance as EditPermissionComponent).permission = permission;
        (ref.instance as EditPermissionComponent).counter = this.counter;
        (ref.instance as EditPermissionComponent).templateId = 'EDIT-USER';
        (ref.instance as EditPermissionComponent).isDigitalAdmin = this.isDigitalAdmin;
        ref.instance.onRemovePermission.subscribe((app) => {
            this.hasChanged = true;
            if (app) {
                this.permissionViewModel.map((_permission) => {
                    if (_permission.applicationId === app.applicationId) {
                        _permission.assigned = false;
                        _permission.rolesViewModel.map((_role) => {
                            _role.assigned = false;
                        });
                    }
                });
                this.permissionsRemoved.push(app);
                this.visibleAppsForAdmin =
                    this.permissionViewModel.filter((_app) => _app.assigned && _app.canEdit).length;
                this.hasChanged = this.AllPermissionsHaveRolAssigned();
            }
            this.counter -= 1;
            ref.destroy();

        });
        ref.instance.onRoleToggled.subscribe((app) => {
            this.hasChanged = true;
            this.permissionViewModel.map((_permission) => {
                if (_permission.applicationId === app.appId) {
                    _permission.rolesViewModel.map((_role) => {
                        if (_role.roleId === app.role.roleId) {
                            _role.assigned = app.added;
                        }
                    });
                }
            });

            this.hasChanged = this.AllPermissionsHaveRolAssigned();
        });
        ref.changeDetectorRef.detectChanges();
    }

    private addPermissionRow() {
        this.permissonIsCompleted = false;
        this.hasChanged = false;
        this.scrollableDiv.nativeElement.scrollIntoView(true);
        const factory = this.componentFactoryResolver.resolveComponentFactory(AddPermissionComponent);
        const ref = this.viewContainerRef.createComponent(factory);
        this.permissionsContainer.insert(ref.hostView);
        this.counter += 1;
        const unassignedApplications = this.permissionViewModel.filter((app) => !app.assigned && app.canEdit);

        (ref.instance as AddPermissionComponent).userApplications = unassignedApplications;
        (ref.instance as AddPermissionComponent).counter = this.counter;
        (ref.instance as AddPermissionComponent).isRTL = this.isRTL;
        (ref.instance as AddPermissionComponent).templateId = 'EDIT-USER';
        ref.instance.onApplicationSelected.subscribe((app) => {
            this.permissonIsCompleted = true;
            if (app.added) {
                this.permissionViewModel.map((_permission) => {
                    if (_permission.applicationId === app.applicationToAdd.applicationId) {
                        _permission.assigned = true;
                    }
                });
            } else {
                this.permissionViewModel.map((_permission) => {
                    if (_permission.applicationId === app.applicationToAdd.applicationId) {
                        _permission.assigned = true;
                    }
                    if (_permission.applicationId === app.applicationToRemove.applicationId) {
                        _permission.assigned = false;
                    }
                });
            }
        });
        ref.instance.onRoleToggled.subscribe((app) => {
            this.hasChanged = true;

            this.permissionViewModel.map((_permission) => {
                if (_permission.applicationId === app.appId) {
                    _permission.rolesViewModel.map((role) => {
                        if (role.roleId === app.role.roleId) {
                            role.assigned = app.added;
                        }
                    });
                }
            });

            this.hasChanged = this.AllPermissionsHaveRolAssigned();
            this.permissonIsCompleted = true;
        });
        ref.instance.onRemovePermission.subscribe((data) => {
            if (data) {
                this.permissionViewModel.map((p) => {
                    if (p.applicationId === data.applicationId) {
                        p.assigned = false;
                        p.rolesViewModel.forEach((role) => {
                            role.assigned = false;
                        });
                    }
                });
                this.visibleAppsForAdmin =
                    this.permissionViewModel.filter((app) => app.assigned && app.canEdit).length;
                this.permissionsRemoved.push(data);
                this.hasChanged = this.AllPermissionsHaveRolAssigned();
            }
            this.counter -= 1;
            this.permissonIsCompleted = true;
            ref.destroy();
        });
        ref.changeDetectorRef.detectChanges();
    }

    // Filtering Functions
    private filterJobsites(): void {
        if (this.searchTerm !== '') {
            const arrayFilter = {
                customerDesc: this.searchTerm,
                customerCode: this.searchTerm,
                jobsiteDesc: this.searchTerm,
                jobsiteCode: this.searchTerm
            };
            this.userJobsites = this.sharedFunctions.filterData(this.backupUserJobsites, arrayFilter);
        } else {
            this.userJobsites = this.backupUserJobsites;
        }
    }

    private filterCustomers(): void {
        if (this.searchTerm !== '') {
            const arrayFilter = {
                customerDesc: this.searchTerm,
                customerCode: this.searchTerm
            };
            this.userCustomers = this.sharedFunctions.filterData(this.backupUserCustomers, arrayFilter);
        } else {
            this.userCustomers = this.backupUserCustomers;
        }
    }

    private getCustomersAndJobsitesForUser(): void {
        this.subscription = this.customerService
            .getCustomersAndJobsitesForUser(this.user.userId)
            .subscribe((data) => {
                this.getCustomersCat(data);
            },
                (error) => this.throwError(error));
    }

    private getCustomersCat(dataCustomer): void {
        this.subscription = this.customerService
            .getCustomersAndJobsitesForUser(this.loggedUserId)
            .subscribe((data) => {
                // customer user login
                this.customers = data;
                // customer user search
                const custom = [];
                data.forEach((item, index) => {
                    // verify if the user's customer are the same as the logged user
                    if (dataCustomer.some((customer) => customer.customerId === item.customerId)) {
                        custom.push(item);
                    }
                });
                // verify if the user's main customer are in le list logged user
                let customerMain = custom.filter((c) => c.customerId === this.customerId)[0];
                if (!customerMain) {
                    customerMain = dataCustomer.filter((c) => c.customerId === this.customerId)[0];
                    if (customerMain) {
                        custom.unshift(customerMain);
                    }
                    this.userCustomers = custom;
                    this.backupUserCustomers = custom;
                } else {
                    const customers = custom.filter((c) => c.customerId !== this.customerId);
                    if (customerMain) {
                        customers.unshift(customerMain);
                    }
                    this.userCustomers = customers;
                    this.backupUserCustomers = customers;
                }
                this.jobsites = this.getJobsitesForUser(custom);
                const jobsites = [];
                const userJobsites = this.getJobsitesForUser(dataCustomer);
                this.jobsites.forEach((item, index) => {
                    if (userJobsites.some((jobiste) => jobiste.jobsiteId === item.jobsiteId)) {
                        jobsites.push(item);
                    }
                });
                this.userJobsites = jobsites;
                this.backupUserJobsites = jobsites;
            },
                (error) => this.throwError(error));
    }

    private getJobsitesForUser(data) {
        const jobsite: Jobsite[] = [];
        data.forEach((c) => {
            c.jobSites.forEach((j) => {
                jobsite.push(j);
            });
        });
        return jobsite;
    }

    private AllPermissionsHaveRolAssigned(): boolean {
        for (const app of this.permissionViewModel.filter((p) => p.assigned && p.applicationCode !== 'UserProvisioning_App')) {
            if (app.rolesViewModel.findIndex((r) => r.assigned === true) === -1) {
                return false;
            }
        }
        return true;
    }

    private updateSelectedCustomers(event): void {
        // Select customers will deleted
        if (event.isDelete) {
            this.selectedCustomersDelet = event.customers;
            this.selectedCustomersDelet = this.selectedCustomersDelet.filter((r) => r.customerId !== this.customerId);
            this.isDeleteCustomer = true;
            this.deleteMultipleLegalEntitiesLocations();
        } else {
            event = event.filter((r) => r.customerId !== this.customerId);
            this.selectedCustomersDelet = event || [];
        }
    }

    public updateSelectedJobsites(event): void {
        if (event.isDelete) {
            this.selectedJobsiteDelet = event.jobsites;
            this.isDeleteCustomer = false;
            this.deleteMultipleLegalEntitiesLocations();
        } else {
            this.selectedJobsiteDelet = event || [];
        }
    }

    /**
     * LEGAL ENTITIES OPTIONS
     */
    public openWarningDialog(isDeleteCustomer) {
        this.isDeleteCustomer = isDeleteCustomer;
        this.templateMsg = isDeleteCustomer ? this.translationService.pt('views.userManagement.unassigned.legalEntity') :
            this.translationService.pt('views.userManagement.unassigned.locations');
        this.warningDelete.open();
    }

    private openLegalEntitesDialog() {
        const customers = this.customers.filter((c) => c.customerId !== this.customerId);
        const customer = this.customers.filter((c) => c.customerId === this.customerId)[0];
        if (customer) {
            customers.unshift(customer);
        }
        this.legalEntitiesModal.customers = customers;
        this.legalEntitiesModal.filteredCustomers = customers;
        this.legalEntitiesModal.selectedCustomers = JSON.parse(JSON.stringify(this.userCustomers)); // this.userCustomers;
        this.legalEntitiesModal.defaulCustomer = this.customerId;
        this.legalEntitiesModal.open();
    }

    public assingLegalEntities(event) {
        const customers = event.filter((c) => c.customerId !== this.customerId);
        const customer = event.filter((c) => c.customerId === this.customerId)[0];
        if (customer) {
            customers.unshift(customer);
        }
        this.userCustomers = customers || [];
        this.compareLegalEntties(1, this.userCustomers);
    }

    private compareLegalEntties(option, userCustomers) {
        /** OPTION 1 = ADD
         *  OPTION 2 = DELTE
         */
        let obj = {
            customersIds: [],
            oldCustomersIds: []
        };
        const newCustomers = [];
        const deleteCustomers = [];
        if (option === 1) {
            userCustomers.forEach((element) => {
                const found = this.backupUserCustomers.filter((ir) => ir.customerId === element.customerId);
                if (found.length !== 1) {
                    newCustomers.push(element);
                }
            });
            this.backupUserCustomers.forEach((element) => {
                const found = userCustomers.filter((ir) => ir.customerId === element.customerId);
                if (found.length === 0) {
                    deleteCustomers.push(element);
                }
            });

            obj = {
                customersIds: newCustomers.map(function getIdsCustom(r) { return r.customerId; }),
                oldCustomersIds: deleteCustomers.map(function getIdsCustom(r) { return r.customerId; })
            };
        } else {
            this.backupUserCustomers.forEach((element) => {
                const found = userCustomers.filter((ir) => ir.customerId === element.customerId);
                if (found.length === 1) {
                    deleteCustomers.push(element);
                }
            });

            obj = {
                customersIds: [],
                oldCustomersIds: deleteCustomers.map(function getIdsCustom(r) { return r.customerId; })
            };
        }

        this.jobsitesService.updateCustomers(this.user.userId, obj).subscribe(
            (data) => {
                this.templateMsg = this.translationService.pt('views.edituser.user_updated');
                this.successLocEditUser.open();
            },
            (error) => this.throwError(error, true));
    }

    /**
     * LOCATIONS OPTIONS
     */

    private openJobsiteDialog() {
        if (this.selectallCheckedJobsites) {
            this.isMoreLocation = true;
            this.templateMsg = this.translationService.pt('views.userManagement.no.more.locations');
            this.warningDelete.open();
        } else {
            this.isMoreLocation = false;
            const inputModal = {
                jobsites: this.jobsites,
                selectedJobsites: this.userJobsites,
                filteredJobsites: this.jobsites,
                selectAll: this.selectallCheckedJobsites
            };

            this.jobsiteModal.init(inputModal);
        }
    }

    public assingjobsite(event) {
        this.selectallCheckedJobsites = event.checkAllJobsites;
        this.selectedJobsiteDelet = [];

        this.compareJobsites(1, event.jobsites);
    }

    private compareJobsites(option, userJobsites, updateCustomers?: boolean) {
        /** OPTION 1 = ADD
         *  OPTION 2 = DELTE
         */
        let obj = {
            jobSitesIds: [],
            oldJobSitesIds: []
        };
        this.newJobsites = [];
        this.deleteJobsites = [];
        if (option === 1) {
            // If is has option select All -- the change no apply
            userJobsites.forEach((element) => {
                const found = this.backupUserJobsites.filter((ir) => ir.jobsiteId === element.jobsiteId);
                if (found.length !== 1) {
                    this.newJobsites.push(element);
                }
            });
            this.backupUserJobsites.forEach((element) => {
                const found = userJobsites.filter((ir) => ir.jobsiteId === element.jobsiteId);
                if (found.length === 0) {
                    this.deleteJobsites.push(element);
                }
            });

            obj = {
                jobSitesIds: this.newJobsites.map(function getIdsCustom(r) { return r.jobsiteId; }),
                oldJobSitesIds: this.deleteJobsites.map(function getIdsCustom(r) { return r.jobsiteId; })
            };

            if (this.backSelectallCheckedJobsites && this.deleteJobsites.length > 0) {
                obj = {
                    jobSitesIds: [],
                    oldJobSitesIds: [this.deleteJobsites.map(function getIdsCustom(r) { return r.jobsiteId; })]
                };
            }
        } else {
            this.backupUserJobsites.forEach((element) => {
                const found = userJobsites.filter((ir) => ir.jobsiteId === element.jobsiteId);
                if (found.length === 0) {
                    this.deleteJobsites.push(element);
                }
            });

            obj = {
                jobSitesIds: [],
                oldJobSitesIds: this.deleteJobsites.map(function getIdsCustom(r) { return r.jobsiteId; })
            };
        }

        if (this.selectallCheckedJobsites !== this.backSelectallCheckedJobsites) {
            const _body = {
                userSelectedId: this.selectallCheckedJobsites ? 'A' : 'S',
            };

            this.userService.updateUser(this.user.userId, _body).subscribe(
                (response: Response) => {
                    this.jobsitesService.updateJobsite(this.user.userId, obj).subscribe(
                        (data) => {
                            this.templateMsg = this.translationService.pt('views.edituser.user_updated');
                            this.successLocEditUser.open();
                        },
                        (error) => this.throwError(error, true));
                },
                (error) => this.throwError(error, true));
        } else {
            if (updateCustomers && !this.backSelectallCheckedJobsites && this.newJobsites.length > 0) {
                obj = {
                    jobSitesIds: [],
                    oldJobSitesIds: []
                };
            }
            this.jobsitesService.updateJobsite(this.user.userId, obj).subscribe(
                (data) => {
                    this.templateMsg = this.translationService.pt('views.edituser.user_updated');
                    this.successLocEditUser.open();
                },
                (error) => this.throwError(error, true));
        }
    }

    private throwError(error, useModal?: boolean) {
        this.errorService
            .handleError(error)
            .subscribe((apiError: ApiError) => {
                this.errorMsg = `${apiError.errorCode}: ${this.translationService.pt(apiError.errorDesc)}`;
                if (useModal) {
                    this.errorModal.open();
                } else {
                    Logger.error(this.errorMsg);
                }
            });
    }

    private deleteMultipleLegalEntitiesLocations() {
        this.warningDelete.close();
        if (this.isDeleteCustomer) {
            this.selectedCustomersDelet.forEach((element) => {
                this.userCustomers = this.userCustomers.filter((r) => r.customerId !== element.customerId);
            });
            this.compareLegalEntties(2, this.selectedCustomersDelet);
        } else {
            let userJobsites = this.userJobsites;
            this.selectedJobsiteDelet.forEach((element) => {
                userJobsites = userJobsites.filter((r) => r.jobsiteId !== element.jobsiteId);
            });
            this.selectedJobsiteDelet = [];
            this.selectallCheckedJobsites = false;
            this.compareJobsites(2, userJobsites);
        }

    }

    private goToHome() {
        this.router.navigate(['/user-management']);
    }

    canDeactivate() {
        if (this.hasChanged) {
            this.pendingChangesWarningModal.open();
            return false;
        }
        return true;
    }

    leaveRoute() {
        this.hasChanged = false;
        this.counter = 0;
        this.pendingChangesWarningModal.close();
        this.router.navigate([this.destinationRoute], { queryParams: this.destinationParams });
    }

}
