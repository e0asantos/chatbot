import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

// CMX Dependencies
import { CmxDialogModule } from '@cemex/cmx-dialog-v4';
import { CmxButtonModule } from '@cemex/cmx-button-v4';

// Internal Dependencies
import { ButtonsFooterModule } from './../../components/buttons-footer/buttons-footer.module';
import { AddPermissionModule } from '../../components/add-permission/add-permission.module';
import { BreadcrumbsModule } from '../../components/breadcrumbs/breadcrumbs.module';

// Providers
import { CanDeactivateGuard } from '../../guards/can-deactivate.guard';

// Components
import { PermissionRequestComponent } from './permission-request.component';

export const ROUTES: Routes = [
    { path: '', component: PermissionRequestComponent, canDeactivate: [CanDeactivateGuard] }
];

@NgModule({
    imports: [
        RouterModule.forChild(ROUTES),
        AddPermissionModule,
        CommonModule,
        ButtonsFooterModule,
        CmxButtonModule,
        CmxDialogModule,
        BreadcrumbsModule
    ],
    declarations: [
        PermissionRequestComponent,
    ],
    exports: [
        PermissionRequestComponent,
    ],
    providers: [
        CanDeactivateGuard
    ],
})

export class PermissionRequestModule { }
