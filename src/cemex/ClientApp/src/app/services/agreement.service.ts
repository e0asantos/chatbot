import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';

// Providerd
import { HttpCemex, ProjectSettings } from '@cemex-core/angular-services-v2/dist';

// Models
import { Agreement } from '../models/agreement';

@Injectable()
export class AgreementService {

    public touSigned;
    private options = new RequestOptions();

    constructor(private http: HttpCemex, private projectEnv: ProjectSettings) {
        this.options.headers = new Headers();
        this.options.headers.append('Content-Type', 'application/json');
    }

    public updateTermsOfUse(isSigned: boolean): void {
        this.touSigned = isSigned;
    }

    public get isTermsOfUseSigned(): boolean {
        const touSigned = this.touSigned || (/true/i).test(sessionStorage.getItem('touSigned')) || false;
        return touSigned;
    }

    /*
    Agreement Categories:
        Terms of Use Agreement - TOU
        Legal - LGL
        Privacy Policy - PRP
    */

    public getAgreement(countryCode: string, agreementCat: string): Observable<Agreement[]> {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath()
            + 'v5/legal/agreements?regionId=&countryCode=' + countryCode + '&category=' + agreementCat)
            .map((res: Response) => res.json().agreements)
            .catch(this.handleError);
    }

    public getAgreements(countryCode: string): Observable<Agreement[]> {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath()
            + 'v5/legal/agreements?regionId=&countryCode=' + countryCode + '&category=')
            .map((res: Response) => res.json().agreements)
            .catch(this.handleError);
    }

    public getAgreementContent(documentHash: string): Observable<any> {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath() + 'v5/legal/agreements/content/' + documentHash)
            .map((res: Response) => res.text())
            .catch(this.handleError);
    }

    public getPublicAgreementContentByCategory(countryCode: string, categoryCode: string): Observable<any> {
        const url = `/v5/legal/agreements/content?regionId=&countryCode=${countryCode}&category=${categoryCode}`;
        return this.http.get(this.projectEnv.getBaseOrgEnvPath() + url)
            .map((res: Response) => res.text())
            .catch(this.handleError);
    }

    public signAgreement(agreementCode: string, agreementVersionId: number): Observable<any> {
        const agreementSigned = {
            agreementVersionId,
            hash: agreementCode,
        };
        return this.http.post(this.projectEnv.getBaseOrgEnvPath() + 'v5/legal/agreements/sign', agreementSigned)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Observable.throw(error || 'Server error');
    }
}
