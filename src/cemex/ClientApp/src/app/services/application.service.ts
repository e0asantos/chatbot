import { Injectable } from '@angular/core';
import { RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

// Providers
import { HttpCemex, ProjectSettings } from '@cemex-core/angular-services-v2/dist';

// Models
import { Application } from './../models/application';

@Injectable()
export class ApplicationService {

    private options = new RequestOptions();

    constructor(private http: HttpCemex, private projectEnv: ProjectSettings) {
        this.options.headers = new Headers();
        this.options.headers.append('Content-Type', 'application/json');
    }

    public getUserApplications(): Observable<Application[]> {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath() + 'v1/secm/applications/menu')
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    public getUserApplicationsAndRoles(): Observable<Application[]> {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/user/data/applications')
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    public getApplicationsAndRolesForUser(userId: number): Observable<Application[]> {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/user/' + userId + '/applications')
            .map((res: Response) => res.json().applications)
            .catch(this.handleError);
    }

    public getAllApplicationsAndRoles(): Observable<Application[]> {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/applications?internalRoles=false')
            .map((res: Response) => res.json().applications)
            .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        return Observable.throw(error);
    }
}
