import { Injectable } from '@angular/core';
import { RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

// Providers
import { HttpCemex, ProjectSettings } from '@cemex-core/angular-services-v2/dist';

// Models
import { Jobsite } from '../models';

@Injectable()
export class JobsiteService {

    private options = new RequestOptions();

    constructor(private http: HttpCemex, private projectEnv: ProjectSettings) {
        this.options.headers = new Headers();
        this.options.headers.append('Content-Type', 'application/json');
    }

    public getJobsitesForUser(userId: number): Observable<Jobsite[]> {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/user/' + userId + '/data/jobsites?pageSize=0&pageNumber=0')
            .map((res: Response) => res.json().jobsites)
            .catch(this.handleError);
    }

    public getUserJobsitesForCustomers(customersCodes: string): Observable<Jobsite[]> {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath()
            + 'v5/secm/user/data/customers/_CUSTOM_/jobsites?customersId=' + customersCodes + '&allJobsites=true')
            .map((res: Response) => res.json().jobsites)
            .catch(this.handleError);
    }

    public updateJobsite(userId , data) {
        return this.http.patch(this.projectEnv.getBaseOrgEnvPath() + '/v5/secm/user/' + userId + '/data/jobsites', data)
        .map((res: Response) => res)
        .catch(this.handleError);
    }

    public updateCustomers(userId , data) {
        return this.http.patch(this.projectEnv.getBaseOrgEnvPath() + '/v5/secm/user/' + userId + '/data/customers', data)
        .map((res: Response) => res)
        .catch(this.handleError);
    }

    private handleError(error: any) {
        return Observable.throw(error || 'Server error');
    }
}
