import {  RequestOptions, Headers, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

// Providers
import { HttpCemex, ProjectSettings } from '@cemex-core/angular-services-v2/dist';
// Models

import { User, UserProfile } from '../models';

@Injectable()
export class UserService {

    private options = new RequestOptions();

    constructor(private http: HttpCemex, private projectEnv: ProjectSettings) {
        this.options.headers = new Headers();
        this.options.headers.append('Content-Type', 'application/json');
    }

    public createUser(userData: any): Observable<Response> {
        return this.http.post(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/user', userData)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    public updateUser(userId: number, userDataPatch: any): Observable<Response> {
        return this.http.patch(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/user/' + userId, userDataPatch)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    public signUp(userData: any): Observable<Response> {
        return this.http.post(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/user', userData, null, true)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    public getUsers(): Observable<User[]> {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/users')
            .map((res: Response) => res.json().users)
            .catch(this.handleError);
    }

    public getUser(): Observable<UserProfile> {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/user')
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    public getUserDetails(userAccount: string) {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/users/search?userAccount=' + userAccount)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    public getUserFullActionLog(userId: number, dateFrom?, dateTo?) {
        const uri =
            `v5/secm/audit/log?dateFrom=${dateFrom}&dateTo=${dateTo}&entityType=USER_ALL&entityId=${userId}&selfService=&pageNumber=0&pageSize=0`;
        return this.http.get(this.projectEnv.getBaseOrgEnvPath() + uri)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    public updateFlag(allowInfShare: boolean): Observable<Response>  {
        const user = JSON.parse(sessionStorage.getItem('user_profile'));
        return this.http.patch(this.projectEnv.getBaseOrgEnvPath() + '/v5/secm/user/' + user.userId + '/profileflags', {
            allowInformationShare: allowInfShare
        })
                .map((res: Response) =>  res.status)
                .catch(this.handleError);
    }

    public getRulesPassword(countryCode: string) {
        return this.http.get(this.projectEnv.getBaseOrgEnvPath() +
            'v6/secm/passwords/rules?country=' + countryCode )
        .map((data: Response) => data.json())
        .catch(this.handleError);
    }

    private handleError(error: any) {
        return Observable.throw(error || 'Server error');
    }
}
