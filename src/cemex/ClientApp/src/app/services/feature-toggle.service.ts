import { Injectable } from '@angular/core';
import { toggleConfig } from '../../../toggleConfig';
import * as Rox from 'rox-browser';

@Injectable()
export class FeatureToggleService {

    /**
     * Creates an instance of feature toggle service.
     */
    constructor() {
        const appKey = (<any>window)['ROLLOUT_APP_KEY'];
        const manifest = require('../../../../wwwroot/manifest.json');
        const appVersion = manifest.version;

        Rox.register('CXGOS5', toggleConfig);
        Rox.setup(appKey, {
            version: appVersion,
            freeze: 'none',
        });
    }

    public chatbotIsEnabled(country): boolean {
        this.setCountryForFeature(country);
        return toggleConfig.useChatbot.isEnabled();
    }

    public toggleFeatureButtonIsEnabled(): boolean {
        return toggleConfig.useToggleFeatureButton.isEnabled();
    }

    public showOverrides(): void {
        Rox.showOverrides();
    }

    /**
     * Sets country in case the feature depends on it
     * @param country
     */
    private setCountryForFeature(country): void {
        Rox.setCustomStringProperty('country', country);
    }
}
