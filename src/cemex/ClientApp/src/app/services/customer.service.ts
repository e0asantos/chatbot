import { Injectable } from '@angular/core';
import { RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

// Providers
import { HttpCemex, ProjectSettings } from '@cemex-core/angular-services-v2/dist';

// Models
import { CustomerDetail } from '../models';

@Injectable()
export class CustomerService {

    private options = new RequestOptions();

    constructor(private http: HttpCemex, private projectEnv: ProjectSettings) {
        this.options.headers = new Headers();
        this.options.headers.append('Content-Type', 'application/json');
    }

    public getUserCustomers(): Observable<CustomerDetail[]> {
        return this.http.get( this.projectEnv.getBaseOrgEnvPath() + 'v2/cum/mycustomers?include=address')
            .map((res: Response) => res.json().customers)
            .catch(this.handleError);
    }

    public getCustomersForUser(userId: number): Observable<CustomerDetail[]> {
        return this.http.get( this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/user/' +  userId + '/data/customers')
            .map((res: Response) => res.json().customers)
            .catch(this.handleError);
    }

    public getCustomersAndJobsitesForUser(userId: number) {
        return this.http.get( this.projectEnv.getBaseOrgEnvPath() + 'v5/secm/user/' +  userId + '/data/customers?includeJobsites=true')
            .map((res: Response) => res.json().customers)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Observable.throw(error || 'Server error');
    }
}
