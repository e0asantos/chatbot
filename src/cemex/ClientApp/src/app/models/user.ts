import { ApplicationDTO } from '@cemex-core/types-v1/dist/applicationDTO';
import { ICustomer } from '@cemex-core/types-v2/dist/index.interface';
export class User {
    public userId: number;
    public fullName: string;
    public firstName: string;
    public lastName: string;
    public status: string;
    public customer: ICustomer;
    public customerId: number;
    public customerDesc: string;
    public applications: ApplicationDTO[];
}
