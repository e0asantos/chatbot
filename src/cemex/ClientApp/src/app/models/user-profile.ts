import { Profile } from './profile';
import { ApplicationDTO } from '@cemex-core/types-v1/dist/applicationDTO';
import { ICustomer } from '@cemex-core/types-v2/dist/index.interface';
export class UserProfile {
    public userId: number;
    public userAccount: string;
    public countryCode?: string;
    public fullName?: string;
    public firstName: string;
    public lastName: string;
    public phone?: string;
    public phoneNumber?: string;
    public status?: string;
    public customer?: ICustomer;
    public customerId: number;
    public userType: string;
    public applications?: ApplicationDTO[];
    public profile?: Profile;
    public userPosition?: string;
    public customerDesc?: string;
    public customerCode?: string;
    public isDigitalAdmin?: boolean;
    public token?: string;
}
