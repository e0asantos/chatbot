export class EmailStatus {
    public email: string;
    public sent: boolean;
    public action?: any;
}
