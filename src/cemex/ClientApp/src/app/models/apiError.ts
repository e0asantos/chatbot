export class ApiError {
    errorCode?: string;
    errorDesc?: string;
}
