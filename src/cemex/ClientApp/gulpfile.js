const gulp = require('gulp');
const purify = require('gulp-purifycss');
const minify = require('gulp-minify');
var options = {
    minify: true,
    output: "../wwwroot/dist/dls/dls.bundle.css"
};

var optionsCustom = {
    minify: true,
    output: "../wwwroot/dist/assets/styles/custom.css"
};

gulp.task('css', function() {
  return gulp.src('../submodules/dls/dist/css/app.css')
    .pipe(purify(['./src/**/*.html']))
    .pipe(gulp.dest('../wwwroot/dist/dls'));
});

gulp.task('minify', function() {
    gulp.src('../wwwroot/dist/dls/app.css')
      .pipe(purify(['./src/**/*.html'], options));
    
    gulp.src('../wwwroot/dist/assets/styles/custom.css')
      .pipe(purify(['./src/**/*.html'], optionsCustom));
    
    return true;
});
