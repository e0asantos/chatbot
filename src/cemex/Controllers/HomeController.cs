using System;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http.Headers;

namespace useradminconsole.Controllers
{
    public class HomeController : Controller

    {
        private readonly ILogger _logger;
        private HttpClient client;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
            var handler = new HttpClientHandler();
            handler.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            client = new HttpClient(handler);
        }

        public async Task<IActionResult> Index()
        {

            var translate = new TranslateController();
            var languages = await translate.LoadLanguagesFromServer();
            ViewData["CMX_LANGUAGES"] = languages;

            if (this.HttpContext.Request.Method == "GET" && (this.HttpContext.Request.Path.ToString().IndexOf("5167333814") != -1 || Environment.GetEnvironmentVariable("SITE_BUSY") == "yes"))
            {
                Environment.SetEnvironmentVariable("SITE_BUSY", "yes");
                return Redirect("https://www.cemexgo.com/under-maintenance");
            }
            else if (this.HttpContext.Request.Method == "GET" && this.HttpContext.Request.Path.ToString().IndexOf("6618988544") != -1)
            {
                Environment.SetEnvironmentVariable("SITE_BUSY", "no");
            }
            return View();
        }


        public IActionResult Error()
        {
            return View();
        }
    }
}
