#!/bin/bash

git checkout $1 

apt-get update
git submodule update --init --recursive
cd src/cemex/submodules/dls
git checkout deployable        
cd ../../

npm install
npm install webpack -g
node --max_old_space_size=4096 node_modules/webpack/bin/webpack.js --config webpack.config.vendor.js
node --max_old_space_size=4096 node_modules/webpack/bin/webpack.js
rm -rf node_modules/
dotnet publish --output publish --configuration release