cd src/cemex/ClientApp
        if [ -f "cypress.json" ]; then
                set -e
                npm cache clean --force
                npm link gulp
                npm link node-sass
                npm link gulp-inline-ng2-template
                npm link gulp-purifycss
                npm link buffer-to-vinyl
                npm link gulp-util
                npm link @angular/compiler-cli
                npm link wait-on
                npm link copyfiles
                npm link concurrently
                npm link webpack
                npm link webpack-dev-server
                npm install @cemex/cmx-button-v4
                npm install start-server-and-test
                npm install
                npm install cypress
                npm run test:cypress
                set +e
                files=`ls /opt/atlassian/pipelines/agent/build/src/cemex/cypress/videos/*.mp4`
                echo $files
                for eachfile in $files
                do
                    curl --upload-file $eachfile https://transfer.sh/$(basename $eachfile)
                done
                cd ../..
            else
                echo "had no automated tests found"
                echo "/ \__/|/ \/ ___\/ ___\/ \/ \  /|/  __/  / \ /\/ \  /|/ Y__ __\""
                echo "| |\/||| ||    \|    \| || |\ ||| |  _  | | ||| |\ ||| | / \  "
                echo "| |  ||| |\___ |\___ || || | \||| |_//  | \_/|| | \||| | | |  "
                echo "\_/  \|\_/\____/\____/\_/\_/  \|\____\  \____/\_/  \|\_/ \_/  "                                                                    
                echo "_____ _____ ____ _____ "
                echo "/__ __Y  __// ___Y__ __\""
                echo "/ \ |  \  |    \ / \  "
                echo "| | |  /_ \___ | | |  "
                echo "\_/ \____\\____/ \_/ "
                cd ../..
            fi